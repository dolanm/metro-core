﻿using metro_server.bases.items;
using metro_server.utils;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class RecipeManager
    {

        public List<Recipe> recipes { get; set; }

        public RecipeManager()
        {
            this.recipes = new List<Recipe>();
            this.load();
        }

        private void load()
        {
            Utils.Logging.PrintToConsole("[RecipeManager]: Loading recipes...");

            MySqlDataReader result = Server.INSTANCE.MYSQL.Select("recipes");
            while (result.Read())
            {
                Recipe i = new Recipe(result.GetInt32("id"), result.GetInt32("category"), JsonConvert.DeserializeObject<List<InventoryItem>>(result.GetString("resources")), JsonConvert.DeserializeObject<InventoryItem>(result.GetString("output")), result.GetInt32("xpreward"));

                this.recipes.Add(i);
            }
            result.Close();
            Utils.Logging.PrintToConsole("[ItemManager]: Loaded " + this.recipes.Count() + " recipes!");
        }


    }
}
