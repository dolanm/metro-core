﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_server.bases.items;
using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class RadioManager
    {
        public RadioManager()
        {
            API.RegisterCommand("rfreq", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if(p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        Profile cP = u.GetSelectedProfile();
                        if(cP != null)
                        {
                            if(cP.HasItem("Radio"))
                            {
                                try
                                {
                                    double freq = Convert.ToDouble(args[0]);
                                    p.TriggerEvent("metro:CL_E:Radio:SetFreq", freq.ToString("0.0"));
                                }
                                catch
                                {
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Attempted to set invalid frequency." } });
                                }
                            }
                            else
                            {
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have a radio." } });
                            }
                        }
                    }
                }
            }), false);

            API.RegisterCommand("radio", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        Profile cP = u.GetSelectedProfile();
                        if (cP != null)
                        {
                            if (cP.HasItem("Radio"))
                            {
                                if (args.Count > 0)
                                {
                                    string FullMsg = "";
                                    foreach(string s in args)
                                    {
                                        FullMsg = FullMsg += s + " ";
                                    }
                                    p.TriggerEvent("metro:CL_E:Radio:Send", FullMsg);
                                }
                            }
                            else
                            {
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have a radio." } });
                            }
                        }
                    }
                }
            }), false);

            API.RegisterCommand("r", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        Profile cP = u.GetSelectedProfile();
                        if (cP != null)
                        {
                            if (cP.HasItem("Radio"))
                            {
                                if (args.Count > 0)
                                {
                                    string FullMsg = "";
                                    foreach (string s in args)
                                    {
                                        FullMsg = FullMsg += s + " ";
                                    }
                                    p.TriggerEvent("metro:CL_E:Radio:Send", FullMsg);
                                }
                            }
                            else
                            {
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have a radio." } });
                            }
                        }
                    }
                }
            }), false);
        }



        public void SendMessage(double freq, string msg)
        {
            PlayerList pl = new PlayerList();
            foreach(Player p in pl)
            {
                p.TriggerEvent("metro:CL_E:Radio:Recieve", freq, msg);
            }
        }
    }
}
