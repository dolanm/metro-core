﻿using metro_server.bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using metro_server.utils;

namespace metro_server.managers
{
    public class SessionManager
    {
        public List<Session> sessions { get; set; }

        public SessionManager()
        {
            this.sessions = new List<Session>();
        }

        public Session generate(int servid)
        {
            Session s = Session.generate(servid);
            this.sessions.Add(s);
            return s;
        }

        public Session getSession(int servid)
        {
            foreach (Session s in this.sessions)
            {
                if(s.user == servid)
                {
                    return s;
                }
            }
            return null;
        }

        public Session getSession(Guid session)
        {
            foreach(Session s in this.sessions)
            {
                if(s.token.Equals(session))
                {
                    return s;
                }
            }
            return null;
        }

        public Boolean isValid(Guid token, int servid)
        {
            foreach(Session s in this.sessions)
            {
                if(s.token.Equals(token))
                {
                    if(Utils.getCurrentMili() - s.lastUpdate >= 900000)
                    {
                        this.sessions.Remove(s);
                        return false;
                    }
                    else
                    {
                        return s.user == servid;
                        
                    }
                }
            }

            return false;
        }

    }
}
