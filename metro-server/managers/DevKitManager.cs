﻿using CitizenFX.Core;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class DevKitManager
    {
        public void LogVector(Vector3 position)
        {
            if(position != null)
            {
                Utils.Logging.PrintToConsole("[DevKit]: LogVector = " + position.ToString());
            }
        }
    }
}
