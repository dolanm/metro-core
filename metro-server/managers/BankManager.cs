﻿using CitizenFX.Core;
using metro_server.bases;
using metro_server.bases.users;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class BankManager
    {
        public List<BankVault> RegisteredBanks;

        public BankManager()
        {
            Utils.Logging.PrintToConsole("Setting up BankManager...");
            this.RegisteredBanks = new List<BankVault>();

            #region ADD BANKS
            this.RegisteredBanks.Add(new BankVault("Pillbox", new Vector3(146.77f, -1044.79f, 29.38f), 5, 4, false, 5000, 1000));
            this.RegisteredBanks.Add(new BankVault("Paleto Bay", new Vector3(-103.6623f, 6477.792f, 31.62672f), 5, 4, false, 5000, 1000));
            this.RegisteredBanks.Add(new BankVault("Route 68", new Vector3(1176.496f, 2711.58f, 38.09778f), 5, 4, false, 5000, 1000));
            this.RegisteredBanks.Add(new BankVault("Chumash", new Vector3(-2957.652f, 481.3654f, 15.70647f), 5, 4, false, 5000, 1000));
            this.RegisteredBanks.Add(new BankVault("Hawick", new Vector3(-353.9925f, -54.05035f, 49.04631f), 5, 4, false, 5000, 1000));
            this.RegisteredBanks.Add(new BankVault("Vinewood", new Vector3(311.1357f, -283.2345f, 54.17453f), 5, 4, false, 5000, 1000));
            #endregion

            Utils.Logging.PrintToConsole("[BankManager]: All banks registered!");
        }

        public BankVault GetBankByName(string name)
        {
            foreach(BankVault bank in this.RegisteredBanks)
            {
                if(bank.VaultName == name)
                {
                    return bank;
                }
            }
            return null;
        }

        public BankVault GetBankByVector(Vector3 position)
        {
            foreach(BankVault bank in this.RegisteredBanks)
            {
                if(bank.VaultTrigger.DistanceToSquared(position) <= 2.0f)
                {
                    return bank;
                }
            }
            return null;
        }

        public void CancelRobbery(int source, Vector3 PlayerPosition)
        {
            PlayerList pl = new PlayerList();
            Player p = pl[source];
            if (this.GetBankByVector(PlayerPosition) != null)
            {
                BankVault bank = this.GetBankByVector(PlayerPosition);
                if(bank.IsBeingRobbed)
                {
                    if(!bank.IsCooling)
                    {
                        if(bank.TriggerSource == p)
                        {
                            bank.OnRobberyCanceled();
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Failed to cancel robbery that you do not own." } });
                            Utils.Logging.PrintToConsole("[VaultRobbery] => " + p.Name + " attempted to cancel a robbery that isn't owned");
                        }
                    }
                }
                else
                {
                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Failed to cancel robbery as one has not been started." } });
                    Utils.Logging.PrintToConsole("[VaultRobbery] => " + p.Name + " attempted to cancel a robbery that doesn't exist");
                }
            }
            else
            {
                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Could not cancel robbery at current position." } });
                Utils.Logging.PrintToConsole("[VaultRobbery] => " + p.Name + " attempted to cancel a robbery with invalid position at");
            }
        }

        public void BeginRobbery(int source, Vector3 PlayerPosition)
        {
            PlayerList pl = new PlayerList();
            Player p = pl[source];
            if(this.GetBankByVector(PlayerPosition) != null)
            {
                BankVault bank = this.GetBankByVector(PlayerPosition);
                if(!bank.IsBeingRobbed)
                {
                    if(!bank.IsCooling)
                    {
                        bank.TriggerSource = p;
                        User tempUser = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                        bank.TriggerProfile = tempUser.GetSelectedProfile();
                        bank.OnRobberyTriggered();
                    }
                    else
                    {
                        p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Failed to begin robbery as bank is currently on cooldown." } });
                        Utils.Logging.PrintToConsole("[VaultRobbery] => " + p.Name + " attempted to start a robbery on bank that is cooling");
                    }
                }
                else
                {
                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Failed to begin robbery as one is already started." } });
                    Utils.Logging.PrintToConsole("[VaultRobbery] => " + p.Name + " attempted to start a robbery that was already started");
                }
            }
            else
            {
                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Could not begin robbery at current position." } });
                Utils.Logging.PrintToConsole("[VaultRobbery] => " + p.Name + " attempted to start a robbery with invalid position at");
            }
        }


        public void OnTick()
        {
            foreach(BankVault bank in this.RegisteredBanks)
            {
                if(bank.IsBeingRobbed || bank.IsCooling)
                {
                    bank.Tick();
                }
            }
        }
    }
}
