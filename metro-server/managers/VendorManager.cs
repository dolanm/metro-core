﻿using CitizenFX.Core;
using metro_server.bases;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class VendorManager
    {
        public List<NPCVendor> Vendors = new List<NPCVendor>();

        public VendorManager()
        {
            Utils.Logging.PrintToConsole("Setting up VendorManager...");
            this.Vendors.Add(new NPCVendor(1, "Contraband Vendor", new Vector3(900.24f, -1071.98f, 32.97f), 4.0f));


            foreach(NPCVendor vendor in this.Vendors)
            {
                Utils.Logging.PrintToConsole("[VendorManager]: Registered vendor '" + vendor.VendorTitle + "' with ID(" + vendor.VendorID + ")");
            }
            Utils.Logging.PrintToConsole("[VendorManager]: Populating vendor inventories...");
            this.GetVendor(1).AddItem("Radio", 50, 3);
            Utils.Logging.PrintToConsole("[VendorManager]: Finished!");
        }

        public NPCVendor GetVendor(int VendorID)
        {
            foreach(NPCVendor vendor in this.Vendors)
            {
                if(vendor.VendorID == VendorID)
                {
                    return vendor;
                }
            }
            return null;
        }

        public void TryPurchase(int source, Vector3 position, int VendorID, string ItemName)
        {
            NPCVendor Vendor = this.GetVendor(VendorID);
            if(Vendor != null)
            {
                if(position.DistanceToSquared(Vendor.VendorLocation) <= Vendor.DistanceToTrigger)
                {
                    VendorItem item = Vendor.GetItem(ItemName);
                    if(item != null)
                    {
                        item.PurchaseItem(source);
                    }
                    else
                    {
                        Utils.Logging.PrintToConsole("[VendorManager]: Attempted to purchase invalid item. Ref ID: " + VendorID.ToString());
                    }
                }
                else
                {
                    Utils.Logging.PrintToConsole("[VendorManager]: Attempted to trigger TryPurchase with invalid position on vendor. Ref ID: " + VendorID.ToString());
                }
            }
            else
            {
                Utils.Logging.PrintToConsole("[VendorManager]: Attempted TryPurchase with invalid vendor. Ref ID: " + VendorID.ToString());
            }
        }
    }
}
