﻿using metro_server.bases.items;
using metro_server.bases.items.Consumables;
using metro_server.bases.items.Misc;
using metro_server.utils;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class ItemManager
    {

        public List<Item> items { get; set; }
        public ItemManager()
        {
            this.items = new List<Item>();
      

            this.load();
        }


        private void load()
        {
            Utils.Logging.PrintToConsole("[ItemManager]: Loading items...");

            this.items.Add(new AppleItem(this.items.Count()));
            this.items.Add(new WoodItem(this.items.Count()));
            this.items.Add(new StoneItem(this.items.Count()));
            this.items.Add(new RadioItem(this.items.Count()));




            Utils.Logging.PrintToConsole("[ItemManager]: Loaded " + this.items.Count() + " items!");

        }

        public Item getItem(int id)
        {
            foreach(var i in this.items)
            {
                if(i.id == id)
                {
                    return i;
                }
            }
            return null;
        }
    }
}
