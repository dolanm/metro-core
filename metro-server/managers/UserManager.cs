﻿using CitizenFX.Core;
using metro_server.bases;
using metro_server.bases.users;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class UserManager
    {
        public List<User> OnlinePlayers { get; set; }

        public UserManager()
        {
            Utils.Logging.PrintToConsole("Setting up UserManager...");
            this.OnlinePlayers = new List<User>();
        }

        public void onPlayerReady(Player p)
        {
            Utils.Logging.PrintToConsole("[" + p.Name + "] is ready...");
            Dictionary<string, string> ids = Utils.GetProperPlayerIDs(p);

            User tempuser = new User(ids["license"]);
            tempuser.ServerID = Convert.ToInt32(p.Handle);
            if (ids.ContainsKey("steam"))
            {
                tempuser.steamid = ids["steam"];
            }



            tempuser.OnLogin(p.Name);
            tempuser.sendModels();
            tempuser.sendItems();
            tempuser.sendRecipes();
            tempuser.sendSession();
            tempuser.sendProfiles();
            this.OnlinePlayers.Add(tempuser);
            p.TriggerEvent("metro:CL_E:ServerReady");
            Utils.Logging.PrintToConsole("[" + tempuser.username + "] has connected.");

        }

        public User GetPlayerFromID(int id)
        {
            foreach(User u in this.OnlinePlayers)
            {
                if(u.ServerID == id)
                {
                    return u;
                }
            }
            return null;
        }
    }
}
