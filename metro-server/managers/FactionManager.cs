﻿using metro_server.bases.factions;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.managers
{
    public class FactionManager
    {
        public List<Faction> RegisteredFactions { get; set; }

        public FactionManager()
        {
            Utils.Logging.PrintToConsole("Setting up FactionManager...");
            this.RegisteredFactions = new List<Faction>();
        }

        public Faction GetFactionByName(string name)
        {
            foreach(Faction f in this.RegisteredFactions)
            {
                if(f.FactionName == name)
                {
                    return f;
                }
            }
            return null;
        }

        public Faction GetFactionByID(int id)
        {
            foreach(Faction f in this.RegisteredFactions)
            {
                if(f.FactionID == id)
                {
                    return f;
                }
            }
            return null;
        }

        public void RegisterFaction(Faction f)
        {
            if(f != null)
            {
                this.RegisteredFactions.Add(f);
                Utils.Logging.PrintToConsole("[FactionManager]: Registered new faction '" + f.FactionName + "' with ID(" + f.FactionID + ").");
            }
            else
            {
                Utils.Logging.PrintToConsole("[FactionManager]: Attempted to register null faction...");
            }
        }
    }
}
