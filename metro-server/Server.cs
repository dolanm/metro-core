﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_server.bases;
using metro_server.bases.factions;
using metro_server.bases.items;
using metro_server.bases.users;
using metro_server.jobs.police;
using metro_server.managers;
using metro_server.staff;
using metro_server.utils;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server
{
    public class Server : BaseScript
    {
        public static Server INSTANCE { get; set; }

        public Mysql MYSQL { get; set; }

        public ItemManager ItemManager { get; set; }

        public RecipeManager RecipeManager { get; set; }

        public UserManager UserManager { get; set; }
        public SessionManager SessionManager { get; set; }
        public FactionManager FactionManager { get; set; }

        public DevKitManager DevKitManager { get; set; }

        public StaffCommands StaffCommands { get; set; }

        public RadioManager RadioManager { get; set; }

        public PoliceMain PoliceJob { get; set; }

        public BankManager BankManager { get; set; }

        public VendorManager VendorManager { get; set; }

        public Server()
        {
            Server.INSTANCE = this;
            Debug.WriteLine(" ");
            Debug.WriteLine("==========================================================");
            Debug.WriteLine(" ");
            Utils.Logging.PrintToConsole("INITIALIZED FRAMEWORK...");

            this.DevKitManager = new DevKitManager();
            this.RadioManager = new RadioManager();
            this.PoliceJob = new PoliceMain();
            this.VendorManager = new VendorManager();

            Utils.Logging.PrintToConsole("Connecting to MySQL...");
            //SERVER LOGIN = root:cucumber132
            this.MYSQL = new Mysql("localhost", "metro");
            this.MYSQL.Login("root", "");

            if(!this.MYSQL.isConnected())
            {
                Utils.Logging.PrintToConsole("[MySQL]: Unable to connect!");
                API.StopResource(API.GetCurrentResourceName());
                return;
            }

            Utils.Logging.PrintToConsole("[MySQL]: Connected!");

            this.BankManager = new BankManager();

            Utils.Logging.PrintToConsole("Registering events...");
            #region EVENT REGISTRY
            EventHandlers.Add("playerConnecting", new Action<Player, string, dynamic, dynamic>(this.OnPlayerConnecting));
            EventHandlers.Add("playerDropped", new Action<Player, string>(this.OnPlayerDisconnect));
            EventHandlers.Add("chatMessage", new Action<int, string, string>(this.OnChatReceived));
            EventHandlers.Add("metro:SV_E:PlayerSpawnedInWorld", new Action<Player>(this.SpawnCallback));
            EventHandlers.Add("metro:SV_E:PlayerReady", new Action<Player>(this.onPlayerReady));
            EventHandlers.Add("metro:SV_E:RequestSession", new Action<Player>(this.OnPlayerRequestSession));
            EventHandlers.Add("metro:SV_E:RequestProfiles", new Action<Player, String>(this.OnPlayerRequestProfiles));
            EventHandlers.Add("metro:SV_E:CreateProfile", new Action<Player, String, String, String, String, double, int>(this.onPlayerCreateProfile));
            EventHandlers.Add("metro:SV_E:DeleteProfile", new Action<Player, String, int>(this.onPlayerDeleteProfile));
            EventHandlers.Add("metro:SV_E:SelectProfile", new Action<Player, String, int>(this.onPlayerSelectProfile));
            EventHandlers.Add("metro:SV_E:inventory:swap", new Action<Player, String, int, int>(this.onPlayerInventorySwap));
            EventHandlers.Add("metro:SV_E:inventory:interact", new Action<Player, String, int>(this.OnPlayerInventoryInteract));

            EventHandlers.Add("metro:SV_E:Radio:Send", new Action<double, string>(this.RadioManager.SendMessage));

            EventHandlers.Add("metro:SV_E:Robbery:StartBank", new Action<int, Vector3>(this.BankManager.BeginRobbery));
            EventHandlers.Add("metro:SV_E:Robbery:Cancel", new Action<int, Vector3>(this.BankManager.CancelRobbery));

            EventHandlers.Add("metro:SV_E:SendLocalChat", new Action<string, string, Vector3>(this.SendLocalChat));

            #region DEV KIT EVENTS
            EventHandlers.Add("metro:SV_E:LaLa:DEV:LogVector", new Action<Vector3>(this.DevKitManager.LogVector));
            #endregion

            #endregion
            Utils.Logging.PrintToConsole("All events registered!");

            this.ItemManager = new ItemManager();

            this.RecipeManager = new RecipeManager();

            this.SessionManager = new SessionManager();

            this.UserManager = new UserManager();

            this.FactionManager = new FactionManager();
            #region Faction Class Registry
            this.FactionManager.RegisterFaction(new Civilian("civilian", 0, new Vector3(-207.0937f, -1015.908f, 30.13814f), true, false));
            this.FactionManager.RegisterFaction(new EMS("ems", 1, new Vector3(0.0f, 0.0f, 0.0f), false, false));
            this.FactionManager.RegisterFaction(new Police("police", 2, new Vector3(460.16f, -990.82f, 30.69f), true, true));
            /*Utils.FactionClasses.Add("civilian", 0);
            Utils.FactionClasses.Add("ems", 1);
            Utils.FactionClasses.Add("police", 2);*/

            #endregion
            Utils.Logging.PrintToConsole("All factions registered!");

            Utils.Logging.PrintToConsole("Registering commands...");

            this.StaffCommands = new StaffCommands();
            this.StaffCommands.RegisterCommands();
            
            #region COMMAND REGISTRY
            API.RegisterCommand("finfo", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sP = pl[source];
                User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(sP.Handle));
                Profile p = u.GetSelectedProfile();
                sP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Faction class: " + p.CurrentFaction.FactionName.ToString() + " | Faction rank: " + p.FactionRank.ToString()} });
            }), false);


            API.RegisterCommand("devkit", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sP = pl[source];
                sP.TriggerEvent("metro:CL_E:LaLa:DEV:OpenKit");
            }), true);


            API.RegisterCommand("setstaff", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sP = pl[source];
                if (args.Count > 0)
                {
                    Player tP = pl[Int32.Parse(args[0].ToString())];

                    if(tP != null)
                    {
                        User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(tP.Handle));
                        if(u != null)
                        {
                            u.IsStaff = Convert.ToBoolean(args[1].ToString());
                            u.save();
                            u.sendProfiles();

                            Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Modified staff for '" + tP.Name.ToString() + "' to " + args[1].ToString());
                            sP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Modified staff for '" + tP.Name.ToString() + "' to " + args[1].ToString() } });
                            tP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Your staff rank has been updated to '" + args[1].ToString().ToLower() + "'." } });
                        }
                        else
                        {
                            tP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Found player, but cannot find user? :/" } });
                        }

                    }
                    else
                    {
                        tP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Unable to find user with ID: '" + args[0].ToString().ToLower() + "'." } });
                    }


                }
                else
                {
                    Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Attempted to use 'setfacrank' without args...");
                }
            }), true);


            API.RegisterCommand("setclass", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sP = pl[source];
                if(args.Count > 0)
                {
                    Player tP = pl[Int32.Parse(args[0].ToString())];
                    if(this.FactionManager.GetFactionByName(args[1].ToString().ToLower()) != null)
                    {
                        //User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(player.Handle));
                        User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(tP.Handle));
                        Profile p = u.GetSelectedProfile();
                        p.CurrentFaction = this.FactionManager.GetFactionByName(args[1].ToString().ToLower());
                        u.save();
                        u.sendProfiles();
                        tP.TriggerEvent("metro:CL_E:FactionUpdated", args[1].ToString().ToLower());
                        Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Changed class for '" + tP.Name.ToString() + "' to '" + args[1].ToString().ToLower() + "'...");
                        sP.TriggerEvent("chat:addMessage", new { color=new[] { 255, 0, 0 }, multiline = true, args=new[] { "^1[METRO]", "Set class to '" + args[1].ToString().ToLower() + "'." } });
                        tP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Your class has been updated to '" + args[1].ToString().ToLower() + "'." } });
                    }
                    else
                    {
                        Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Attempted to use 'setclass' with an invalid class id...");
                    }
                }
                else
                {
                    Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Attempted to use 'setclass' without args...");
                }
            }), true);


            API.RegisterCommand("setfacrank", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sP = pl[source];
                if (args.Count > 0)
                {
                    Player tP = pl[Int32.Parse(args[0].ToString())];

                    User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(tP.Handle));
                    Profile p = u.GetSelectedProfile();
                    p.FactionRank = Convert.ToInt32(args[1].ToString().ToLower());
                    u.save();
                    u.sendProfiles();

                    Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Changed faction rank for '" + tP.Name.ToString() + "' to '" + args[1].ToString().ToLower() + "'...");
                    sP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Set faction rank to '" + args[1].ToString().ToLower() + "'." } });
                    tP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Your faction rank has been updated to '" + args[1].ToString().ToLower() + "'." } });
                }
                else
                {
                    Utils.Logging.PrintToConsole("[" + sP.Name.ToString() + "] => Attempted to use 'setfacrank' without args...");
                }
            }), true);

            #endregion
            Utils.Logging.PrintToConsole("All commands registered!");

            Debug.WriteLine(" ");
            Debug.WriteLine("==========================================================");
            Debug.WriteLine(" ");

            Tick += new Func<Task>(async delegate
            {
                this.BankManager.OnTick();
            });
        }

        private void onPlayerReady([FromSource]Player p)
        {
            
            this.UserManager.onPlayerReady(p);
        }

        private void SpawnCallback([FromSource]Player p)
        {
            //Can do whatever to the player when ped in the world.
           
            Utils.Logging.PrintToConsole("[" + p.Name + "] has spawned.");



        }

        public void OnChatReceived(int source, string name, string msg)
        {
            if(!msg.StartsWith("/"))
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if(msg != "")
                {
                    p.TriggerEvent("metro:CL_E:SendLocalChat", name, msg);
                }
                API.CancelEvent();
            }
        }

        public void SendLocalChat(string prefix, string msg, Vector3 position)
        {
            PlayerList pl = new PlayerList();
            foreach(Player p in pl)
            {
                p.TriggerEvent("metro:CL_E:DisplayLocalChat", prefix, msg, position);
            }
            Utils.Logging.PrintToConsole("[CHAT INFO] => " + prefix.Replace('^', '&') + ": " + msg.Replace('^', '&'));
        }

        private void OnPlayerInventoryInteract([FromSource]Player player, String session, int itemid)
        {
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Interacting Inventory...");
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(player.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried interacting inventory but cannot find user; Dropping.");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            Guid news;

            if (!Guid.TryParse(session, out news))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried interacting inventory but sent an invalid session; Dropping. (" + session + ")");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            if (!this.SessionManager.isValid(news, u.ServerID))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried interacting inventory but sent a mismatching session; Dropping.");
                player.Drop("Session mismatch, please reconnect");
                return;
            }

            Session s = this.SessionManager.getSession(news);

            s.update();


            Profile selectedProfile = u.GetSelectedProfile();
            if (selectedProfile == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried interacting inventory but does not have an active profile.");
                return;
            }

            Item i = this.ItemManager.getItem(itemid);
            if(i == null)
            {
                u.sendMessage("Server", "Cannot interact with that. (Unknown Item)", new int[] { 255, 0, 0});
                return;
            }


            if(!selectedProfile.HasItemAtleast(new InventoryItem(itemid, 1)))
            {
                u.sendMessage("Server", "Cannot interact with that. (Do not have)", new int[] { 255, 0, 0 });
                return;
            }

            if(i is ConsumableItem)
            {
                ConsumableItem ci = (ConsumableItem)i;
                ci.OnConsumed(u);
                u.sendMessage("Server", "Interacted (consumable)", new int[] { 255, 0, 0 });

            }
            else if(i is WeaponItem)
            {
                WeaponItem wi = (WeaponItem)i;
                wi.onEquipt(u);
                u.sendMessage("Server", "Interacted (weapon)", new int[] { 255, 0, 0 });

            }
            else
            {
                u.sendMessage("Server", "Cannot interact with that. (Do not have)", new int[] { 255, 0, 0 });
                return;
            }


          
        }

        private void onPlayerInventorySwap([FromSource]Player player, String session, int from, int to)
        {
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Altering Inventory...");
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(player.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried altering inventory but cannot find user; Dropping.");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            Guid news;

            if (!Guid.TryParse(session, out news))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried altering inventory but sent an invalid session; Dropping. (" + session + ")");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            if (!this.SessionManager.isValid(news, u.ServerID))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried altering inventory but sent a mismatching session; Dropping.");
                player.Drop("Session mismatch, please reconnect");
                return;
            }

            Session s = this.SessionManager.getSession(news);

            s.update();


            Profile selectedProfile = u.GetSelectedProfile();
            if(selectedProfile == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried altering inventory but does not have an active profile.");
                return;
            }

            selectedProfile.SwapSlots(from, to);
            u.save();
            u.sendProfiles();
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Swapped items in inventory.");

        }

        private void onPlayerSelectProfile([FromSource]Player player, String session, int profile)
        {
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Selecting Profile...");
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(player.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but cannot find user; Dropping.");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            Guid news;

            if (!Guid.TryParse(session, out news))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried selecting profile but sent an invalid session; Dropping. (" + session + ")");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            if (!this.SessionManager.isValid(news, u.ServerID))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Tried selecting profile but sent a mismatching session; Dropping.");
                player.Drop("Session mismatch, please reconnect");
                return;
            }

            Session s = this.SessionManager.getSession(news);

            s.update();

            u.SelectedProfile = profile;

            
            player.TriggerEvent("metro:CL_E:ProfileSelected", u.SelectedProfile);
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Selected Profile (" + profile + ")");
            Utils.UpdatePlayerLists();

        }

        private void onPlayerCreateProfile([FromSource]Player player, String session, String first, String last, String dob, double height, int gender)
        {
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Creating Profile...");
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(player.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but cannot find user; Dropping.");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            Guid news;

            if (!Guid.TryParse(session, out news))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid session; Dropping. (" + session + ")");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            if (!this.SessionManager.isValid(news, u.ServerID))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent a mismatching session; Dropping.");
                player.Drop("Session mismatch, please reconnect");
                return;
            }

            Session s = this.SessionManager.getSession(news);

            s.update();

            if (u.AllProfiles.Count == 3)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but already has 3; Blocked.");
                return;
            }

           


            if (first.Length < 4)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid first name; Blocked.");
                return;
            }

            if (last.Length < 4)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid last name; Blocked.");
                return;
            }
            if (!Utils.isValidDOB(dob))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid DOB; Blocked.");

                return;
            }


            if (height < 4 || height > 7)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid height; Blocked.");
                return;
            }

            if (gender != 0 && gender != 1)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid gender; Blocked.");
                return;
            }


            Profile pr = u.createProfile( first, last, dob, height, gender);
            Item i = this.ItemManager.getItem(0);
            if(i != null)
            {
                pr.AddItem(new InventoryItem(i.id, i.maxstack * 2 - (i.maxstack / 2)));
                u.save();
            }
            u.sendProfiles();
            u.SelectedProfile = u.AllProfiles.Count - 1;
            
            player.TriggerEvent("metro:CL_E:ProfileCreated");
        }

        private void onPlayerDeleteProfile([FromSource]Player player, String session, int profile)
        {
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Deleting Profile...");
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(player.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but cannot find user; Dropping.");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            Guid news;

            if (!Guid.TryParse(session, out news))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent an invalid session; Dropping. (" + session + ")");
                player.Drop("Unable to find user, please contact an admin");
                return;
            }

            if (!this.SessionManager.isValid(news, u.ServerID))
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent a mismatching session; Dropping.");
                player.Drop("Session mismatch, please reconnect");
                return;
            }

            Session s = this.SessionManager.getSession(news);

            s.update();

            if (profile < 0 || profile > 3)
            {
                Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Created profile but sent a mismatching session; Dropping.");
                return;
            }

            u.removeProfile(profile);
            u.sendProfiles();
            player.TriggerEvent("metro:CL_E:ProfileDeleted");
            Utils.Logging.PrintToConsole("[" + player.Name.ToString() + "] Deleted Profile...");

        }

        private void OnPlayerConnecting([FromSource]Player p, string name, dynamic reason, dynamic deferrals)
        {
            Dictionary<string, string> ids = Utils.GetProperPlayerIDs(p);
            if(!ids.ContainsKey("license"))
            {
                Utils.Logging.PrintToConsole("[" + p.Name + "] Unable to find license; Dropping.");
                p.Drop("Unable to find license, please contact an admin.");
                return;
            }

            Dictionary<string, object> BanParas = new Dictionary<string, object>();
            BanParas.Add("license", ids["license"]);
            MySqlDataReader result = this.MYSQL.Select("bans", BanParas);
            if(result.HasRows)
            {
                while (result.Read())
                {
                    if (result.GetDateTime("expires") <= DateTime.Now)
                    {
                        //Allow connecting
                    }
                    else
                    {
                        Utils.Logging.PrintToConsole("[" + p.Name + "] Attempted to join while banned; Dropping.");
                        reason("You have been banned.\r\n------------------------\r\nBanned By: " + result.GetString("admin") + "\r\nExpiration Date: " + result.GetDateTime("expires") + "\r\nBan Reason: " + result.GetString("reason"));
                        result.Close();
                        API.CancelEvent();
                        return;
                    }
                }
            }
            result.Close();
            User tempuser = new User(ids["license"]);

            tempuser.ServerID = Convert.ToInt32(p.Handle);


            Utils.Logging.PrintToConsole("[" + p.Name + "] Attemping to find Steam ID...");
            if (ids.ContainsKey("steam"))
            {
                tempuser.steamid = ids["steam"];
                Utils.Logging.PrintToConsole("[" + p.Name + "] Found Steam ID!");

            }
            tempuser.username = p.Name;
            tempuser.save();

            Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] is currently connecting... (" + p.Handle + ")");
        }



        private void OnPlayerDisconnect([FromSource]Player p, string reason)
        {
            if(!reason.Contains("banned"))
            {
                User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                if (u == null)
                {
                    Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Disconnected; but cannot find user. Reason: " + reason);
                    return;
                }

                Session s = this.SessionManager.getSession(u.ServerID);
                if (s != null)
                {
                    this.SessionManager.sessions.Remove(s);
                    Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Removed Session for predisconnect");
                }
                this.UserManager.OnlinePlayers.Remove(u);
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Disconnected: " + reason);
                Utils.UpdatePlayerLists();
            }
        }

        private void OnPlayerRequestSession([FromSource]Player p)
        {
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Requested session but cannot find user; Dropping.");
                p.Drop("Unable to find user, please contact an admin");
                return;
            }

            Session existses = this.SessionManager.getSession(u.ServerID);
            if(existses != null)
            {
                this.SessionManager.sessions.Remove(existses);
            }

            Session newses = this.SessionManager.generate(u.ServerID);

            p.TriggerEvent("metro:CL_E:SessionCreation", newses.token.ToString());


        }

        private void OnPlayerRequestProfiles([FromSource]Player p, String session)
        {
            Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Requesting profiles...");
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Requested profiles but cannot find user; Dropping.");
                p.Drop("Unable to find user, please contact an admin");
                return;
            }

            Guid news;

            if(!Guid.TryParse(session, out news))
            {
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Requested profiles but sent an invalid session; Dropping. (" + session + ")");
                p.Drop("Unable to find user, please contact an admin");
                return;
            }

            if(!this.SessionManager.isValid(news, u.ServerID))
            {
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Requested profiles but sent a mismatching session; Dropping.");
                p.Drop("Session mismatch, please reconnect");
                return;
            }
            Session s = this.SessionManager.getSession(news);

            s.update();
            u.sendProfiles();


        }

    }
}
