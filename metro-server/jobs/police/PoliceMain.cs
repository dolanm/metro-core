﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_server.bases.users;
using metro_server.utils;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.jobs.police
{
    public class PoliceMain
    {
        public PoliceMain()
        {
            Utils.Logging.PrintToConsole("Setting up Jobs.PoliceMain...");
            this.RegisterAllCommands();
        }

        public void SendPoliceMessage(string prefix, string message)
        {
            string RealPrefix = "";
            if(prefix == null || prefix == "")
            {
                RealPrefix = "^^5[POLICE]";
            }
            else {
                RealPrefix = prefix;
            }
            PlayerList pl = new PlayerList();
            foreach(Player p in pl)
            {
                User tempUser = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                if(tempUser.GetSelectedProfile().CurrentFaction.FactionName == "police")
                {
                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { RealPrefix, message } });
                }
            }
        }

        #region Commands
        public void RegisterAllCommands()
        {
            #region TEMPLATE
           /* API.RegisterCommand("warrant", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        Profile cP = u.GetSelectedProfile();
                        if (cP != null)
                        {
                            if (cP.CurrentFaction.FactionName == "police")
                            {

                            }
                            else
                            {
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You are not police." } });
                            }
                        }
                    }
                }
            }), false);*/
            #endregion


            API.RegisterCommand("warrant", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        Profile cP = u.GetSelectedProfile();
                        if (cP != null)
                        {
                            if(cP.CurrentFaction.FactionName == "police")
                            {
                                if(args.Count > 0)
                                {
                                    string op = args[0].ToString();
                                    switch(op)
                                    {
                                        case "list":
                                            args.RemoveAt(0);
                                            if(args[0] != null && args[1] != null)
                                            {
                                                Dictionary<string, object> paras = new Dictionary<string, object>();
                                                paras.Add("fname", args[0].ToString());
                                                paras.Add("lname", args[1].ToString());
                                                MySqlDataReader result = Server.INSTANCE.MYSQL.Select("char_warrants", paras);
                                                while(result.Read())
                                                {
                                                    //p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[WARRANT]", "(" + result.GetInt32("id") + "): " + result.GetString("reason") + " ~ " + result.GetString("issuedby") } });
                                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[WARRANT]", "(#" + result.GetInt32("id") + ") " + result.GetString("fname") + " " + result.GetString("lname") + " | " + result.GetString("reason") + " | ~ " + result.GetString("issuedby") } });
                                                }
                                                result.Close();
                                            }
                                            else
                                            {
                                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid arguments specified." } });
                                            }
                                            break;
                                        case "create":
                                            args.RemoveAt(0);
                                            if (args[0] != null && args[1] != null)
                                            {
                                                string FirstName = args[0].ToString();
                                                string LastName = args[1].ToString();
                                                args.RemoveRange(0, 2);
                                                string FullReason = "";
                                                foreach(string arg in args)
                                                {
                                                    FullReason = FullReason + arg + " ";
                                                }
                                                DateTime inputTime = DateTime.Now;
                                                string CopName = cP.FirstName + " " + cP.LastName;
                                                long idd = Server.INSTANCE.MYSQL.Insert("char_warrants", new object[] { null, FirstName, LastName, FullReason, CopName, inputTime });
                                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Warrant has been issued." } });
                                            }
                                            else
                                            {
                                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid arguments specified." } });
                                            }
                                            break;
                                        case "del":
                                            args.RemoveAt(0);
                                            if(args[0] != null)
                                            {
                                                if (Server.INSTANCE.MYSQL.DeleteByID("char_warrants", Convert.ToInt32(args[0].ToString())) != 0)
                                                {
                                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Deleted warrant." } });
                                                }
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid arguments specified." } });
                                }
                            }
                            else
                            {
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You are not police." } });
                            }
                        }
                    }
                }
            }), false);
        }
        #endregion
    }
}
