﻿using CitizenFX.Core;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.factions
{
    public class EMS : Faction
    {
        public EMS(string name, int id, Vector3 spawn, bool usespawn, bool hasranks) : base(name, id, spawn, usespawn, hasranks) { }
    }
}
