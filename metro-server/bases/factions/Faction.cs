﻿using CitizenFX.Core;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.factions
{
    public class Faction
    {
        public string FactionName { get; set; }
        public int FactionID { get; set; }
        public Vector3 FactionSpawn { get; set; }
        public List<Player> OnlineMembers { get; set; }
        public bool UseSpawn { get; set; }

        public bool HasRanks { get; set; }

        public Faction(string name, int id, Vector3 spawn, bool usespawn, bool hasranks)
        {
            this.FactionName = name;
            this.FactionID = id;
            this.FactionSpawn = spawn;
            this.OnlineMembers = new List<Player>();
            this.UseSpawn = usespawn;
            this.HasRanks = hasranks;
        }
    }
}
