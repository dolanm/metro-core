﻿using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.items
{
    public class Item
    {

        public int id { get; set; }
        public int category { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public int weight { get; set; }

        public string model { get; set; }

        public string sprite { get; set; }

        public int maxstack { get; set; }

        public int cooldown { get; set; }

        public Dictionary<int, int> benifits { get; set; }

        public Item(int id, int category, String name, String description, int weight, String sprite, String model, int maxstack, int cooldown)
        {
            this.id = id;
            this.category = category;
            this.name = name;
            this.description = description;
            this.weight = weight;
            this.sprite = sprite;
            this.model = model;
            this.maxstack = maxstack;
            this.cooldown = cooldown;
            this.benifits = new Dictionary<int, int>();
        }

        public bool isMisc()
        {
            return this.category == 0;
        }

        public bool isConsumable()
        {
            return this.category == 1;
        }

        public bool isWeapon()
        {
            return this.category == 2;
        }

        public void onDropped(User u)
        {
            // Called when received event of player dropping this item.
            // TODO: Drop ingame

        }

    }
}
