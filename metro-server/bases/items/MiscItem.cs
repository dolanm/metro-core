﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.items
{
    class MiscItem : Item
    {
        public MiscItem(int id, string name, string description, string sprite, string model, int weight, int maxstack = 1) : base(id, 0, name, description, weight, sprite, model, maxstack, -1)
        {

        }
    }
}
