﻿using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.items
{
    public abstract class ConsumableItem : Item
    {
        public ConsumableItem(int id, string name, string description, string sprite, string model, int weight, int maxstack = 1, int cooldown = 10000) : base(id, 1, name, description, weight, sprite, model, maxstack, cooldown)
        {

        }


        public abstract void OnConsumed(User u);

    }
}
