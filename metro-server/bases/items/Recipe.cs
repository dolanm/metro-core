﻿using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.items
{
    public class Recipe
    {
        public int id { get; set; }
        public int category { get; set; }
        public List<InventoryItem> resources { get; set; }
        public InventoryItem output { get; set; }

        public int xpreward { get; set; }

        public Recipe(int id, int category, List<InventoryItem> resources, InventoryItem output, int xpreward)
        {
            this.id = id;
            this.category = category;
            this.resources = resources;
            this.output = output;
            this.xpreward = xpreward;
        }

        public void onCrafted(User u)
        {
            Profile pro = u.GetSelectedProfile();
            if (pro == null)
            {
                return;
            }
            foreach(InventoryItem ni in this.resources)
            {
                pro.RemoveItem(ni);
            }
            pro.AddItem(output.id, output.quantity);
        }

        public Boolean canCraft(User u)
        {
            Profile pro = u.GetSelectedProfile();
            if (pro == null)
            {
                return false;
            }
            foreach (InventoryItem resource in this.resources)
            {
               if(!pro.HasItemAtleast(resource))
                {
                    return false;
                }

            }
            return true;
        }
    }
}
