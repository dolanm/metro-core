﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.items
{
    public class InventoryItem
    {
        public int id { get; set; }
        public int quantity { get; set; }

        public InventoryItem(int id, int quantity)
        {
            this.id = id;
            this.quantity = quantity;
        }

    }
}
