﻿using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.items
{
    public abstract class WeaponItem : Item
    {
        public WeaponItem(int id, String name, String description, String sprite, string model, int weight, int maxstack = 1, int cooldown = 0) : base(id, 2, name, description, weight, sprite, model, maxstack, cooldown)
        {

        }

        public abstract void onEquipt(User u);
    }
}
