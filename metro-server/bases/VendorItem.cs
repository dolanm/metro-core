﻿using CitizenFX.Core;
using metro_server.bases.items;
using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases
{
    public class VendorItem
    {
        public string ItemName { get; set; }
        public int ItemPrice { get; set; } = 0;
        public int ReferenceID { get; set; }

        public VendorItem(string name, int price, int id)
        {
            this.ItemName = name;
            this.ItemPrice = price;
            this.ReferenceID = id;
        }

        public void PurchaseItem(int source)
        {
            if(source != 0)
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if(p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if(u != null)
                    {
                        Profile sP = u.GetSelectedProfile();
                        if(sP != null)
                        {
                            if(sP.Bal_Wallet >= this.ItemPrice)
                            {
                                Item i = Server.INSTANCE.ItemManager.getItem(this.ReferenceID);
                                if (i != null)
                                {
                                    sP.AddItem(new InventoryItem(i.id, 1));
                                    sP.Bal_Wallet = sP.Bal_Wallet - this.ItemPrice;
                                    u.save();
                                    u.sendProfiles();
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^2[VENDOR]", "You have purchased x1 " + this.ItemName + " for $" + this.ItemPrice } });
                                }
                            }
                            else
                            {
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^2[VENDOR]", "You can not afford this item"} });
                            }
                        }
                    }
                }
            }
        }
    }
}
