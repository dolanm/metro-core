﻿using CitizenFX.Core;
using metro_server.bases.users;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases
{
    public class BankVault
    {
        public string VaultName { get; set; }
        public Vector3 VaultTrigger { get; set; }
        public int RobberyLength { get; set; }
        public int Cooldown { get; set; }
        public bool IsCooling { get; set; }
        public int VaultContents { get; set; }
        public int VaultMin { get; set; }
        public bool IsBeingRobbed { get; set; } = false;
        public Player TriggerSource { get; set; }
        public Profile TriggerProfile { get; set; }

        private int LastUpdateTick;
        private int MinutesRobbed;
        private int MinutesCooled;

        public BankVault(string name, Vector3 trigger, int length, int cool, bool isCool, int vaultMoneyMax, int vaultMMoneyMin)
        {
            this.VaultName = name;
            this.VaultTrigger = trigger;
            this.RobberyLength = length;
            this.Cooldown = cool;
            this.IsCooling = isCool;
            this.VaultContents = vaultMoneyMax;
            this.VaultMin = vaultMMoneyMin;

            this.LastUpdateTick = Environment.TickCount;
        }

        public virtual void OnRobberyTriggered()
        {
            Utils.Logging.PrintToConsole("[VaultRobbery] => " + this.TriggerProfile.FirstName + " " + this.TriggerProfile.LastName + " triggered bank robbery at: " + this.VaultName);
            this.TriggerSource.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[ROBBERY]", "You have started robbing the vault, please wait " + this.RobberyLength + " minutes" } });
            Server.INSTANCE.PoliceJob.SendPoliceMessage("^1[ROBBERY]", "The " + this.VaultName + " bank vault is being robbed!");
            this.IsBeingRobbed = true;
            this.TriggerSource.TriggerEvent("metro:CL_E:Robbery:Start", this.VaultTrigger);
        }

        public virtual void OnRobberyCompleted()
        {
            if(this.IsCooling)
            {
                Utils.Logging.PrintToConsole("[VaultRobbery] => Attempted to complete a robbery that is on cooldown: " + this.VaultName);
            }
            else
            {
                Random r = new Random();
                int Payout = r.Next(this.VaultMin, this.VaultContents);
                this.TriggerProfile.Bal_Wallet = this.TriggerProfile.Bal_Wallet + Payout;
                this.TriggerSource.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^2[WALLET]", "You have gained $" + Payout.ToString() } });
                this.IsBeingRobbed = false;
                this.IsCooling = true;
                User tempUser = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(this.TriggerSource.Handle));
                tempUser.save();
                tempUser.sendProfiles();
                this.TriggerSource.TriggerEvent("metro:CL_E:Robbery:Stop");
            }
        }

        public virtual void OnRobberyCanceled()
        {
            if(this.IsCooling)
            {
                Utils.Logging.PrintToConsole("[VaultRobbery] => Attempted to cancel a robbery that is on cooldown: " + this.VaultName);
            }
            else
            {
                this.TriggerSource.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[ROBBERY]", "You have canceled the robbery" } });
                this.TriggerSource.TriggerEvent("metro:CL_E:Robbery:Stop");
                Server.INSTANCE.PoliceJob.SendPoliceMessage("^1[ROBBERY]", "The " + this.VaultName + " bank vault robbery has been cancelled.");
                this.ResetVault();
            }
        }

        private void ResetVault()
        {
            this.IsBeingRobbed = false;
            this.IsCooling = false;
            this.TriggerSource = null;
            this.TriggerProfile = null;
            this.MinutesRobbed = 0;
            this.MinutesCooled = 0;
            Utils.Logging.PrintToConsole("[VaultRobbery] => Reset vault values for vault: " + this.VaultName);
            Utils.Logging.PrintToConsole("[VaultRobbery] => Allowing robbery for vault: " + this.VaultName);
        }

        public void Tick()
        {
            if(this.IsCooling)
            {
                if(this.Cooldown <= this.MinutesCooled)
                {
                    this.ResetVault();
                }else if(Environment.TickCount - this.LastUpdateTick > 60000)
                {
                    this.MinutesCooled = this.MinutesCooled + 1;
                    int TimeLeft = this.Cooldown - this.MinutesCooled;
                    Utils.Logging.PrintToConsole("[VaultRobbery] => " + TimeLeft + " minutes remaining before reset of vault: " + this.VaultName);
                    this.LastUpdateTick = Environment.TickCount;
                }
            }
            if(this.IsBeingRobbed)
            {
                if (this.RobberyLength <= this.MinutesRobbed)
                {
                    this.OnRobberyCompleted();
                }
                else if (Environment.TickCount - this.LastUpdateTick > 60000)
                {
                    this.MinutesRobbed = this.MinutesRobbed + 1;
                    this.TriggerSource.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[ROBBERY]", this.RobberyLength - this.MinutesRobbed + " minutes remaining..." } });
                    this.LastUpdateTick = Environment.TickCount;
                }
            }
        }
    }
}
