﻿using metro_server.utils;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases
{
    public class Mysql
    {
        public MySqlConnection CONNECTION { get; set; }
        public string HOST { get; set; }
        public string DB_NAME { get; set; }
        public string DB_USER { get; set; }
        public string PASSWORD { get; set; }

        public Mysql(String host = "localhost", String database = null)
        {
            this.HOST = host;
            this.DB_NAME = database;
        }

        public void Login(String user = "root", String pass = "")
        {
            this.DB_USER = user;
            this.PASSWORD = pass;
            Utils.Logging.PrintToConsole("[MySQL]: Trying to login using string: server=" + this.HOST + ";user=" + this.DB_USER + ";database=" + this.DB_NAME + ";port=3306;password=" + this.PASSWORD);
            try
            {
                this.CONNECTION = new MySqlConnection("server=" + this.HOST + ";user=" + this.DB_USER + ";database=" + this.DB_NAME + ";port=3306;password=" + this.PASSWORD);
                this.CONNECTION.Open();
            }catch (Exception e)
            {
                Utils.Logging.PrintToConsole("[MySQL]: Error: " + e.ToString());
            }
        }

        public bool isConnected()
        {
            return this.CONNECTION.State == System.Data.ConnectionState.Open || this.CONNECTION.State == System.Data.ConnectionState.Fetching || this.CONNECTION.State == System.Data.ConnectionState.Executing;
        }

        public long Insert(string table, Object[] values)
        {
            if (!this.isConnected())
                return -1;

            String vals = "";
            int cur = 0;
            foreach(Object value in values)
            {
                if((cur + 1) == values.Length)
                {
                    vals += "@value" + cur;
                }
                else
                {
                    vals += "@value" + cur + ", ";
                }
                cur++;
            }
            Utils.Logging.PrintToConsole("[MySQL]: Inserting: INSERT INTO " + table + " VALUES(" + vals + ")");
            MySqlCommand cmd = new MySqlCommand("INSERT INTO " + table + " VALUES(" + vals + ")", this.CONNECTION);

            cur = 0;
            foreach(Object value in values)
            {
                cmd.Parameters.AddWithValue("@value" + cur, value);
                cur++;
            }

            cmd.ExecuteNonQuery();
            return cmd.LastInsertedId;
        }

        public int DeleteByID(string table, int ID)
        {
            if (!this.isConnected())
                return 0;

            MySqlCommand cmd = null;
            if(table != null && table != "")
            {
                cmd = new MySqlCommand("DELETE FROM `" + table + "` WHERE `id` = " + ID, this.CONNECTION);
            }

            return cmd.ExecuteNonQuery();
        }

        public MySqlDataReader Select(String table, Dictionary<String, Object> args = null)
        {
            if (!this.isConnected())
                return null;

            MySqlCommand cmd = null;

            if (args != null && args.Count > 0)
            {
                String vals = "";
                int cur = 1;
                foreach (KeyValuePair<String, Object> entry in args)
                {
                    if (cur == args.Count)
                    {
                        vals += entry.Key + " = @value" + cur;

                    }
                    else
                    {
                        vals += entry.Key + " = @value" + cur + " AND ";

                    }
                    cur++;

                    // do something with entry.Value or entry.Key
                }
                 Utils.Logging.PrintToConsole("Selecting: " + "SELECT * FROM " + table + " WHERE " + vals);

                cmd = new MySqlCommand("SELECT * FROM " + table + " WHERE " + vals, this.CONNECTION);

                cur = 1;
                foreach (KeyValuePair<String, Object> entry in args)
                {
                    cmd.Parameters.AddWithValue("@value" + cur, entry.Value);
                    cur++;
                }
            }
            else
            {


                cmd = new MySqlCommand("SELECT * FROM " + table, this.CONNECTION);


            }


            return cmd.ExecuteReader();


        }

        public int Update(String table, Dictionary<String, Object> set, Dictionary<String, Object> where)
        {
            if (!this.isConnected())
                return 0;

            MySqlCommand cmd = null;



            String sets = "";
            int setcur = 1;
            foreach (KeyValuePair<String, Object> entry in set)
            {
                if (setcur == set.Count)
                {
                    sets += entry.Key + " = @set" + setcur;

                }
                else
                {
                    sets += entry.Key + " = @set" + setcur + ", ";

                }
                setcur++;

                // do something with entry.Value or entry.Key
            }


            String wheres = "";
            int wherecur = 1;
            foreach (KeyValuePair<String, Object> entry in where)
            {
                if (wherecur == where.Count)
                {
                    wheres += entry.Key + " = @where" + wherecur;

                }
                else
                {
                    wheres += entry.Key + " = @where" + wherecur + " AND ";

                }
                wherecur++;

                // do something with entry.Value or entry.Key
            }

            //  Debug.WriteLine("Updating: " + "UPDATE " + table + " SET " + sets + " WHERE " + wheres);
            cmd = new MySqlCommand("UPDATE " + table + " SET " + sets + " WHERE " + wheres, this.CONNECTION);

            setcur = 1;
            foreach (KeyValuePair<String, Object> entry in set)
            {
                cmd.Parameters.AddWithValue("@set" + setcur, entry.Value);
                setcur++;
            }

            wherecur = 1;
            foreach (KeyValuePair<String, Object> entry in where)
            {
                cmd.Parameters.AddWithValue("@where" + wherecur, entry.Value);
                wherecur++;
            }


            return cmd.ExecuteNonQuery();


        }

    }
}
