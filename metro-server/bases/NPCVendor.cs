﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases
{
    public class NPCVendor
    {
        public string VendorTitle { get; set; }
        public Vector3 VendorLocation { get; set; }
        public float DistanceToTrigger { get; set; }
        public List<VendorItem> VendorInventory = new List<VendorItem>();
        public int VendorID { get; set; }

        public NPCVendor(int id, string title, Vector3 location, float triggerDistance)
        {
            this.VendorID = id;
            this.VendorTitle = title;
            this.VendorLocation = location;
            this.DistanceToTrigger = triggerDistance;
        }

        public void AddItem(string name, int price, int id)
        {
            if(name != null && name != "")
            {
                this.VendorInventory.Add(new VendorItem(name, price, id));
            }
        }

        public VendorItem GetItem(string name)
        {
            if(name != null && name != "")
            {
                foreach(VendorItem item in this.VendorInventory)
                {
                    if(item.ItemName == name)
                    {
                        return item;
                    }
                }
            }
            return null;
        }
    }
}
