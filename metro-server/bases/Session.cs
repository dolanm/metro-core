﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using metro_server.utils;
namespace metro_server.bases
{
    public class Session
    {
        public Guid token { get; set; }

        public int user { get; set; }
        public long lastUpdate { get; set; }


        public static Session generate(int userid)
        {
            Session s = new Session();
            s.token = Guid.NewGuid();
            s.user = userid;
            s.lastUpdate = Utils.getCurrentMili();
            return s;
        }

        public void update()
        {
            this.lastUpdate = Utils.getCurrentMili();
        }

        public static Boolean isValid(Guid token, int servid)
        {
            return Server.INSTANCE.SessionManager.isValid(token, servid);
        }

        public static Boolean isValid(Session s, int servid)
        {
            return isValid(s.token, servid);
        }
        
    }
}
