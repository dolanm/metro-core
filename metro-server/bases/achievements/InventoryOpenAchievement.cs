﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using metro_server.bases.users;

namespace metro_server.bases.achievements
{
    class InventoryOpenAchievement : Achievement
    {
        public InventoryOpenAchievement() : base(0, "Inventory Opened!", "Open your inventory", 1)
        {

        }

        public override void onAchieved(User u)
        {
            
        }

        public override void onProgressed(User u)
        {
            throw new NotImplementedException();
        }
    }
}
