﻿using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.achievements
{
    public abstract class Achievement
    {
        public int id { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public int targetValue { get; set; }

        public int currentValue { get; set; }

        public Achievement(int id, string name, string description, int target, int current = 0)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.targetValue = target;
            this.currentValue = current;
        }

        public abstract void onAchieved(User u);

        public void progress(User u)
        {
            if (this.currentValue >= this.targetValue)
                return;

            this.currentValue++;
            if(this.currentValue == this.targetValue)
            {
                this.onAchieved(u);
            }
            else
            {
                this.onProgressed(u);
            }
        }

        public abstract void onProgressed(User u);

    }
}
