﻿using metro_server.bases.factions;
using metro_server.bases.items;
using metro_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.bases.users
{
    public class Profile
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }

        public int gender { get; set; }

        public double height { get; set; }

        public int Bal_Wallet { get; set; } = 0;
        public int Bal_Bank { get; set; } = 0;

        public int Fac_Class { get; set; } = 0;

        public string model { get; set; }

        public long lastModelChange { get; set; }

        public InventoryItem[] inventory { get; set;  } = new InventoryItem[25];

        public Faction CurrentFaction = null;

        public int FactionRank { get; set; } = 0;


        public int LastHealth { get; set; } = 100;
        public int LastArmour { get; set; } = 100;

        public int Hunger { get; set; } = 100;
        public int Thirst { get; set; } = 100;

        public int MaxWeight { get; set; } = 15000;

        public double GetWeight()
        {
            double cw = 0;
            foreach(var i in inventory)
            {
                if(i != null)
                {
                    Item item = Server.INSTANCE.ItemManager.getItem(i.id);
                    var total = item.weight * i.quantity;
                    cw += total;
                }
            }
            return cw;
        }

        public int GetFirstAvailableSlot()
        {
            var curid = 0;
            foreach(InventoryItem item in this.inventory)
            {
                if(item == null)
                {
                    return curid; 
                }
                curid++;
            }
            return -1;
        }

        public Boolean IsFull()
        {
            return this.GetFirstAvailableSlot() == -1;
        }

        public void AddItem(int slot, Item item, int quantity)
        {
            InventoryItem ii = new InventoryItem(item.id, quantity);
            this.inventory[slot] = ii;
        }
        public void AddItem(int slot, int item, int quantity)
        {
            InventoryItem ii = new InventoryItem(item, quantity);
            this.inventory[slot] = ii;
        }
        public void AddItem(Item item, int quantity)
        {
            var avail = this.GetFirstAvailableSlot();
            if(avail != -1)
            {
                this.AddItem(avail, item, quantity);
            }
        }
        public void AddItem(int item, int quantity)
        {
            var avail = this.GetFirstAvailableSlot();
            if (avail != -1)
            {
                this.AddItem(avail, item, quantity);
            }
        }

        public void AddItem(InventoryItem item)
        {
            int[] slots = this.getSlotsWithItem(item.id);

            int toadd = item.quantity;
            foreach(int slot in slots)
            {
                InventoryItem invitem = this.inventory[slot];
                if(invitem != null)
                {
                    Item i = Server.INSTANCE.ItemManager.getItem(invitem.id);
                    if(i != null && i.id == item.id)
                    {
                        if(invitem.quantity < i.maxstack)
                        {
                            int bufferover = (invitem.quantity + item.quantity) - i.maxstack;
                            if(bufferover > 0)
                            {
                                var buf = toadd - bufferover;
                                toadd -= buf;
                                invitem.quantity += buf;
                            }
                            else
                            {
                                invitem.quantity += toadd;
                                return;
                            }
                        }
                    }
                }
            }
            Item tempi = Server.INSTANCE.ItemManager.getItem(item.id);

            while (toadd > 0)
            {

                if(toadd > tempi.maxstack)
                {
                    toadd -= tempi.maxstack;
                    int avail = this.GetFirstAvailableSlot();
                    if(avail == -1)
                    {
                        return;
                    }

                    this.AddItem(tempi.id, tempi.maxstack);
                }
                else
                {
                    this.AddItem(tempi.id, toadd);
                    toadd = 0;

                }
            }
            
            /*if(foundslot == -1)
            {
                int firstavail = this.GetFirstAvailableSlot();
                if (firstavail == -1)
                {
                    return;
                }
                this.AddItem(firstavail, item.id, item.quantity);
            }
            else
            {
                InventoryItem slotitem = this.inventory[foundslot];
                if(slotitem == null)
                {
                    return;
                }

                slotitem.quantity += item.quantity;
            }*/
        }

        public void RemoveItem(InventoryItem item)
        {
            var toremove = item.quantity;
            for(var i = 0; i < this.inventory.Length; i++)
            {
                InventoryItem inventitem = this.inventory[i];
                if(inventitem != null && inventitem.id == item.id)
                {
                    if(inventitem.quantity > toremove)
                    {
                        inventitem.quantity -= toremove;
                        return;
                    }else if(inventitem.quantity == toremove)
                    {
                        this.inventory[i] = null;

                        return;
                    }
                    else
                    {
                        var hasremoved = inventitem.quantity;
                        this.inventory[i] = null;

                        toremove -= hasremoved;
                    }
                }
            }
        }

        public int[] getSlotsWithItem(int id)
        {
            List<int> ids = new List<int>();
            int curslot = 0;
            foreach(InventoryItem item in this.inventory)
            {
                if(item != null && item.id == id)
                {
                    ids.Add(curslot);
                }
                curslot++;
            }
            return ids.ToArray();
        }

        public Boolean HasItemAtleast(InventoryItem item)
        {
            var total = 0;
            foreach(InventoryItem ni in this.inventory)
            {
                if(ni != null && ni.id == item.id)
                {
                    total += ni.quantity;
                }
            }
            return total > 0;
        }

        public Boolean HasItem(string ItemName)
        {
            bool doesExist = false;
            foreach(var i in this.inventory)
            {
                if(i != null)
                {
                    Item item = Server.INSTANCE.ItemManager.getItem(i.id);
                    if(item.name == ItemName)
                    {
                        doesExist = true;
                    }
                }
            }
            return doesExist;
        }

        public void SwapSlots(int from, int to)
        {
            var oldto = this.inventory[to];

            this.inventory[to] = this.inventory[from];

            this.inventory[from] = oldto;

        }

    }
}
