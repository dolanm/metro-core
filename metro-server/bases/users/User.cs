﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_server.bases.factions;
using metro_server.bases.items;
using metro_server.utils;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace metro_server.bases.users
{
    public class User
    {
        public int id { get; set; } = -1;
        public string username { get; set; } = "";
        public string license { get; set; } = "";
        public string steamid { get; set; } = "";

        public List<Profile> AllProfiles { get; set; }
        public int SelectedProfile { get; set; } = -1;

        public int ServerID { get; set; }
        public DateTime InputTime { get; set; }

        public Faction CurrentFaction { get; set; }


        public bool IsStaff { get; set; } = false;


        public User()
        {
            this.AllProfiles = new List<Profile>();
        }

        public User(int id)
        {
            this.AllProfiles = new List<Profile>();
            this.load(id);
        }

        public User(string license)
        {
            this.AllProfiles = new List<Profile>();
            this.license = license;
            this.load(license);
        }

        public bool Exists()
        {
            return this.id != -1;
        }

        public void OnLogin(string username)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");

            this.username = rgx.Replace(username, "?");
            this.save();
        }

        public Profile createProfile(string first, string last, string dob, double height, int gender)
        {


            Profile pr = new Profile();
            pr.FirstName = first;
            pr.LastName = last;
            pr.DOB = dob;
            pr.height = height;
            pr.gender = gender;

            pr.model = Utils.getRandomModel(gender);
            pr.lastModelChange = Utils.getCurrentMili();

            pr.CurrentFaction = Server.INSTANCE.FactionManager.GetFactionByName("civilian");

            pr.Bal_Wallet = 0;
            pr.Bal_Bank = 150;

            this.AllProfiles.Add(pr);
            this.save();
            return pr;
        }

        public void removeProfile(int index)
        {
            this.AllProfiles.RemoveAt(index);
            this.save();
        }

        public void sendMessage(String prefix, String message, int[] color)
        {
            Player p = this.getPlayer();
            if(p != null)
            {
                p.TriggerEvent("chatMessage", prefix, color, message);
            }
        }

        public void sendModels()
        {
            Player p = this.getPlayer();
            if (p != null)
            {
                
                p.TriggerEvent("metro:CL_E:ModelSync", JsonConvert.SerializeObject(Utils.ModelsMale), JsonConvert.SerializeObject(Utils.ModelsFemale));
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Sent Models!");

            }
        }

        public void sendSession(Session ses = null)
        {
            Player p = this.getPlayer();
            if(p != null)
            {
                Session newses = ses;
                if(newses == null)
                {
                    newses = Server.INSTANCE.SessionManager.generate(this.ServerID);
                }
                p.TriggerEvent("metro:CL_E:SessionCreation", newses.token.ToString());
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Sent Session!");

            }
        }

        public void sendProfiles()
        {
            Player p = this.getPlayer();
            if(p != null)
            {
                p.TriggerEvent("metro:CL_E:UserProfiles", JsonConvert.SerializeObject(this.AllProfiles));
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Sent Profiles!");
            }
        }

        public void sendItems()
        {
            Player p = this.getPlayer();
            if (p != null)
            {
                p.TriggerEvent("metro:CL_E:ItemSync", JsonConvert.SerializeObject(Server.INSTANCE.ItemManager.items));
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Sent Items!");
            }
        }

        public void sendRecipes()
        {
            Player p = this.getPlayer();
            if (p != null)
            {
                p.TriggerEvent("metro:CL_E:RecipeSync", JsonConvert.SerializeObject(Server.INSTANCE.RecipeManager.recipes));
                Utils.Logging.PrintToConsole("[" + p.Name.ToString() + "] Sent Recipes!");
            }
        }

        public void sendPlayerlist()
        {
            

            List<dynamic> profs = new List<dynamic>();
            foreach(User u in Server.INSTANCE.UserManager.OnlinePlayers)
            {
                Profile tempp = u.GetSelectedProfile();
               
                if (tempp != null)
                {
                    dynamic temp = new ExpandoObject();
                    temp.id = u.getPlayer().Handle;
                    temp.FirstName = tempp.FirstName;
                    temp.LastName = tempp.LastName;
                    temp.Faction = tempp.CurrentFaction.FactionName;
                    profs.Add(temp);
                }
            }

            this.getPlayer().TriggerEvent("metro:CL_E:PlayerListUpdated", JsonConvert.SerializeObject(profs.ToArray<dynamic>()));
            

        }

        public Player getPlayer()
        {
            return new PlayerList()[this.ServerID];
        }

        public Profile GetSelectedProfile()
        {
            if (this.SelectedProfile == -1)
                return null;
            return this.AllProfiles[this.SelectedProfile];
        }

       

        private void load(object id)
        {
            Dictionary<string, object> paras = new Dictionary<string, object>();
            if(id is int)
            {
                paras.Add("id", id);
            }else if(id is string)
            {
                paras.Add("license", id);
            }

            MySqlDataReader result = Server.INSTANCE.MYSQL.Select("users", paras);
            while(result.Read())
            {
                this.id = result.GetInt32("id");
                this.username = result.GetString("username");
                this.license = result.GetString("license");
                this.steamid = result.GetString("steamid");
                this.IsStaff = result.GetBoolean("isStaff");
                this.AllProfiles = JsonConvert.DeserializeObject<List<Profile>>(result.GetString("profiles"));
                


                this.InputTime = (DateTime)result["inputtime"];
            }
            result.Close();
        }

        public void save()
        {
            if(this.id == -1)
            {
                this.InputTime = DateTime.Now;
                long idd = Server.INSTANCE.MYSQL.Insert("users", new object[] { null, this.username, this.license, this.steamid, JsonConvert.SerializeObject(this.AllProfiles), this.IsStaff, this.InputTime });
                this.id = (int)idd;
                return;
            }
            else
            {
                Dictionary<string, object> tempset = new Dictionary<string, object>();
                tempset.Add("username", this.username);
                tempset.Add("license", this.license);
                tempset.Add("steamid", this.steamid);
                tempset.Add("profiles", JsonConvert.SerializeObject(this.AllProfiles));
                tempset.Add("isStaff", this.IsStaff);
                Dictionary<string, object> tempwhere = new Dictionary<string, object>();
                tempwhere.Add("id", this.id);

                int effected = Server.INSTANCE.MYSQL.Update("users", tempset, tempwhere);
            }
        }
    }
}
