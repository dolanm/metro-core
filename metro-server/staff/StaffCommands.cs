﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_server.bases.items;
using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.staff
{
    public class StaffCommands
    {
        public void RegisterCommands()
        {
            API.RegisterCommand("goto", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if(p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if (u.IsStaff)
                        {
                            if (args.Count > 0)
                            {
                                p.TriggerEvent("metro:CL_E:Staff:Goto", Convert.ToInt32(args[0]));
                            }
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have access to this command." } });
                        }
                    }
                }
            }), false);

            API.RegisterCommand("bring", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if(p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if (u.IsStaff)
                        {
                            if (args.Count > 0)
                            {
                                Player targetP = pl[Convert.ToInt32(args[0].ToString())];
                                if(targetP != null)
                                {
                                    targetP.TriggerEvent("metro:CL_E:Staff:Goto", source);
                                }
                                else
                                {
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid player specified." } });
                                }
                            }
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have access to this command." } });
                        }
                    }
                }
            }), false);

            API.RegisterCommand("giveitem", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if (u.IsStaff)
                        {
                            if (args.Count > 0)
                            {
                                Player targetP = pl[Convert.ToInt32(args[0].ToString())];

                                if (targetP != null)
                                {
                                    User tU = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(targetP.Handle));
                                    if(tU != null)
                                    {
                                        Profile tP = tU.GetSelectedProfile();
                                        if(tP != null)
                                        {
                                            Item i = Server.INSTANCE.ItemManager.getItem(Convert.ToInt32(args[1]));
                                            if(i != null)
                                            {
                                                tP.AddItem(new InventoryItem(i.id, Convert.ToInt32(args[2])));
                                                tU.save();
                                                tU.sendProfiles();
                                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Gave player item." } });
                                            }
                                            else
                                            {
                                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid item ID specified." } });
                                            }
                                        }
                                        else
                                        {
                                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Target player is not using a profile." } });
                                        }
                                    }
                                    else
                                    {
                                        p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Could not get instance of User." } });
                                    }
                                }
                                else
                                {
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid player specified." } });
                                }
                            }
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have access to this command." } });
                        }
                    }
                }
            }), false);

            API.RegisterCommand("achat", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if (u.IsStaff)
                        {
                            if(args.Count > 0)
                            {
                                string FullMsg = "";
                                foreach(string arg in args)
                                {
                                    FullMsg = FullMsg + arg + " ";
                                }
                                foreach(Player oP in pl)
                                {
                                    if(oP != p)
                                    {
                                        User tempuser = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(oP.Handle));
                                        if(tempuser.IsStaff)
                                        {
                                            oP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^8[A]" + p.Name, FullMsg } });
                                        }
                                    }
                                }
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^8[A]" + p.Name, FullMsg } });
                            }
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have access to this command." } });
                        }
                    }
                }
            }), false);

            API.RegisterCommand("a", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if (u.IsStaff)
                        {
                            if (args.Count > 0)
                            {
                                string FullMsg = "";
                                foreach (string arg in args)
                                {
                                    FullMsg = FullMsg + arg + " ";
                                }
                                foreach (Player oP in pl)
                                {
                                    if (oP != p)
                                    {
                                        User tempuser = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(oP.Handle));
                                        if (tempuser.IsStaff)
                                        {
                                            oP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^8[A]" + p.Name, FullMsg } });
                                        }
                                    }
                                }
                                p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^8[A]" + p.Name, FullMsg } });
                            }
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have access to this command." } });
                        }
                    }
                }
            }), false);

            API.RegisterCommand("help", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if(args.Count > 0)
                        {
                            string FullMsg = "";
                            foreach (string arg in args)
                            {
                                FullMsg = FullMsg + arg + " ";
                            }
                            foreach (Player oP in pl)
                            {
                                if (oP != p)
                                {
                                    User tempuser = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(oP.Handle));
                                    if (tempuser.IsStaff)
                                    {
                                        oP.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^6[H](#" + p.Handle + ")" + p.Name, FullMsg } });
                                    }
                                }
                            }
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^6[H](#" + p.Handle + ")" + p.Name, FullMsg } });
                        }
                    }
                }
            }), false);

            API.RegisterCommand("ban", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player p = pl[source];
                if (p != null)
                {
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                    if (u != null)
                    {
                        if (u.IsStaff)
                        {
                            if (args.Count > 0)
                            {
                                Player tP = pl[Convert.ToInt32(args[0])];
                                if(tP != null)
                                {
                                    DateTime BanLength = Convert.ToDateTime(args[1]);
                                    args.RemoveRange(0, 2);
                                    //long idd = Server.INSTANCE.MYSQL.Insert("char_warrants", new object[] { null, FirstName, LastName, FullReason, CopName, inputTime });
                                    string reason = "";
                                    foreach(string arg in args)
                                    {
                                        reason = reason + " " + arg;
                                    }
                                    DateTime IssuedDate = DateTime.Now;
                                    long idd = Server.INSTANCE.MYSQL.Insert("bans", new object[] { null, tP.Name, "license:" + tP.Identifiers["license"], reason, p.Name, BanLength, IssuedDate });
                                    tP.Drop("You have been banned.\r\n------------------------\r\nBanned By: " + p.Name + "\r\nExpiration Date: " + BanLength.ToString() + "\r\nBan Reason: " + reason);
                                }
                                else
                                {
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Invalid target specified." } });
                                }
                            }
                        }
                        else
                        {
                            p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "You do not have access to this command." } });
                        }
                    }
                }
            }), false);
        }
    }
}
