﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.utils
{
    public class Logging
    {



        public void PrintToConsole(string msg)
        {
            if(msg == null || msg == "")
            {
                CitizenFX.Core.Debug.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][METRO-CORE] => Invalid message provided in: Logging.PrintToConsole(string msg)");
            }
            else
            {
                StreamWriter sw = this.getFile();
                sw.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][METRO-CORE] => " + msg);
                sw.Close();
                CitizenFX.Core.Debug.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][METRO-CORE] => " + msg);
            }
        }

        public StreamWriter getFile()
        {
            String name = DateTime.Now.ToString("yyyy-MM-dd");

            return Utils.getFile(name, "log");
        }
    }
}
