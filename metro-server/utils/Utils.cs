﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_server.bases.users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_server.utils
{
    public class Utils
    {
        public static Logging Logging = new Logging();

        public static Dictionary<string, int> FactionClasses = new Dictionary<string, int>();


        public static String[] ModelsMale = new string[] { "u_m_y_abner", "a_m_o_acult_02", "a_m_m_afriamer_01", "ig_mp_agent14", "csb_agent", "s_m_y_airworker", "u_m_m_aldinapoli", "s_m_y_ammucity_01", "s_m_m_ammucountry", "ig_andreas", "cs_andreas", "u_m_y_antonb", "g_m_m_armboss_01", "g_m_m_armgoon_01", "g_m_y_armgoon_02", "g_m_m_armlieut_01", "s_m_m_autoshop_01", "s_m_m_autoshop_02", "ig_money", "g_m_y_azteca_01", "u_m_y_babyd", "g_m_y_ballaeast_01", "g_m_y_ballaorig_01", "ig_ballasog", "csb_ballasog", "g_m_y_ballasout_01", "u_m_m_bankman", "ig_bankman", "s_m_y_barman_01", "ig_barry", "u_m_y_baygor", "s_m_y_baywatch_01", "a_m_m_beach_01", "a_m_o_beach_01", "a_m_y_beach_01", "a_m_m_beach_02", "a_m_y_beach_02", "a_m_y_beach_03", "a_m_y_beachvesp_01", "a_m_y_beachvesp_02", "ig_benny", "ig_bestmen", "ig_beverly", "a_m_m_bevhills_01", "a_m_y_bevhills_01", "a_m_m_bevhills_02", "a_m_y_bevhills_02", "u_m_m_bikehire_01", "s_m_m_bouncer_01", "ig_brad", "a_m_y_breakdance_01", "u_m_y_burgerdrug_01", "s_m_y_busboy_01", "a_m_y_busicas_01", "a_m_m_business_01", "a_m_y_business_01", "a_m_y_business_02", "a_m_y_business_03", "s_m_o_busker_01", "ig_car3guy1", "ig_car3guy2", "cs_carbuyer", "s_m_m_ccrew_01", "s_m_y_chef_01", "ig_chef2", "g_m_m_chiboss_01", "g_m_m_chicold_01", "g_m_m_chigoon_01", "g_m_m_chigoon_02", "csb_chin_goon", "u_m_y_chip", "ig_clay", "ig_claypain", "ig_cletus", "s_m_y_clown_01", "s_m_m_cntrybar_01", "s_m_y_construct_01", "s_m_y_construct_02", "ig_chrisformage", "csb_customer", "u_m_y_cyclist_01", "a_m_y_cyclist_01", "ig_dale", "cs_dale", "ig_davenorton", "cs_davenorton", "s_m_y_dealer_01", "ig_devin", "s_m_y_devinsec_01", "a_m_y_dhill_01", "s_m_m_dockwork_01", "s_m_y_dockwork_01", "cs_dom", "s_m_y_doorman_01", "a_m_y_downtown_01", "ig_dreyfuss", "ig_drfriedlander", "s_m_y_dwservice_01", "a_m_m_eastsa_01", "a_m_y_eastsa_01", "a_m_m_eastsa_02", "a_m_y_eastsa_02", "u_m_m_edtoh", "a_m_y_epsilon_01", "a_m_y_epsilon_02", "mp_m_exarmy_01", "ig_fabien", "s_m_y_factory_01", "g_m_y_famca_01", "mp_m_famdd_01", "g_m_y_famdnf_01", "g_m_y_famfor_01", "a_m_m_farmer_01", "a_m_m_fatlatin_01", "ig_fbisuit_01", "u_m_y_fibmugger_01", "s_m_m_fiboffice_02", "u_m_m_filmdirector", "u_m_o_filmnoir", "u_m_o_finguru_01", "ig_floyd", "csb_fos_rep", "player_one", "ig_g", "s_m_m_gaffer_01", "s_m_y_garbage", "s_m_m_gardener_01", "a_m_y_gay_01", "a_m_y_gay_02", "csb_g", "a_m_m_genfat_01", "a_m_m_genfat_02", "a_m_o_genstreet_01", "a_m_y_genstreet_01", "a_m_y_genstreet_02", "s_m_m_gentransport", "u_m_m_glenstank_01", "a_m_m_golfer_01", "a_m_y_golfer_01", "u_m_m_griff_01", "ig_groom", "csb_grove_str_dlr", "u_m_y_guido_01", "u_m_y_gunvend_01", "hc_hacker", "s_m_m_hairdress_01", "ig_hao", "a_m_m_hasjew_01", "a_m_y_hasjew_01", "s_m_m_highsec_01", "s_m_m_highsec_02", "a_m_y_hiker_01", "a_m_m_hillbilly_01", "a_m_m_hillbilly_02", "u_m_y_hippie_01", "a_m_y_hippy_01", "a_m_y_hipster_01", "a_m_y_hipster_02", "a_m_y_hipster_03", "csb_hugh", "ig_hunter", "csb_imran", "a_m_m_indian_01", "a_m_y_indian_01", "csb_jackhowitzer", "csb_janitor", "ig_jay_norris", "u_m_m_jesus_01", "a_m_y_jetski_01", "u_m_m_jewelsec_01", "u_m_m_jewelthief", "ig_jimmyboston", "ig_jimmydisanto", "ig_joeminuteman", "cs_johnnyklebitz", "ig_josef", "cs_josef", "ig_josh", "a_m_y_juggalo_01", "u_m_y_justin", "g_m_m_korboss_01", "g_m_y_korean_01", "g_m_y_korean_02", "g_m_y_korlieut_01", "a_m_m_ktown_01", "a_m_o_ktown_01", "a_m_y_ktown_01", "a_m_y_ktown_02", "ig_lamardavis", "s_m_m_lathandy_01", "a_m_y_latino_01", "ig_lazlow", "cs_lazlow", "ig_lestercrest", "s_m_m_lifeinvad_01", "s_m_m_linecook", "g_m_y_lost_01", "g_m_y_lost_02", "g_m_y_lost_03", "a_m_m_malibu_01", "u_m_y_mani", "ig_manuel", "s_m_m_mariachi_01", "u_m_m_markfost", "cs_martinmadrazo", "a_m_y_methhead_01", "g_m_m_mexboss_01", "g_m_m_mexboss_02", "a_m_m_mexcntry_01", "g_m_y_mexgang_01", "g_m_y_mexgoon_01", "g_m_y_mexgoon_02", "g_m_y_mexgoon_03", "a_m_m_mexlabor_01", "a_m_y_mexthug_01", "player_zero", "s_m_m_migrant_01", "u_m_y_militarybum", "ig_milton", "s_m_y_mime", "a_m_y_motox_01", "a_m_y_motox_02", "cs_movpremmale", "s_m_m_movprem_01", "mp_g_m_pros_01", "ig_mrk", "a_m_y_musclbeac_01", "a_m_y_musclbeac_02", "ig_nervousron", "ig_nigel", "a_m_m_og_boss_01", "ig_old_man1a", "ig_old_man2", "ig_omega", "ig_oneil", "ig_ortega", "csb_oscar", "a_m_m_paparazzi_01", "u_m_y_paparazzi", "ig_paper", "u_m_y_party_01", "u_m_m_partytarget", "s_m_y_pestcont_01", "g_m_y_pologoon_01", "g_m_y_pologoon_02", "a_m_m_polynesian_01", "a_m_y_polynesian_01", "ig_popov", "csb_porndudes", "s_m_m_postal_01", "s_m_m_postal_02", "ig_priest", "s_m_y_prismuscl_01", "u_m_y_prisoner_01", "s_m_y_prisoner_01", "u_m_y_proldriver_01", "a_m_m_prolhost_01", "u_m_m_promourn_01", "ig_ramp_gang", "csb_ramp_gang", "ig_ramp_hic", "ig_ramp_hipster", "ig_ramp_mex", "csb_ramp_mex", "ig_rashcosvki", "csb_reporter", "u_m_m_rivalpap", "a_m_y_roadcyc_01", "s_m_y_robber_01", "ig_roccopelosi", "a_m_y_runner_01", "a_m_y_runner_02", "a_m_m_rurmeth_01", "ig_russiandrunk", "a_m_m_salton_01", "a_m_o_salton_01", "a_m_y_salton_01", "a_m_m_salton_02", "a_m_m_salton_03", "a_m_m_salton_04", "g_m_y_salvaboss_01", "g_m_y_salvagoon_01", "g_m_y_salvagoon_02", "g_m_y_salvagoon_03", "u_m_y_sbike", "mp_m_shopkeep_01", "s_m_y_shop_mask", "ig_siemonyetarian", "a_m_m_skater_01", "a_m_y_skater_01", "a_m_y_skater_02", "a_m_m_skidrow_01", "a_m_m_socenlat_01", "ig_solomon", "a_m_m_soucent_01", "a_m_o_soucent_01", "a_m_y_soucent_01", "a_m_m_soucent_02", "a_m_o_soucent_02", "a_m_y_soucent_02", "a_m_m_soucent_03", "a_m_o_soucent_03", "a_m_y_soucent_03", "a_m_m_soucent_04", "a_m_y_soucent_04", "u_m_m_spyactor", "u_m_y_staggrm_01", "a_m_y_stbla_01", "a_m_y_stbla_02", "ig_stevehains", "a_m_y_stlat_01", "a_m_m_stlat_02", "ig_stretch", "cs_stretch", "s_m_m_strperf_01", "s_m_m_strpreach_01", "g_m_y_strpunk_01", "g_m_y_strpunk_02", "s_m_m_strvend_01", "s_m_y_strvend_01", "a_m_y_stwhi_01", "a_m_y_stwhi_02", "a_m_y_sunbathe_01", "a_m_y_surfer_01", "ig_talina", "ig_taocheng", "ig_taostranslator", "u_m_o_taphillbilly", "u_m_y_tattoo_01", "a_m_m_tennis_01", "ig_tenniscoach", "ig_terry", "cs_tom", "ig_tomepsilon", "a_m_m_tourist_01", "ig_trafficwarden", "u_m_o_tramp_01", "a_m_m_tramp_01", "a_m_o_tramp_01", "a_m_m_trampbeac_01", "a_m_m_tranvest_01", "player_two", "s_m_m_trucker_01", "ig_tylerdix", "csb_undercover", "s_m_m_ups_01", "s_m_m_ups_02", "mp_m_g_vagfun_01", "ig_vagspeak", "s_m_y_valet_01", "a_m_y_vindouche_01", "a_m_y_vinewood_01", "a_m_y_vinewood_02", "a_m_y_vinewood_03", "a_m_y_vinewood_04", "ig_wade", "s_m_y_waiter_01", "ig_chengsr", "u_m_m_willyfist", "s_m_y_winclean_01", "s_m_y_xmech_01", "s_m_y_xmech_02", "a_m_y_yoga_01", "ig_zimbor", "g_m_importexport_01", "ig_agent", "ig_malc", "mp_m_cocaine_01", "mp_m_counterfeit_01", "mp_m_execpa_01", "mp_m_forgery_01", "mp_m_meth_01", "mp_m_waremech_01", "mp_m_weapexp_01", "mp_m_weapwork_01", "mp_m_weed_01", "s_m_y_xmech_02_mp", "ig_lestercrest_2", "ig_avon" };
        public static String[] ModelsFemale = new string[] { "ig_abigail", "s_f_y_airhostess_01", "ig_amandatownley", "csb_anita", "ig_ashley", "g_f_y_ballas_01", "s_f_y_bartender_01", "a_f_m_beach_01", "a_f_y_beach_01", "a_f_m_bevhills_01", "a_f_y_bevhills_01", "a_f_m_bevhills_02", "a_f_y_bevhills_02", "a_f_y_bevhills_03", "a_f_y_bevhills_04", "u_f_y_bikerchic", "mp_f_boatstaff_01", "a_f_m_bodybuild_01", "ig_bride", "a_f_y_business_01", "a_f_m_business_02", "a_f_y_business_02", "a_f_y_business_03", "a_f_y_business_04", "u_f_y_comjane", "u_f_m_corpse_01", "u_f_y_corpse_02", "cs_debra", "ig_denise", "csb_denise_friend", "a_f_m_downtown_01", "mp_f_deadhooker", "a_f_m_eastsa_01", "a_f_y_eastsa_01", "a_f_m_eastsa_02", "a_f_y_eastsa_02", "a_f_y_eastsa_03", "a_f_y_epsilon_01", "s_f_y_factory_01", "g_f_y_families_01", "a_f_m_fatbla_01", "a_f_m_fatwhite_01", "s_f_m_fembarber", "a_f_y_fitness_01", "a_f_y_fitness_02", "a_f_y_genhot_01", "a_f_o_genstreet_01", "a_f_y_golfer_01", "cs_gurk", "a_f_y_hiker_01", "a_f_y_hippie_01", "a_f_y_hipster_01", "a_f_y_hipster_02", "a_f_y_hipster_03", "a_f_y_hipster_04", "s_f_y_hooker_01", "s_f_y_hooker_02", "s_f_y_hooker_03", "u_f_y_hotposh_01", "a_f_o_indian_01", "a_f_y_indian_01", "ig_janet", "u_f_y_jewelass_01", "ig_jewelass", "cs_jewelass", "a_f_y_juggalo_01", "ig_kerrymcintosh", "a_f_m_ktown_01", "a_f_o_ktown_01", "a_f_m_ktown_02", "g_f_y_lost_01", "ig_magenta", "s_f_m_maid_01", "ig_marnie", "ig_maryann", "ig_maude", "s_f_y_migrant_01", "u_f_m_miranda", "u_f_y_mistress", "ig_molly", "cs_movpremf_01", "u_f_o_moviestar", "s_f_y_movprem_01", "ig_mrsphillips", "ig_mrs_thornhill", "ig_natalia", "ig_paige", "ig_patricia", "u_f_y_poppymich", "u_f_y_princess", "u_f_o_prolhost_01", "a_f_m_prolhost_01", "u_f_m_promourn_01", "a_f_y_runner_01", "a_f_y_rurmeth_01", "a_f_m_salton_01", "a_f_o_salton_01", "a_f_y_scdressy_01", "ig_screen_writer", "s_f_y_scrubs_01", "s_f_m_shop_high", "s_f_y_shop_low", "s_f_y_shop_mid", "a_f_y_skater_01", "a_f_m_skidrow_01", "a_f_m_soucent_01", "a_f_o_soucent_01", "a_f_y_soucent_01", "a_f_m_soucent_02", "a_f_o_soucent_02", "a_f_y_soucent_02", "a_f_y_soucent_03", "a_f_m_soucentmc_01", "u_f_y_spyactress", "csb_stripper_01", "csb_stripper_02", "s_f_m_sweatshop_01", "s_f_y_sweatshop_01", "ig_tanisha", "a_f_y_tennis_01", "ig_tonya", "a_f_y_topless_01", "a_f_m_tourist_01", "a_f_y_tourist_01", "a_f_y_tourist_02", "ig_tracydisanto", "cs_tracydisanto", "a_f_m_tramp_01", "a_f_m_trampbeac_01", "a_m_m_tranvest_02", "g_f_y_vagos_01", "a_f_y_vinewood_01", "a_f_y_vinewood_02", "a_f_y_vinewood_03", "a_f_y_vinewood_04", "a_f_y_yoga_01", "a_f_y_femaleagent", "g_f_importexport_01", "mp_f_cardesign_01", "mp_f_chbar_01", "mp_f_counterfeit_01", "mp_f_execpa_01", "mp_f_execpa_02", "mp_f_forgery_01", "mp_f_meth_01", "mp_f_weed_01" };
        public static String getRandomModel(int gender)
        {
            Random r = new Random();
            if (gender == 0)
            {
                Logging.PrintToConsole("Created Male!");
                return ModelsMale[r.Next(ModelsMale.Length)];
            }
            else
            {
                Logging.PrintToConsole("Created Female!");

                return ModelsFemale[r.Next(ModelsFemale.Length)];
            }
        }

        public static Dictionary<string, string> GetProperPlayerIDs(Player p)
        {
            Dictionary<string, string> ids = new Dictionary<string, string>();
            foreach(string s in p.Identifiers)
            {
                if(s.ToLower().Contains("license:"))
                {
                    ids.Add("license", s);
                }else if (s.ToLower().Contains("steam:"))
                {
                    ids.Add("steam", s);
                }
            }
            return ids;
        }

        public static long getCurrentMili()
        {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        public static StreamWriter getFile(String name, String extension = "txt", Boolean create = true)
        {
            
            String path = API.GetResourcePath(API.GetCurrentResourceName()) + "/" + name + "." + extension;
            if(!File.Exists(path) && !create)
            {
                return null;
            }

            if(File.Exists(path))
            {
                return new StreamWriter(@path, true);
            }
            else
            {
                return File.CreateText(path);

            }

        }


        public static Boolean isValidDOB(string dob)
        {
            String[] parts = dob.Split('-');
            if (parts.Length != 3)
            {
                return false;
            }


            var temp = -1;
            if (!int.TryParse(parts[0], out temp) || parts[0].Length != 4)
            {
                return false;
            }

            if (!int.TryParse(parts[1], out temp) || parts[1].Length != 2)
            {
                return false;
            }

            if (!int.TryParse(parts[2], out temp) || parts[2].Length != 2)
            {
                return false;
            }

            return true;
        }

        public static void UpdatePlayerLists()
        {
            foreach(User u in Server.INSTANCE.UserManager.OnlinePlayers)
            {
                if(u.GetSelectedProfile() != null)
                {
                    u.sendPlayerlist();
                }
            }
        }

    }
}
