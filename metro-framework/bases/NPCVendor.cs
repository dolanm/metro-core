﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases
{
    public class NPCVendor
    {
        public string VendorTitle { get; set; }
        public Vector3 VendorLocation { get; set; }
        public float DistanceToTrigger { get; set; }

        public float VendorHeading { get; set; }

        public int VendorID { get; set; }

        public string VendorModel { get; set; }
        public Ped Vendor = null;

        public bool IsHidden { get; set; }

        public BlipSprite VendorIcon { get; set; }

        public Blip VendorBlip { get; set; }

        public NPCVendor(int id, string title, Vector3 location, float heading, float triggerDistance, string model, bool hide, BlipSprite blipIcon = 0)
        {
            this.VendorID = id;
            this.VendorTitle = title;
            this.VendorLocation = location;
            this.VendorHeading = heading;
            this.DistanceToTrigger = triggerDistance;
            this.VendorModel = model;
            this.IsHidden = hide;
            this.VendorIcon = blipIcon;

            CitizenFX.Core.Debug.WriteLine("CREATED VENDOR: " + this.VendorTitle + ", " + this.VendorID + ", " + this.VendorModel);
        }
    }
}
