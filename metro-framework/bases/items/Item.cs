﻿using CitizenFX.Core;
using metro_framework.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases
{
    public class Item
    {

        public int id { get; set; }
        public int category { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public int weight { get; set; }

        public string sprite { get; set; }

        public bool stackable { get; set; }

        public int cooldown { get; set; }

        public Dictionary<int, int> benifits { get; set; }

        public Item()
        {
            this.benifits = new Dictionary<int, int>();
        }

        public bool isMisc()
        {
            return this.category == 0;
        }

        public bool isConsumable()
        {
            return this.category == 1;
        }

        public bool isWeapon()
        {
            return this.category == 2;
        }

        public void onInteract()
        {
            BaseScript.TriggerServerEvent("metro:SV_E:inventory:interact", Utils.SessionToken.ToString(), this.id);

        }

        public void onDrop()
        {
            BaseScript.TriggerServerEvent("metro:SV_E:dropitem", Utils.SessionToken.ToString(), this.id);
        }
    }
}
