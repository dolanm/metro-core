﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.items
{
    public class Recipe
    {
        public int id { get; set; }
        public int category { get; set; }
        public List<InventoryItem> resources { get; set; }
        public InventoryItem output { get; set; }

        public int xpreward { get; set; }
    }
}
