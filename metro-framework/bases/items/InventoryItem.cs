﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.items
{
    public class InventoryItem
    {
        public int id { get; set; }
        public int quantity { get; set; }
    }
}
