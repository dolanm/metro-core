﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.factions
{
    public class EMS : Faction
    {
        public EMS(string name, int id, Vector3 spawn, bool usespawn) : base(name, id, spawn, usespawn) { }
    }
}
