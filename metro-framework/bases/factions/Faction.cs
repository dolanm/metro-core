﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.factions
{
    public class Faction
    {
        public string FactionName { get; set; }
        public int FactionID { get; set; }
        public Vector3 FactionSpawn { get; set; }
        public List<Player> OnlineMembers { get; set; }
        public bool UseSpawn { get; set; }

        public Faction(string name, int id, Vector3 spawn, bool usespawn)
        {
            this.FactionName = name;
            this.FactionID = id;
            this.FactionSpawn = spawn;
            this.OnlineMembers = new List<Player>();
            this.UseSpawn = usespawn;
        }
    }
}
