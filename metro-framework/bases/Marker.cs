﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases
{
    public class Marker
    {
        public Vector3 Postion { get; set; }
        public float Scale { get; set; }
        public System.Drawing.Color Color { get; set; }
        public Action<Marker> MarkerTask { get; set; }
        public Blip MarkerBlip { get; set; }

        public Marker(Vector3 position, Vector3 rotation, float scale, System.Drawing.Color color, Action<Marker> callback)
        {
            this.Postion = position;
            this.Scale = scale;
            this.Color = color;
            this.MarkerTask = callback;
        }

        public void SetBlip(string name, BlipSprite sprite, BlipColor color)
        {
            if(this.MarkerBlip != null)
            {
                this.MarkerBlip.Delete();
            }

            this.MarkerBlip = World.CreateBlip(this.Postion);
            this.MarkerBlip.Sprite = sprite;
            this.MarkerBlip.Color = color;
            this.MarkerBlip.Name = name;
            this.MarkerBlip.IsShortRange = true;
        }

        public virtual void OnWalkOver(Marker m)
        {
            this.MarkerTask(m);
        }

        public async Task Tick()
        {
            if(Game.PlayerPed.Position.DistanceToSquared(this.Postion) <= 1.5f)
            {
                this.OnWalkOver(this);
            }

            World.DrawMarker(MarkerType.VerticalCylinder, this.Postion, new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(this.Scale, this.Scale, this.Scale), this.Color);
        }

        public void DestroyMarker()
        {
            if (this.MarkerBlip != null)
                this.MarkerBlip.Delete();
        }
    }
}
