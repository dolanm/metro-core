﻿using metro_framework.bases.factions;
using metro_framework.bases.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.users
{
    public class Profile
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }

        public int gender { get; set; }

        public double height { get; set; }

        public int Bal_Wallet { get; set; } = 0;
        public int Bal_Bank { get; set; } = 0;

        public int Fac_Class { get; set; } = 0;

        public Faction CurrentFaction = null;

        public int FactionRank { get; set; } = 0;

        public string model { get; set; }

        public long lastModelChange { get; set; }

        public InventoryItem[] inventory { get; set; } = new InventoryItem[25];


        public int LastHealth { get; set; }
        public int LastArmour { get; set; }

        public int Hunger { get; set; }
        public int Thirst { get; set; }

        public int MaxWeight { get; set; }

    }
}
