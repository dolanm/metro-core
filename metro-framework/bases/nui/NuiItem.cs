﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.nui
{
    public class NuiItem
    {
        public int id { get; set; }
        public int category { get; set; }
        public String name { get; set; }

        public String description { get; set; }

        public Dictionary<int, int> benifits { get; set; }

        public int weight { get; set; }

        public int cooldown { get; set; }

        public int quantity { get; set; }

        public String sprite { get; set; }
    }
}
