﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.nui
{
    public class NuiPacket
    {

        public String type { get; set; }

        public Dictionary<String, Object> values { get; set; }

        public NuiPacket(String type, params PacketValue[] values)
        {
            this.type = type;
            this.values = new Dictionary<string, object>();

            foreach(PacketValue pv in values)
            {
                this.values.Add(pv.key, pv.value);
            }
        }

        public String toJson()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}
