﻿using CitizenFX.Core;
using metro_framework.bases.items;
using metro_framework.utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.nui
{
    public class NuiInventory
    {

        public NuiItem[] items = new NuiItem[25];

        public NuiInventory(InventoryItem[] invent)
        {
            var curid = 0;
            foreach(InventoryItem item in invent)
            {
                if(item != null)
                {
                    Item normi = ItemUtils.getItem(item.id);
                    if (normi != null)
                    {
                        NuiItem newitem = new NuiItem();
                        newitem.id = normi.id;
                        newitem.category = normi.category;
                        newitem.name = normi.name;
                        newitem.description = normi.description;
                        newitem.benifits = normi.benifits;
                        newitem.weight = normi.weight;
                        if(normi.cooldown > 0)
                        {
                            newitem.cooldown = normi.cooldown / 1000;
                        }
                        else
                        {
                            newitem.cooldown = -1;
                        }
                        
                        newitem.quantity = item.quantity;
                        newitem.sprite = normi.sprite;

                        this.items[curid] = newitem;
                    }
                }
               

                curid++;
                
            }

        }

        public String toJson()
        {
            return JsonConvert.SerializeObject(this.items);
        }
    }
}
