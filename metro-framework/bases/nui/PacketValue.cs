﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.bases.nui
{
    public class PacketValue
    {
        public String key { get; set; }

        public object value { get; set; }

        public PacketValue(String key, object value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
