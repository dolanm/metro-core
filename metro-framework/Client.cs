﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.bases;
using metro_framework.bases.factions;
using metro_framework.bases.items;
using metro_framework.bases.nui;
using metro_framework.bases.users;
using metro_framework.jobs.ticked.police;
using metro_framework.managers;
using metro_framework.menus;
using metro_framework.utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace metro_framework
{
    public class Client : BaseScript
    {
        /*EventHandlers["playerSpawned"] += new Action<dynamic>(async (d) =>
            {
                API.SetPedDefaultComponentVariation(API.GetPlayerPed(-1));
                BaseScript.TriggerServerEvent("epoch:server:spawn", LocalPlayer.ServerId);

            });*/

        public static Client INSTANCE { get; set; }

        public MenuManager MenuManager = new MenuManager();
        public FactionManager FactionManager = new FactionManager();
        public StaffManager StaffManager = new StaffManager();
        public DevKitManager DevKitManager = new DevKitManager();
        public RadioManager RadioManager = new RadioManager();
        public BankManager BankManager = new BankManager();
        public VendorManager VendorManager = new VendorManager();
        public MapManager MapManager = new MapManager();
        public RobberyManager RobberyManager = new RobberyManager();

        #region Ticked Jobs
        public PoliceMain PoliceMain = new PoliceMain();
        #endregion

        public Client()
        {
            Client.INSTANCE = this;


            EventHandlers.Add("onClientMapStart", new Action(this.SpawnCallback));
            EventHandlers.Add("onClientResourceStart", new Action<string>(this.handleResourceReady));
            EventHandlers.Add("metro:CL_E:ServerReady", new Action(this.handleServerReady));
            EventHandlers.Add("metro:CL_E:SessionCreation", new Action<String>(this.handleSessionCreation));

            EventHandlers.Add("metro:CL_E:UserProfiles", new Action<String>(this.handleUserProfiles));
            EventHandlers.Add("metro:CL_E:ProfileCreated", new Action(this.handleProfileCreated));
            EventHandlers.Add("metro:CL_E:ProfileDeleted", new Action(this.handleProfileDeleted));
            EventHandlers.Add("metro:CL_E:ProfileSelected", new Action<int>(this.handleProfileSelected));

            EventHandlers.Add("metro:CL_E:ModelSync", new Action<String, String>(this.handleModelSync));
            EventHandlers.Add("metro:CL_E:ItemSync", new Action<String>(this.handleItemSync));
            EventHandlers.Add("metro:CL_E:RecipeSync", new Action<String>(this.handleRecipeSync));
            EventHandlers.Add("metro:CL_E:InventoryUpdated", new Action(this.handleInventoryUpdated));

            EventHandlers.Add("metro:CL_E:FactionUpdated", new Action<string>(this.HandleUpdatedFaction));

            EventHandlers.Add("metro:CL_E:PlayerListUpdated", new Action<String>(this.handlePlayerListUpdated));

            EventHandlers.Add("metro:CL_E:LaLa:DEV:OpenKit", new Action(this.OpenDevKit));

            EventHandlers.Add("metro:CL_E:Radio:SetFreq", new Action<string>(this.RadioManager.SetFrequency));
            EventHandlers.Add("metro:CL_E:Radio:Send", new Action<string>(this.RadioManager.SendMessage));
            EventHandlers.Add("metro:CL_E:Radio:Recieve", new Action<double, string>(this.RadioManager.RecieveMessage));

            EventHandlers.Add("metro:CL_E:Robbery:Start", new Action<Vector3>(this.BankManager.StartRobbery));
            EventHandlers.Add("metro:CL_E:Robbery:Stop", new Action(this.BankManager.CleanupRobbery));

            EventHandlers.Add("metro:CL_E:SendLocalChat", new Action<string, string>(this.SendLocalChat));
            EventHandlers.Add("metro:CL_E:DisplayLocalChat", new Action<string, string, Vector3>(this.ReceiveLocalChat));

            API.DisplayRadar(false);
            API.SetRadarBigmapEnabled(false, false);



            #region Staff Events
            EventHandlers.Add("metro:CL_E:Staff:Goto", new Action<int>(this.StaffManager.HandleGoto));
            #endregion



            #region Nui Events

            this.RegisterNUICallback("metro:nui:inventory:swap", (data, callback) =>
            {
                BaseScript.TriggerServerEvent("metro:SV_E:inventory:swap", Utils.SessionToken.ToString(), data.from, data.to);
            });

            this.RegisterNUICallback("metro:nui:inventory:interact", (data, callback) =>
            {
                BaseScript.TriggerServerEvent("metro:SV_E:inventory:interact", Utils.SessionToken.ToString(), data.item);
            });

            this.RegisterNUICallback("metro:nui:createprofile", (data, callback) =>
            {

                if(Utils.profiles.Count == 3)
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Max Profiles"), new PacketValue("text", "You cannot make any more profiles")).toJson());
                    return;
                }

                var first = data.first;
                var last = data.last;

                var dob = data.dob;
                var height = data.height;

                var gender = data.gender;

                


                if(first.Length < 2)
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "First Name"), new PacketValue("text", "Must be atleast 2 character")).toJson());
                    return;
                }

                if(last.Length < 2)
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Last Name"), new PacketValue("text", "Must be atleast 2 character")).toJson());
                    return;
                }
                if(!Utils.isValidDOB(dob))
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Date Of Birth"), new PacketValue("text", "Does not match format, please try again")).toJson());
                    return;
                }

                double newheight = -1;
                if(!double.TryParse(height, out newheight))
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Height"), new PacketValue("text", "Invalid height, please try again")).toJson());
                    return;
                }

                if (newheight < 4 || newheight > 7)
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Height"), new PacketValue("text", "Invalid height, please try again")).toJson());
                    return;
                }

                if(gender != 0 && gender != 1)
                {
                    gender = 0;
                }


                BaseScript.TriggerServerEvent("metro:SV_E:CreateProfile", Utils.SessionToken.ToString(), first, last, dob, newheight, gender);
            });

            this.RegisterNUICallback("metro:nui:deleteprofile", (data, callback) =>
            {
                var id = data.id;
                if(id < 0 || id > 3)
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Invalid Profile"), new PacketValue("text", "Please try again")).toJson());
                    return;
                }

                BaseScript.TriggerServerEvent("metro:SV_E:DeleteProfile", Utils.SessionToken.ToString(), id);
            });

            this.RegisterNUICallback("metro:nui:selectprofile", (data, callback) =>
            {
                var id = data.id;
                if (id < 0 || id > 3)
                {
                    API.SendNuiMessage(new NuiPacket("alert", new PacketValue("type", "error"), new PacketValue("title", "Invalid Profile"), new PacketValue("text", "Please try again")).toJson());
                    return;
                }

                //this.handleProfileSelected(id);

                BaseScript.TriggerServerEvent("metro:SV_E:SelectProfile", Utils.SessionToken.ToString(), id);
            });

            this.RegisterNUICallback("metro:nui:unfocus", (data, callback) =>
            {
                API.SetNuiFocus(false, false);

            });
            this.RegisterNUICallback("metro:nui:openprofiles", (data, callback) =>
            {
                API.SendNuiMessage(new NuiPacket("openui", new PacketValue("page", "profileSelection"), new PacketValue("profiles", Utils.profiles), new PacketValue("males", Utils.ModelsMale.Length), new PacketValue("females", Utils.ModelsFemale.Length)).toJson());
                API.DisableControlAction(1, 200, true);
            });
            this.RegisterNUICallback("metro:nui:setcursor", (data, callback) =>
            {
                API.SetCursorSprite(data.id);

            });
            this.RegisterNUICallback("metro:nui:previewmodel", (data, callback) => {
                var gender = data.gender;
                var id = data.id;

                if(gender == 0)
                {
                    Utils.SetPlayerModel(Utils.ModelsMale[id]);
                    Debug.WriteLine("set model");
                }
                else
                {
                    Utils.SetPlayerModel(Utils.ModelsFemale[id]);
                    Debug.WriteLine("set model");

                }

            });


            this.RegisterNUICallback("metro:nui:playsound", (data, callback) =>
            {
                // API.SetNuiFocus(false, false);
               
                API.PlaySoundFrontend(-1, data.sound, data.set, true);

            });
            #endregion


            #region Register Classes
            Utils.FactionClasses.Add("civilian", 0);
            Utils.FactionClasses.Add("ems", 1);
            Utils.FactionClasses.Add("police", 2);

            this.FactionManager.RegisterFaction(new Civilian("civilian", 0, new Vector3(-207.0937f, -1015.908f, 30.13814f), true));
            this.FactionManager.RegisterFaction(new EMS("ems", 1, new Vector3(0.0f, 0.0f, 0.0f), false));
            this.FactionManager.RegisterFaction(new Police("police", 2, new Vector3(460.16f, -990.82f, 30.69f), true));

            #endregion

            #region Register Commands
            API.RegisterCommand("inventory", new Action<int, List<object>, string>((source, args, raw) => {
                if(Utils.getSelectedProfile() == null)
                {
                    BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "You have not selected a profile yet");
                    return;
                }
                NuiInventory ni = new NuiInventory(Utils.getSelectedProfile().inventory);
                

                API.SendNuiMessage(new NuiPacket("openui", new PacketValue("page", "inventory"), new PacketValue("inventory", ni.items)).toJson());
                API.SetNuiFocus(true, true);
            }), false);

            API.RegisterCommand("craft", new Action<int, List<object>, string>((source, args, raw) => {
                if (Utils.getSelectedProfile() == null)
                {
                    BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "You have not selected a profile yet");
                    return;
                }
                NuiInventory ni = new NuiInventory(Utils.getSelectedProfile().inventory);


                API.SendNuiMessage(new NuiPacket("openui", new PacketValue("page", "crafting")).toJson());
                API.SetNuiFocus(true, true);
            }), false);
            /*API.RegisterCommand("profiles", new Action<int, List<object>, string>((source, args, raw) => {
               

                API.SendNuiMessage(new NuiPacket("openui", new PacketValue("page", "nprofileSelection")).toJson());
                API.SetNuiFocus(true, true);
            }), false);*/

            API.RegisterCommand("hat", new Action<int, List<object>, string>((source, args, raw) => {
                if (Utils.getSelectedProfile() == null)
                {
                    BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "You have not selected a profile yet");
                    return;
                }
                
                if(Utils.getSelectedProfile().CurrentFaction.FactionName == "police")
                {
                    Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, 0, 0, true);
                }
                else
                {
                    BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "You do not have access to this command.");
                    return;
                }
            }), false);

            API.RegisterCommand("invent", new Action<int, List<object>, string>((source, args, raw) => {
                if (Utils.getSelectedProfile() == null)
                {
                    BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "You have not selected a profile yet");
                    return;
                }
                NuiInventory ni = new NuiInventory(Utils.getSelectedProfile().inventory);

                API.SendNuiMessage(new NuiPacket("openui", new PacketValue("page", "inventory"), new PacketValue("inventory", ni.items)).toJson());
                API.SetNuiFocus(true, true);
            }), false);
            API.RegisterCommand("gay", new Action<int, List<object>, string>((source, args, raw) => {
                /*Ped CharacterPed = new Ped(API.CreatePed(0, (uint)API.GetHashKey("s_m_m_postal_01"), 409.76f, -1002.15f, -99.00f, 4.79f, false, true));

                CharacterPed.Task.RunTo(new Vector3(409.53f, -998.49f, -99.00f), true, -1);*/

                //this.MenuManager.ProfileMenu.Visible = !this.MenuManager.ProfileMenu.Visible;
                BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "Frequency: " + this.RadioManager.CurrentFreq);
            }), false);


            API.RegisterCommand("closestmodel", new Action<int, List<object>, string>((source, args, raw) => {
                /*Ped CharacterPed = new Ped(API.CreatePed(0, (uint)API.GetHashKey("s_m_m_postal_01"), 409.76f, -1002.15f, -99.00f, 4.79f, false, true));

                CharacterPed.Task.RunTo(new Vector3(409.53f, -998.49f, -99.00f), true, -1);*/

                //this.MenuManager.ProfileMenu.Visible = !this.MenuManager.ProfileMenu.Visible;

                Prop[] AllProps = World.GetAllProps();
                foreach(Prop p in AllProps)
                {
                    if(Vector3.DistanceSquared(Game.PlayerPed.Position, p.Position) <= 2.0f)
                    {
                        BaseScript.TriggerEvent("chatMessage", "Server", new int[] { 150, 150, 150 }, "Prop Name: " + p.Model + " | Hash: " + p.Model.Hash.ToString());
                    }
                }
            }), false);


            #endregion

            #region Ticks
            Tick += this.OnTick;
            #endregion
        }


        private void ReceiveLocalChat(string prefix, string msg, Vector3 pos)
        {
            if(Game.PlayerPed.Position.DistanceToSquared(pos) <= 25.0f)
            {
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { prefix, msg } });
            }
        }

        private void SendLocalChat(string name, string msg)
        {
            string newName = Utils.getSelectedProfile().FirstName + " " + Utils.getSelectedProfile().LastName;
            BaseScript.TriggerServerEvent("metro:SV_E:SendLocalChat", "^5[LOCAL] " + newName, msg, Game.PlayerPed.Position);
        }

        #region COMMAND TOOLTIPS
        private void RegisterCommandTooltips()
        {
            TriggerEvent("chat:addSuggestion", "/inventory", "Allows you to view your inventory");
            TriggerEvent("chat:addSuggestion", "/craft", "Allows you to view the crafting menu");
            TriggerEvent("chat:addSuggestion", "/invent", "Allows you to view your inventory");
            TriggerEvent("chat:addSuggestion", "/hat", "Toggles your hat");
            TriggerEvent("chat:addSuggestion", "/finfo", "Prints your current faction info");
            TriggerEvent("chat:addSuggestion", "/devkit", "Displays the developer menu");
            TriggerEvent("chat:addSuggestion", "/setstaff", "Changes target's staff access", new[]
            {
                new {name="target", help="Target player's server ID"},
                new {name="operator", help="true/false"},
            });
            TriggerEvent("chat:addSuggestion", "/setclass", "Changes target's faction class", new[]
            {
                new {name="target", help="Target player's server ID"},
                new {name="class_name", help="civilian, police, ems"},
            });
            TriggerEvent("chat:addSuggestion", "/setfacrank", "Changes target's faction rank", new[]
            {
                new {name="target", help="Target player's server ID"},
                new {name="rank", help="Typically starts from 0"},
            });
            TriggerEvent("chat:addSuggestion", "/rfreq", "Changes your radio frequency", new[]
            {
                new {name="freq", help="Number from 000.0 to 999.9"},
            });
            TriggerEvent("chat:addSuggestion", "/radio", "Allows you to communicate over your radio", new[]
            {
                new {name="msg", help="Message to be sent over the radio"},
            });
            TriggerEvent("chat:addSuggestion", "/r", "Allows you to communicate over your radio", new[]
            {
                new {name="msg", help="Message to be sent over the radio"},
            });
            TriggerEvent("chat:addSuggestion", "/warrant", "Allows for viewing, creating and deleting warrants", new[]
            {
                new {name="operator", help="list/create/del"},
                new {name="arg1", help="Warrant ID(for deleting)/Target's first name"},
                new {name="arg2", help="Target's last name"},
                new {name="arg3", help="Warrant reason"},
            });
            TriggerEvent("chat:addSuggestion", "/goto", "Teleport to a player", new[]
            {
                new {name="target", help="Target player"},
            });
            TriggerEvent("chat:addSuggestion", "/goto", "Bring a player to you", new[]
            {
                new {name="target", help="Target player"},
            });
            TriggerEvent("chat:addSuggestion", "/giveitem", "Add items to a player's inventory", new[]
            {
                new {name="target", help="Target player"},
                new {name="itemId", help="Item ID"},
                new {name="itemCount", help="Item count"},
            });
            TriggerEvent("chat:addSuggestion", "/achat", "Send a message in the admin chat", new[]
            {
                new {name="msg", help="Message"},
            });
            TriggerEvent("chat:addSuggestion", "/a", "Send a message in the admin chat", new[]
            {
                new {name="msg", help="Message"},
            });
            TriggerEvent("chat:addSuggestion", "/help", "Request admin assistance", new[]
            {
                new {name="msg", help="Message"},
            });
            TriggerEvent("chat:addSuggestion", "/ban", "Ban a player from the server", new[]
            {
                new {name="target", help="Target player"},
                new {name="length", help="Expiration date (ex: yyyy-mm-dd)"},
                new {name="reason", help="Ban reason"},
            });
        }
        #endregion

        private void HandleUpdatedFaction(string name)
        {
            if(name == "police")
            {
                if(!this.PoliceMain.HasInitiated)
                {
                    this.PoliceMain.Init();
                    Tick += new Func<Task>(async delegate
                    {
                        this.PoliceMain.StationManager.Tick();
                    });
                }
            }
        }

        private void OpenDevKit()
        {
            this.MenuManager.GetDevKit().Visible = true;
        }

        private void handleResourceReady(String name)
        {
            if (API.GetCurrentResourceName() != name) return;

            Debug.WriteLine("Sending ready...");
            BaseScript.TriggerServerEvent("metro:SV_E:PlayerReady");
        }

        private void SpawnCallback()
        {
            Function.Call(Hash.SET_PED_DEFAULT_COMPONENT_VARIATION, Game.PlayerPed);
            BaseScript.TriggerServerEvent("metro:SV_E:PlayerSpawnedInWorld");

            
        }

        private void handleInventoryUpdated()
        {
            NuiInventory ni = new NuiInventory(Utils.getSelectedProfile().inventory);
            API.SendNuiMessage(new NuiPacket("inventoryupdate", new PacketValue("inventory", ni.items)).toJson());
        }

        private void handleProfileCreated()
        {
            API.SendNuiMessage(new NuiPacket("profilecreated").toJson());
            this.handleProfileSelected(Utils.profiles.Count - 1);
        }

        private void handleProfileDeleted()
        {
            API.SendNuiMessage(new NuiPacket("profiledeleted", new PacketValue("profiles", Utils.profiles)).toJson());

        }

        private void handleProfileSelected(int index)
        {
            API.DisableControlAction(1, 200, false);
            Utils.selected_profile = index;
            Profile p = Utils.getSelectedProfile();
            if(p != null)
            {
                Utils.SetPlayerModel(p.model);
                Debug.WriteLine("model set!" );

                if(p.CurrentFaction.UseSpawn)
                {
                    if(p.CurrentFaction.FactionName == "police")
                    {
                        switch(p.FactionRank)
                        {
                            case 0:
                                p.CurrentFaction.FactionSpawn = new Vector3(460.16f, -990.82f, 30.69f);
                                break;
                            case 1:
                                p.CurrentFaction.FactionSpawn = new Vector3(1850.899f, 3686.193f, 34.26707f);
                                break;
                            case 2:
                                p.CurrentFaction.FactionSpawn = new Vector3(835.9557f, -1290.78f, 28.21111f);
                                break;
                        }
                    }



                    Game.PlayerPed.Position = p.CurrentFaction.FactionSpawn;
                }

                if(p.CurrentFaction.FactionName == "police")
                {
                    this.PoliceMain.Init();
                    Tick += new Func<Task>(async delegate
                    {
                        this.PoliceMain.StationManager.Tick();
                    });
                    API.SetPedMoveRateOverride(Game.PlayerPed.Handle, 0.15f);
                }
                else
                {
                    API.SetPedMoveRateOverride(Game.PlayerPed.Handle, 0.10f);
                }
            }
            else
            {
                Debug.WriteLine("no profile: " + index);
            }
            NuiInventory ni = new NuiInventory(Utils.getSelectedProfile().inventory);
            API.SendNuiMessage(new NuiPacket("inventoryupdate", new PacketValue("inventory", ni.items)).toJson());
            API.SendNuiMessage(new NuiPacket("closeui").toJson());
            API.SetNuiFocus(false, false);
            this.RegisterCommandTooltips();

            this.VendorManager.AddVendors();

            Tick += new Func<Task>(async delegate
            {
                this.BankManager.Tick();
                this.VendorManager.Tick();
                this.MapManager.Tick();
                this.RobberyManager.TickRobberies();
            });

            this.VendorManager.CreateNPCS();
            this.BankManager.AddBankBlips();

            //this.VendorManager.CreateNPCS();
            this.VendorManager.CreateBlips();
        }

        private void handlePlayerListUpdated(string list)
        {
            API.SendNuiMessage(new NuiPacket("playerListUpdate", new PacketValue("playerList", JsonConvert.DeserializeObject<dynamic[]>(list))).toJson());
        }


        private void handleSessionCreation(String token)
        {

            Utils.SessionToken = Guid.Parse(token);
            Debug.WriteLine("Received Session");

        }

        private void handleItemSync(String json)
        {
            ItemUtils.items = JsonConvert.DeserializeObject<List<Item>>(json);
            Debug.WriteLine("Received " + ItemUtils.items.Count() + " items!");
        }

        private void handleModelSync(String males, String females)
        {
            Utils.ModelsMale = JsonConvert.DeserializeObject<String[]>(males);
            Debug.WriteLine("Received " + Utils.ModelsMale.Count() + " Male Models!");

            Utils.ModelsFemale = JsonConvert.DeserializeObject<String[]>(females);
            Debug.WriteLine("Received " + Utils.ModelsFemale.Count() + " Female Models!");
        }

        private void handleRecipeSync(String json)
        {
            ItemUtils.recipes = JsonConvert.DeserializeObject<List<Recipe>>(json);
            Debug.WriteLine("Received " + ItemUtils.recipes.Count() + " recipes!");
        }

        private void handleUserProfiles(String json)
        {
            Utils.profiles = JsonConvert.DeserializeObject<List<Profile>>(json);
            API.SendNuiMessage(new NuiPacket("profiles", new PacketValue("page", "profileSelection"), new PacketValue("profiles", Utils.profiles)).toJson());
            if(Utils.getSelectedProfile() != null)
            {
                NuiInventory ni = new NuiInventory(Utils.getSelectedProfile().inventory);
                API.SendNuiMessage(new NuiPacket("inventoryupdate", new PacketValue("inventory", ni.items)).toJson());
            }
            
            Debug.WriteLine("Received profiles: " + Utils.profiles.Count);
        }

        private void handleServerReady()
        {
            API.SendNuiMessage(new NuiPacket("openui", new PacketValue("page", "profileSelection"), new PacketValue("profiles", Utils.profiles), new PacketValue("males", Utils.ModelsMale.Length), new PacketValue("females", Utils.ModelsFemale.Length)).toJson());
            API.SetNuiFocus(true, true);
        }



        public void RegisterEventHandler(string _event, Delegate _action)
        {
            try
            {
                EventHandlers.Add(_event, _action);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
            }
        }

        public void UnregisterEventHandler(string _event, Delegate _action)
        {
            try
            {
                EventHandlers.Remove(_event);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
            } 
        }
        public void RegisterNUICallback(string _type, Action<dynamic, CallbackDelegate> _callback)
        {
            API.RegisterNuiCallbackType(_type);
            RegisterEventHandler($"__cfx_nui:{_type}", _callback);
        }




        private async Task OnTick()
        {
            if(this.MenuManager != null)
            {
                this.MenuManager.Tick();
                Utils.OnTick();
            }
        }
    }
}
