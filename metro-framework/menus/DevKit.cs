﻿using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace metro_framework.menus
{
    public class DevKit : UIMenu
    {
        public MenuPool MENUPOOL;

        public UIMenu VectorMenu;

        public DevKit() : base("", "")
        {
            this.MENUPOOL = new MenuPool();
            var banner = new Sprite("shopui_title_clubhousemod", "shopui_title_clubhousemod", new Point(0, 0), new Size(0, 0));

            this.VectorMenu = this.MENUPOOL.AddSubMenu(this, "Vector Menu");
            this.SetBannerType(banner);
            this.VectorMenu.SetBannerType(banner);

            this.SetupVectorMenu();

            this.MENUPOOL.Add(this);
            this.MENUPOOL.Add(this.VectorMenu);
        }

        public void SetupVectorMenu()
        {
            UIMenuCheckboxItem DisplayCurrentVector = new UIMenuCheckboxItem("Display Current Vector", false);
            UIMenuItem LogCurrentVector = new UIMenuItem("Log Current Vector");

            this.VectorMenu.AddItem(DisplayCurrentVector);
            this.VectorMenu.AddItem(LogCurrentVector);

            this.VectorMenu.OnCheckboxChange += (sender, item, checked_) =>
            {
                if(item == DisplayCurrentVector)
                {
                    Client.INSTANCE.DevKitManager.ShouldDisplayCurrentVector = checked_;
                }
            };

            this.VectorMenu.OnItemSelect += (sender, item, index) =>
            {
                if(item == LogCurrentVector)
                {
                    Client.INSTANCE.DevKitManager.LogCurrentVector();
                }
            };
        }


        public void Tick()
        {
            this.MENUPOOL.ProcessMenus();
        }
    }
}
