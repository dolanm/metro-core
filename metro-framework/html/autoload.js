class ContextMenu {
  constructor(parent, x, y, items) {
    this.parent = parent;
    this.x = x;
    this.y = y;
    this.items = items;
    this.draw();
  }

  draw() {
    this.element = document.createElement("div");
    this.element.style.padding = "5px";
    this.element.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
    this.element.style.border = "1px solid black";
    this.element.style.position = "absolute";
    this.element.style.color = "white";
    this.element.style.zIndex = 999;
    this.element.style.left = this.x + "px";
    this.element.style.top = this.y + "px";

    var curid = 0;
    for (var itemname in this.items) {
      let func = this.items[itemname];
      if (!func) {
        if (curid === 0 || curid === Object.keys(this.items).length - 1)
          continue;
        var tempdiv = document.createElement("a");
        tempdiv.style.width = "100%";
        tempdiv.style.display = "block";
        tempdiv.style.padding = "3px";
        tempdiv.style.marginTop = "5px";
        tempdiv.style.marginBottom = "5px";
        tempdiv.style.textAlign = "center";
        tempdiv.style.backgroundColor = "rgba(0, 0, 0, 0.8)";

        $(tempdiv).appendTo($(this.element));
      } else {
        var tempdiv = document.createElement("a");
        tempdiv.style.width = "100%";
        tempdiv.style.display = "block";
        tempdiv.style.padding = "10px";
        tempdiv.style.textAlign = "center";
        tempdiv.innerText = itemname;

        $(tempdiv).hover(
          function() {
            $(this).css("background-color", "rgba(0, 0, 0, 0.8)");
          },
          function() {
            $(this).css("background-color", "rgba(0, 0, 0, 0)");
          }
        );
        $(tempdiv).click(() => {
          func();
        });

        $(tempdiv).appendTo($(this.element));
      }
      curid++;
    }

    /*if (this.item.category == 1) {
      var consumediv = document.createElement("a");
      consumediv.style.width = "100%";
      consumediv.style.display = "block";
      consumediv.style.padding = "10px";
      consumediv.style.textAlign = "center";
      consumediv.innerText = "Consume";

      $(consumediv).hover(
        function() {
          $(this).css("background-color", "rgba(0, 0, 0, 0.8)");
        },
        function() {
          $(this).css("background-color", "rgba(0, 0, 0, 0)");
        }
      );
      $(consumediv).click(() => {
        onInventoryInteract(this.item.id);
      });

      $(consumediv).appendTo($(this.element));
    }

    var dropiv = document.createElement("a");
    dropiv.style.width = "100%";
    dropiv.style.display = "block";
    dropiv.style.padding = "10px";
    dropiv.style.textAlign = "center";

    dropiv.innerText = "Drop";

    $(dropiv).hover(
      function() {
        $(this).css("background-color", "rgba(0, 0, 0, 0.8)");
      },
      function() {
        $(this).css("background-color", "rgba(0, 0, 0, 0)");
      }
    );

    $(dropiv).appendTo($(this.element));*/

    $(".mainbody").append($(this.element));
  }

  delete() {
    this.element.parentNode.removeChild(this.element);
  }
}

class Tooltip {
  constructor(parent, item, x, y) {
    this.parent = parent;
    this.item = item;
    this.x = x;
    this.y = y;

    this.draw();
  }

  setItem(item) {
    this.item = item;
    this.delete();
    this.draw();
  }

  draw() {
    this.element = document.createElement("div");
    this.element.style.padding = "5px";
    this.element.style.backgroundColor = "rgba(0, 0, 0, 0.9)";
    this.element.style.border = "1px solid black";
    this.element.style.position = "absolute";
    this.element.style.color = "white";
    this.element.style.zIndex = 999;
    this.element.style.left = this.x + "px";
    this.element.style.top = this.y + "px";

    var titlediv = document.createElement("div");
    titlediv.innerText = this.item.name;

    $(titlediv).appendTo($(this.element));

    var descdiv = document.createElement("div");
    descdiv.innerText = this.item.description;
    $(descdiv).css("color", "grey");

    $(descdiv).appendTo($(this.element));

    var benidiv = document.createElement("div");
    benidiv.style.marginTop = "10px";
    if (this.item.benifits) {
      var hunger = this.item.benifits[0];
      var thirst = this.item.benifits[1];
      var health = this.item.benifits[2];

      if (health && health > 0) {
        var healthdiv = document.createElement("div");

        var textspan = document.createElement("span");
        textspan.innerText = "Health";
        textspan.style.color = "#eb4242";
        $(textspan).appendTo($(healthdiv));

        var valspan = document.createElement("span");
        valspan.innerText = ": +" + health;
        $(valspan).appendTo($(healthdiv));

        $(healthdiv).appendTo($(benidiv));
      }

      if (thirst && thirst > 0) {
        var thirstdiv = document.createElement("div");

        var textspan = document.createElement("span");
        textspan.innerText = "Thirst";
        textspan.style.color = "#4287f5";
        $(textspan).appendTo($(thirstdiv));

        var valspan = document.createElement("span");
        valspan.innerText = ": +" + thirst;
        $(valspan).appendTo($(thirstdiv));

        $(thirstdiv).appendTo($(benidiv));
      }

      if (hunger && hunger > 0) {
        var hungerdiv = document.createElement("div");

        var textspan = document.createElement("span");
        textspan.innerText = "Hunger";
        textspan.style.color = "#f79123";
        $(textspan).appendTo($(hungerdiv));

        var valspan = document.createElement("span");
        valspan.innerText = ": +" + hunger;
        $(valspan).appendTo($(hungerdiv));

        $(hungerdiv).appendTo($(benidiv));
      }
    }

    if (this.item.cooldown !== -1) {
      var cooldowndiv = document.createElement("div");

      var cooldownspan = document.createElement("span");
      cooldownspan.innerText = "Cooldown";
      cooldownspan.style.color = "#EE82EE";
      $(cooldownspan).appendTo($(cooldowndiv));

      var valspan = document.createElement("span");
      valspan.innerText = ": " + this.item.cooldown + " Seconds";
      $(valspan).appendTo($(cooldowndiv));

      $(cooldowndiv).appendTo($(benidiv));
    }

    var weightdiv = document.createElement("div");

    var weightspan = document.createElement("span");
    weightspan.innerText = "Weight";
    weightspan.style.color = "grey";
    $(weightspan).appendTo($(weightdiv));

    var valspan = document.createElement("span");
    valspan.innerText = ": " + this.item.weight * this.item.quantity + " g";
    $(valspan).appendTo($(weightdiv));

    $(weightdiv).appendTo($(benidiv));

    $(benidiv).appendTo($(this.element));

    $(".mainbody").append($(this.element));
  }

  moveTo(x, y) {
    this.x = x;
    this.y = y;
    this.element.style.left = this.x + "px";
    this.element.style.top = this.y + "px";
  }

  delete() {
    this.element.parentNode.removeChild(this.element);
  }
}

class InventSlot {
  constructor(parent, id, x, y) {
    this.parent = parent;
    this.id = id;
    this.x = x;
    this.y = y;
    this.item = null;

    this.element = document.createElement("div");
    this.element.style.width = "75px";
    this.element.style.height = "75px";
    this.element.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
    this.element.style.border = "1px solid #7c6a04";
    this.element.id = "inventslot" + id;
    this.element.style.position = "absolute";
    this.element.style.left = x + "px";
    this.element.style.top = y + "px";

    $(".mainbody").append($(this.element));
  }

  onInserted(item) {
    var preslot = this.parent.inventory.getSlotFromItem(item);

    if (preslot) {
      if (preslot.id !== this.id) {
        onInventorySwap(preslot.id, this.id);
        preslot.onRemoved();
      }
    }

    item.moveTo(this.x + 5, this.y + 5);
    this.item = item;
    if (item.quantity > 1) {
      if (!this.quantityele) {
        this.quantityele = document.createElement("span");
      }
      this.quantityele.innerText = "" + item.quantity;
      this.quantityele.style.right = "2px";
      this.quantityele.style.bottom = "1px";
      this.quantityele.style.position = "absolute";
      this.quantityele.style.color = "white";
      this.quantityele.style.display = "initial";
      $(this.element).append($(this.quantityele));
    }
  }

  onSelected() {
    if (!this.quantityele) return;

    this.quantityele.style.display === "none"
      ? (this.quantityele.style.display = "initial")
      : (this.quantityele.style.display = "none");
  }

  onRemoved() {
    this.item = null;
    this.element.innerHTML = "";
  }

  isOver(x, y) {
    if (
      this.x < x &&
      this.x + this.element.clientWidth > x &&
      this.y < y &&
      this.y + this.element.clientHeight > y
    ) {
      return true;
    }

    return false;
  }
}

class Inventory {
  constructor(parent, sx, sy, cols, rows) {
    this.parent = parent;

    this.x = sx - (80 * cols) / 2;
    this.y = sy - (80 * rows) / 2;
    this.columns = cols;
    this.rows = rows;
    this.drawBG();
    this.slots = [];
    this.items = [];
    var curid = 0;
    for (var y = 0; y < cols; y++) {
      for (var x = 0; x < rows; x++) {
        this.slots.push(
          new InventSlot(this.parent, curid, this.x + 80 * x, this.y + 80 * y)
        );
        curid++;
      }
    }
  }

  drawBG() {
    var endx = this.x + 80 * this.columns;
    var endy = this.y + 80 * this.rows;

    var bg = document.createElement("div");
    bg.style.position = "absolute";
    bg.style.left = this.x - 7 + "px";
    bg.style.top = this.y - 7 + "px";
    bg.style.width = endx - this.x + 8 + "px";
    bg.style.height = endy - this.y + 8 + "px";
    bg.style.backgroundColor = "rgba(75, 75, 75, 1)";
    bg.style.border = "1px solid #f9db2f";

    $(".mainbody").append($(bg));
  }

  getSlot(id) {
    for (var slot of this.slots) {
      if (slot.id === id) {
        return slot;
      }
    }
    return null;
  }

  getSlotFromItem(item) {
    for (var slot of this.slots) {
      if (slot.item === item) {
        return slot;
      }
    }
    return null;
  }

  getSlotAt(x, y) {
    for (var slot of this.slots) {
      if (slot.isOver(x, y)) {
        return slot;
      }
    }
    return null;
  }

  onItemSelected(item) {
    for (var slow of this.slots) {
      if (slow.item === item) {
        slow.onSelected();
        return;
      }
    }
  }

  addItem(item, slot) {
    var slot = this.getSlot(slot);
    if (slot) {
      if (!slot.item) {
        this.items.push(item);
        item.draw(slot.x + 5, slot.y + 5);
        slot.onInserted(item);
      } else {
      }
    }
  }

  getDraggingItem() {
    for (var s of this.items) {
      if (s.beingdragged) {
        return s;
      }
    }
    return null;
  }
}

class Item {
  constructor(
    parent,
    id,
    category,
    name,
    description,
    benifits,
    weight,
    cooldown,
    quantity,
    src
  ) {
    this.id = id;
    this.category = category;
    this.name = name;
    this.description = description;
    this.parent = parent;
    this.benifits = benifits;
    this.quantity = quantity;
    this.weight = weight;
    this.cooldown = cooldown;

    this.mouseover = false;
    this.beingdragged = false;
    this.img = src;
  }

  draw(x, y) {
    this.x = x;
    this.y = y;
    this.element = document.createElement("img");
    this.element.src = this.img;
    this.element.style.width = "65px";
    this.element.style.height = "65px";

    this.element.style.backgroundColor = "rgba(255, 255, 255, 0)";
    this.element.style.position = "absolute";
    this.element.style.left = x + "px";
    this.element.style.top = y + "px";
    $(this.element).mousedown(event => {
      if (
        this.parent.inventory.getDraggingItem() !== null &&
        !this.beingdragged
      ) {
        return;
      }
      if (event.which === 1) {
        if (this.parent.context) {
          return;
        }
        this.beingdragged = !this.beingdragged;
        if (!this.beingdragged) {
          this.onDropped();
        } else {
          this.onPickedUp();
        }
      }
    });
    $(".mainbody").append($(this.element));
  }

  onPickedUp() {
    this.element.style.zIndex = 999;
    this.parent.inventory.onItemSelected(this);
    this.element.style.left = this.parent.x - 65 / 2 + "px";
    this.element.style.top = this.parent.y - 65 / 2 + "px";
    if (this.parent.tooltip) {
      this.parent.tooltip.delete();
      this.parent.tooltip = null;
    }
  }

  onDropped() {
    var over = false;
    var preslot = this.parent.inventory.getSlotFromItem(this);
    for (var slot of this.parent.inventory.slots) {
      if (slot.isOver(this.parent.x, this.parent.y)) {
        over = true;
        if (preslot && preslot.id === slot.id) {
          this.element.style.zIndex = 1;
          slot.onInserted(this);
        } else {
          if (!slot.item) {
            this.element.style.zIndex = 1;
            slot.onInserted(this);
            console.log("Item inserted into slot into: " + slot.id);
          } else {
            this.element.style.zIndex = 1;
            var item = slot.item;
            item.beingdragged = true;
            item.onPickedUp();
            slot.onInserted(this);
            console.log("from: " + preslot.id + " to " + slot.id);
          }
        }

        break;
      }
    }
    if (!over) {
      this.beingdragged = true;
    }
  }

  destroy() {
    this.element.remove();
  }

  moveTo(x, y) {
    this.element.style.left = x + "px";
    this.element.style.top = y + "px";
  }

  isOver(x, y) {
    if (
      this.x < x &&
      this.x + this.element.clientWidth > x &&
      this.y < y &&
      this.y + this.element.clientHeight > y
    ) {
      return true;
    }

    return false;
  }
}

/* Crafting Classes */
class CraftBox {
  constructor(parent) {
    this.parent = parent;

    this.drawBG();
  }

  getWidth() {
    return 400;
  }

  getHeight() {
    return 600;
  }

  getWidthCenter() {
    return window.innerWidth / 2;
  }

  getHeightCenter() {
    return window.innerHeight / 2;
  }

  drawBG() {
    var startx = this.getWidthCenter() - this.getWidth() / 2;
    var starty = this.getHeightCenter() - this.getHeight() / 2;

    this.wrapper = document.createElement("div");
    this.wrapper.style.position = "absolute";
    this.wrapper.style.left = startx + "px";
    this.wrapper.style.top = starty + "px";
    this.wrapper.style.width = this.getWidth() + "px";
    this.wrapper.style.height = this.getHeight() + "px";
    this.wrapper.style.backgroundColor = "rgba(75, 75, 75, 1)";
    this.wrapper.style.border = "1px solid #f9db2f";
    this.wrapper.style.overflowY = "scroll";
    this.wrapper.style.padding = "5px";

    for (var i = 0; i < 50; i++) {
      this.addItem(i);
    }

    $(".mainbody").append($(this.wrapper));
  }

  addItem() {
    var test = document.createElement("div");
    test.style.padding = "10px";
    test.style.border = "1px solid #7c6a04";
    test.style.width = "100%";
    test.style.marginTop = "5px";
    test.style.marginBottom = "5px";

    var imgdiv = document.createElement("div");
    imgdiv.style.display = "inline-block";

    var tempimg = document.createElement("img");
    tempimg.src =
      "https://gamepedia.cursecdn.com/dayz_gamepedia/thumb/d/d4/Firewood.png/100px-Firewood.png?version=b5c60649bc141432c08c7c536a2a520b";
    tempimg.style.width = "65px";
    tempimg.style.height = "65px";
    tempimg.style.margin = "5px";
    $(imgdiv).append($(tempimg));
    $(test).append($(imgdiv));

    var infodiv = document.createElement("div");
    infodiv.style.display = "inline-block";
    infodiv.style.verticalAlign = "middle";

    var title = document.createElement("span");
    title.innerText = "Stick (x5)";
    title.style.display = "block";
    title.style.color = "white";
    $(infodiv).append($(title));

    var other = document.createElement("span");
    other.innerText = "Useful resource for building";
    other.style.display = "block";
    $(infodiv).append($(other));

    var more = document.createElement("span");
    more.innerText = "1x Wood 5x String";
    more.style.display = "block";
    $(infodiv).append($(more));
    $(test).append($(infodiv));

    $(this.wrapper).append($(test));
  }
}
