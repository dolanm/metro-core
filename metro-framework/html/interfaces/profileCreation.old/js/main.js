﻿var modelCounts = [0, 0];

function onCreateClicked() {
  playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
  var firstname = document.getElementById("firstname").value;

  var lastname = document.getElementById("lastname").value;

  var dob = document.getElementById("dob").value;

  var height = document.getElementById("height").value;
  var gender = 0;
  if (document.getElementById("femalecheck").checked) {
    gender = 1;
  }

  var model = document.getElementById("model").value;
  createProfile(firstname, lastname, dob, height, gender, model);
}

function onCancelClicked() {
  backend.closeUI();
}

function onModelChange() {
  var malecheck = document.getElementById("malecheck");

  var newid = document.getElementById("model").value;

  if (malecheck.checked) {
    previewModel(newid - 1, 0);
  } else {
    previewModel(newid - 1, 1);
  }
}

function onGenderChange(gender) {
  var ele = document.getElementById("model");
  ele.value = "1";
  $(ele).attr("max", this.modelCounts[gender]);
  onModelChange(1);
}

function onClientEvent(data) {
  if (data.type === "openui" && data.values.page === "profileCreation") {
    this.modelCounts = data.values.models;
    var val = Math.round(Math.random());
    val === 0
      ? (document.getElementById("malecheck").checked = true)
      : (document.getElementById("femalecheck").checked = true);
    onGenderChange(val);
    onModelChange(1);
    console.log("got models: " + this.modelCounts);
  }
}
