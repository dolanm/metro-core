﻿var selected = 0;

var modelCounts = [0, 0];

var temp = [
  {
    avatar: "https://i.imgur.com/ov54b1X.png",
    first_name: "Spongebob",
    last_name: "Squarepants",
    faction: "Civilian",
    balance_bank: 250,
    balance_wallet: 723
  }
];

var profiles = [];
$(function() {
  $('[data-toggle="tooltip"]').tooltip();
});

function Unselect() {
  for (var i = 0; i <= 2; i++) {
    var ele = document.getElementById("char" + i);
    if (ele) {
      ele.style.border = "1px solid black";
    }
  }
}

function Select(id) {
  playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
  this.selected = id;
  var ele = document.getElementById("char" + id);
  if (ele) {
    ele.style.border = "1px solid #4286f4";
  }

  if (!this.profiles[id]) {
    var actele = document.getElementById("action");
    $(actele).empty();

    var createbut = document.createElement("button");
    createbut.type = "button";
    createbut.id = "createButton";
    createbut.innerText = "Create";
    $(createbut).addClass("btn btn-success btn-block");
    $(createbut).click(() => {
      this.onCreateClicked();
    });
    $(actele).append($(createbut));

    /*<button
        type="button"
        id="testButton"
        class="btn btn-danger"
        onclick="onEnterClicked()"
        style="width: 49%"
        disabled
      >
        Delete
      </button>
      <button
        type="button"
        id="enterButton"
        class="btn btn-primary float-right"
        onclick="onEnterClicked()"
        style="width: 49%"
        disabled
      >
        Enter
      </button>*/
  } else {
    var actele = document.getElementById("action");
    $(actele).empty();

    var deletebut = document.createElement("button");
    deletebut.type = "button";
    deletebut.id = "deleteButton";
    deletebut.innerText = "Delete";
    $(deletebut).addClass("btn btn-danger");
    $(deletebut).css("width", "49%");
    $(deletebut).click(() => {
      this.onDeleteClicked();
    });
    $(actele).append($(deletebut));

    var enterbut = document.createElement("button");
    enterbut.type = "button";
    enterbut.id = "enterButton";
    enterbut.innerText = "Enter";
    $(enterbut).addClass("btn btn-primary float-right");
    $(enterbut).css("width", "49%");
    $(enterbut).click(() => {
      this.onEnterClicked();
    });
    $(actele).append($(enterbut));
  }
}

function onClicked(id) {
  this.Unselect();
  this.Select(id);
}

function onEnterClicked() {
  playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
  selectProfile(selected);
}

function onDeleteClicked() {
  playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
  Swal.fire({
    type: "warning",
    title: "Are you sure?",
    text: "This cannot be undone!",
    showCancelButton: true,
    confirmButtonText: "Delete",
    confirmButtonColor: "red"
  }).then(result => {
    if (result.value) {
      deleteProfile(selected);
    }
  });
}

function onCreateClicked() {
  playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
  backend.closeUI();
  backend.openUI("profileCreation", {
    type: "openui",
    values: { page: "profileCreation", models: modelCounts }
  });
}

function onClientEvent(data) {
  this.modelCounts = [data.values.males, data.values.females];
  this.profiles = data.values.profiles;

  draw();
  console.log("recieved profiles: " + this.profiles.length);
}

function createChar(id, profile) {
  if (!profile) {
    // Empty Slot
    var ccbodydiv = document.createElement("div");
    ccbodydiv.style.border = "1px solid black";
    $(ccbodydiv).attr("id", "char" + id);
    $(ccbodydiv).addClass("card-body ccbody");
    $(ccbodydiv).click(() => {
      this.onClicked(id);
    });
    //TODO: Add onclick!

    var avatarcont = document.createElement("div");
    $(avatarcont).addClass("ccontainer");
    $(avatarcont).css("padding-right", "10px");

    var avatarimg = document.createElement("img");
    $(avatarimg).attr("width", 70);
    $(avatarimg).attr("height", 70);
    avatarimg.src = "http://placehold.jp/42/454545/54a6f7/70x70.png?text=%3F";

    $(avatarcont).append($(avatarimg));
    $(ccbodydiv).append($(avatarcont));

    var txtcont = document.createElement("div");
    $(txtcont).addClass("ccontainer");

    var txtp = document.createElement("p");
    txtp.innerText = "Empty";
    $(txtp).addClass("ccempty");

    $(txtcont).append($(txtp));
    $(ccbodydiv).append($(txtcont));

    var card = document.getElementById("card");
    $(card).append($(ccbodydiv));
    return;
  }

  //Char available

  var ccbodydiv = document.createElement("div");
  ccbodydiv.style.border = "1px solid black";
  $(ccbodydiv).attr("id", "char" + id);
  $(ccbodydiv).addClass("card-body ccbody");
  $(ccbodydiv).click(() => {
    this.onClicked(id);
  });
  //TODO: Add onclick!
  var avatarcont = document.createElement("div");
  $(avatarcont).addClass("ccontainer");
  $(avatarcont).css("padding-right", "10px");

  var avatarimg = document.createElement("img");
  $(avatarimg).attr("width", 70);
  $(avatarimg).attr("height", 70);
  avatarimg.src = "https://i.imgur.com/YYrb84y.png";

  $(avatarcont).append($(avatarimg));
  $(ccbodydiv).append($(avatarcont));

  var titlecont = document.createElement("div");
  $(titlecont).addClass("ccontainer");

  var titlep = document.createElement("p");
  titlep.innerText = profile.FirstName + " " + profile.LastName;
  $(titlep).addClass("cctitle");
  $(titlep).css("color", "white");
  $(titlecont).append($(titlep));

  var factionp = document.createElement("p");
  /*if (profile.Fac_Class === 0) {
    factionp.innerText = "Civilian";
  } else if (profile.Fac_Class === 1) {
    factionp.innerText = "EMS";
  } else if (profile.Fac_Class === 2) {
    factionp.innerText = "Police";
  }*/
    factionp.innerText = profile.CurrentFaction.FactionName;

  $(factionp).addClass("ccfaction");
  $(titlecont).append($(factionp));

  var balancep = document.createElement("p");
  $(balancep).addClass("ccbalance");

  var walletspan = document.createElement("span");
  $(walletspan).css("color", "rgb(131,241,126)");
  walletspan.innerText = "$" + profile.Bal_Wallet + " ";

  $(balancep).append($(walletspan));

  var bankspan = document.createElement("span");
  $(bankspan).css("color", "rgb(194,242,194)");
  bankspan.innerText = "$" + profile.Bal_Bank;

  $(balancep).append($(bankspan));
  $(titlecont).append($(balancep));

  $(ccbodydiv).append($(titlecont));

  var card = document.getElementById("card");
  $(card).append($(ccbodydiv));
}

function draw() {
  for (var i = 0; i <= 2; i++) {
    var profile = this.profiles[i];
    this.createChar(i, profile);
  }
  this.Select(0);
}
