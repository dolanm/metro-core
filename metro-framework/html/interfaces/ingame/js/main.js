﻿function Ingame(parent) {
  this.parent = parent;
  parent.backend.data.currentBase = this;
  document.body.style.backgroundColor = "rgba(0, 0, 0, 0)";

  this.showDetails = function() {
    if (this.details) {
      this.details.main.style.display = "inline-block";
    } else {
      this.details = {};

      this.details.main = document.createElement("div");

      this.details.main.style.width = "200px";
      this.details.main.style.position = "absolute";
      this.details.main.style.border = "1px solid #f9db2f";
      this.details.main.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
      this.details.main.style.left = "10px";
      this.details.main.style.top = window.innerHeight - 100 + "px";

      this.details.healthmain = document.createElement("div");
      $(this.details.healthmain).addClass("progress");
      this.details.healthmain.style.margin = "10px";
      this.details.healthbar = document.createElement("div");

      $(this.details.healthbar)
        .addClass("progress-bar bg-danger")
        .attr("role", "progressbar")
        .attr("aria-valuenow", "100")
        .attr("aria-valuemin", "0")
        .attr("aria-valuemax", "100");
      this.details.healthbar.innerText = "100%";
      this.details.healthbar.style.width = "100%";

      $(this.details.healthbar).appendTo($(this.details.healthmain));

      $(this.details.healthmain).appendTo($(this.details.main));

      this.details.armourmain = document.createElement("div");
      $(this.details.armourmain).addClass("progress");
      this.details.armourmain.style.margin = "10px";

      this.details.armourbar = document.createElement("div");

      $(this.details.armourbar)
        .addClass("progress-bar")
        .attr("role", "progressbar")
        .attr("aria-valuenow", "75")
        .attr("aria-valuemin", "0")
        .attr("aria-valuemax", "100");
      this.details.armourbar.innerText = "75%";
      this.details.armourbar.style.width = "75%";

      $(this.details.armourbar).appendTo($(this.details.armourmain));

      $(this.details.armourmain).appendTo($(this.details.main));

      this.details.weightmain = document.createElement("div");
      $(this.details.weightmain).addClass("progress");
      this.details.weightmain.style.margin = "10px";

      this.details.weightbar = document.createElement("div");

      $(this.details.weightbar)
        .addClass("progress-bar bg-warning")
        .attr("role", "progressbar")
        .attr("aria-valuenow", "25")
        .attr("aria-valuemin", "0")
        .attr("aria-valuemax", "100");
      this.details.weightbar.innerText = "25%";
      this.details.weightbar.style.width = "25%";

      $(this.details.weightbar).appendTo($(this.details.weightmain));

      $(this.details.weightmain).appendTo($(this.details.main));

      $(".mainbody").append($(this.details.main));
    }
  };

  this.onProfilesUpdated = function(data) {};

  //this.showWatermark(); 
  this.showDetails();
}

var base = new Ingame(this);
console.log("OPENED!!!!!!!!");
