﻿function FrontEnd() {
  this.tooltip = null;
  this.context = null;
  this.x = 0;
  this.y = 0;
  $('[data-toggle="tooltip"]').tooltip();
  this.inventory = new Inventory(
    this,
    window.innerWidth / 2,
    window.innerHeight / 2,
    5,
    5
  );

  var curid = 0;
  for (var item of backend.getCurrentInventory()) {
    if (item) {
      this.inventory.addItem(
        new Item(
          this,
          item.id,
          item.category,
          item.name,
          item.description,
          item.benifits,
          item.weight,
          item.cooldown,
          item.quantity,
          item.sprite
        ),
        curid
      );
    }

    curid++;
  }
  this.onMouseMoved = event => {
    this.x = event.pageX;
    this.y = event.pageY;
    var dragging = this.inventory.getDraggingItem();
    if (dragging) {
      dragging.moveTo(this.x - 65 / 2, this.y - 65 / 2);
      if (this.tooltip) {
        this.tooltip.delete();
        this.tooltip = null;
      }
    } else {
      var hs = this.inventory.getSlotAt(this.x, this.y);
      if (hs) {
        if (hs.item) {
          if (this.context) {
            return;
          }
          if (!this.tooltip) {
            this.tooltip = new Tooltip(this, hs.item, this.x + 15, this.y);
          } else {
            this.tooltip.setItem(hs.item);
            this.tooltip.moveTo(this.x + 15, this.y);
          }
        }
      } else {
        if (this.tooltip) {
          this.tooltip.delete();
          this.tooltip = null;
        }
      }
    }
  };

  this.onMouseClicked = event => {
    if (this.context) {
      this.context.delete();
      this.context = null;
    }
  };

  this.onContextMenu = event => {
    event.preventDefault();
    if (this.context) {
      this.context.delete();
      this.context = null;
    }
    this.x = event.pageX;
    this.y = event.pageY;
    var hs = this.inventory.getSlotAt(event.pageX, event.pageY);
    if (hs) {
      if (hs.item) {
        if (this.tooltip) {
          this.tooltip.delete();
          this.tooltip = null;
        }

        var items = {
          sep1: null,
          Drop: () => {},
          Split: () => {}
        };

        if (hs.item.category === 0) {
          this.context = new ContextMenu(this, event.pageX, event.pageY, items);
        } else if (hs.item.category === 1) {
          items = Object.assign(
            {
              Consume: () => {
                onInventoryInteract(hs.item.id);
              }
            },
            items
          );
          this.context = new ContextMenu(this, event.pageX, event.pageY, items);
        } else if (hs.item.category === 2) {
          items = Object.assign(
            {
              Equip: () => {
                onInventoryInteract(hs.item.id);
              }
            },
            items
          );
          this.context = new ContextMenu(this, event.pageX, event.pageY, items);
        }
      }
    }
  };

  this.onClosed = function() {
    console.log("invent closed!!!!");
    $(document).off("click");
    $(document).off("contextmenu");
    $(document).off("mousemove");
  };
}

var frontend = new FrontEnd();
backend.setCurrentBase(frontend);


/*function onClientEvent(data) {
  if (data.type === "openui" && data.values.page === "inventory") {
   
  }
}*/

$(document).contextmenu(frontend.onContextMenu);

$(document).click(frontend.onMouseClicked);

$(document).mousemove(frontend.onMouseMoved);
