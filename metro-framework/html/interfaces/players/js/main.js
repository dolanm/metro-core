﻿function PlayerWindow() {
  this.players = backend.data.playerList;
  this.getWidth = function() {
    return 400;
  };

  this.getHeight = function() {
    return window.innerHeight / 2;
  };

  this.getWidthCenter = function() {
    return window.innerWidth / 2;
  };

  this.getHeightCenter = function() {
    return window.innerHeight / 2;
  };

  this.redraw = function() {
    this.remove();
    this.redraw();
  };

  this.remove = function() {
    $(".mainbody").removeChild($(this.wrapper));
  };

  this.draw = function() {
    var startx = this.getWidthCenter() - this.getWidthCenter() / 2;
    var starty = this.getHeightCenter() - this.getHeightCenter() / 2;

    this.wrapper = document.createElement("div");
    this.wrapper.style.position = "absolute";
    this.wrapper.style.left = startx + "px";
    this.wrapper.style.top = starty + "px";
    this.wrapper.style.width = this.getWidthCenter() + "px";
    this.wrapper.style.height = this.getHeightCenter() + "px";
    this.wrapper.style.backgroundColor = "rgba(75, 75, 75, 1)";
    this.wrapper.style.border = "1px solid #f9db2f";
    this.wrapper.style.borderRadius = "5px";
    this.wrapper.style.overflowY = "auto";

    var table = document.createElement("table");
    $(table).addClass("table");

    var thead = document.createElement("thead");

    var headtr = document.createElement("tr");
    headtr.style.color = "white";
    headtr.style.textAlign = "center";
    headtr.style.fontWeight = "400";

    var idth = document.createElement("th");
    idth.scope = "col";
    idth.innerText = "#";
    $(idth).appendTo($(headtr));

    var firstth = document.createElement("th");
    firstth.innerText = "First";
    $(firstth).appendTo($(headtr));

    var lastth = document.createElement("th");
    lastth.innerText = "Last";
    $(lastth).appendTo($(headtr));

    var factionth = document.createElement("th");
    factionth.innerText = "Faction";
    $(factionth).appendTo($(headtr));

    $(headtr).appendTo($(thead));

    $(thead).appendTo($(table));

    var tbody = document.createElement("tbody");
    tbody.style.color = "grey";
    tbody.style.textAlign = "center";

    if (this.players) {
      for (var player of this.players) {
        if (player) {
          var playertr = document.createElement("tr");

          var pidth = document.createElement("th");
          pidth.scope = "row";
          pidth.innerText = "" + player.id;
          $(pidth).appendTo($(playertr));

          var pfirstth = document.createElement("th");
          pfirstth.innerText = player.FirstName;
          $(pfirstth).appendTo($(playertr));

          var plastth = document.createElement("th");
          plastth.innerText = player.LastName;
          $(plastth).appendTo($(playertr));
          const factCapted =
            player.Faction.charAt(0).toUpperCase() + player.Faction.slice(1);
          var pfactionth = document.createElement("th");
          pfactionth.innerHTML =
            player.Faction.toLowerCase() === "police"
              ? "<span style='color: red'>Police</span>"
              : factCapted;
          $(pfactionth).appendTo($(playertr));

          $(playertr).appendTo($(tbody));
        }
      }
    }

    $(tbody).appendTo($(table));

    $(table).appendTo($(this.wrapper));

    $(".mainbody").append($(this.wrapper));
  };
  this.draw();
}

function FrontEnd() {
  this.playerWindow = new PlayerWindow();

  this.onPlayerListUpdate = function() {
    this.playerWindow.redraw();
  };
}

var frontend = new FrontEnd();
backend.setCurrentBase(frontend);
$('[data-toggle="tooltip"]').tooltip();
/*



inventory.addItem(
  new Item(
    this,
    1,
    "Sardines",
    "Better than nothing",
    { 0: 10, 1: 5 },
    100,
    5,
    2,
    "https://gamepedia.cursecdn.com/dayz_gamepedia/thumb/e/ea/Item_sardines.png/120px-Item_sardines.png"
  ),
  1
);

inventory.addItem(
  new Item(
    this,
    1,
    "Bandage",
    "Got a booboo? :(",
    { 2: 25 },
    109,
    10,
    5,
    "https://gamepedia.cursecdn.com/dayz_gamepedia/5/52/Item_Bandage.png?version=83f590d8c2f511272fd715e2834336a3"
  ),
  2
);

inventory.addItem(
  new Item(
    this,
    0,
    "Stone",
    "Useful material for building",
    null,
    1500,
    -1,
    3,
    "https://gamepedia.cursecdn.com/dayz_gamepedia/thumb/d/d4/Stone.png/80px-Stone.png?version=4fee68b069d4f5238356e08ae4bad8c5"
  ),
  3
);


$(function() {
  $('[data-toggle="tooltip"]').tooltip();
});

$(document).contextmenu(function(event) {
  event.preventDefault();
  if (context) {
    context.delete();
    context = null;
  }
  x = event.pageX;
  y = event.pageY;
  var hs = inventory.getSlotAt(event.pageX, event.pageY);
  if (hs) {
    if (hs.item) {
      console.log("here");
      if (tooltip) {
        tooltip.delete();
        tooltip = null;
      }

      context = new ContextMenu(this, hs.item, event.pageX, event.pageY);
    }
  }
});

$(document).click(function() {
  if (context) {
    context.delete();
    context = null;
  }
});

$(document).mousemove(function(event) {
  x = event.pageX;
  y = event.pageY;
  var dragging = inventory.getDraggingItem();
  if (dragging) {
    dragging.moveTo(x, y);
    if (tooltip) {
      tooltip.delete();
      tooltip = null;
    }
  } else {
    var hs = inventory.getSlotAt(x, y);
    if (hs) {
      if (hs.item) {
        if (context) {
          return;
        }
        if (!tooltip) {
          tooltip = new Tooltip(this, hs.item, x + 15, y);
        } else {
          tooltip.setItem(hs.item);
          tooltip.moveTo(x + 15, y);
        }

        /*if (!tooltip) {
          tooltip = document.createElement("div");
          tooltip.style.padding = "5px";
          tooltip.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
          tooltip.style.border = "1px solid black";
          tooltip.style.position = "absolute";
          tooltip.style.color = "white";
          tooltip.style.zIndex = 999;
          $(".mainbody").append($(tooltip));
        }else{
          $(tooltip).html("");
        }
        tooltip.style.display = "initial";
        tooltip.style.left = x + 15 + "px";
        tooltip.style.top = y + "px";

        var namediv = document.createElement("div");


        namediv.innerText = hs.item.name;
      }
    } else {
      if (tooltip) {
        tooltip.delete();
        tooltip = null;
      }
    }
  }
});*/
