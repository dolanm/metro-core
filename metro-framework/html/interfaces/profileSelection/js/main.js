﻿var x, y;

function profileBox(id, profile) {
  this.selected = false;

  this.getElement = function() {
    return this.maindiv;
  };

  this.getUsedSlots = function() {
    var used = 0;
    if (profile) {
      for (var item of profile.inventory) {
        if (item) {
          used++;
        }
      }
    }
    return used;
  };

  this.createSub = function() {
    this.subdiv = document.createElement("div");
    this.subdiv.style.backgroundColor = "rgba(52,58,64, 1)";
    this.subdiv.style.border = "1px solid #7c6a04";
    this.subdiv.style.borderRadius = "5px";
    var width = window.innerWidth / 4;
    this.subdiv.style.width = width - 5 + "px";
    this.subdiv.style.height = "0px";
    this.subdiv.style.position = "absolute";
    this.subdiv.style.top = 90 + 80 + "px";
    this.subdiv.style.padding = "10px";
    var rawleft = (window.innerWidth / 4) * id;
    this.subdiv.style.left = rawleft + width / 2 + "px";
    this.subdiv.style.visibility = "hidden";
    this.subdiv.style.overflow = "hidden";
    if (!profile) {
      var firstname = document.createElement("input");
      firstname.type = "text";
      firstname.placeholder = "First Name";
      firstname.style.marginBottom = "5px";
      $(firstname)
        .addClass("form-control")
        .focus(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
        });

      $(this.subdiv).append($(firstname));

      var lastname = document.createElement("input");
      lastname.type = "text";
      lastname.placeholder = "Last Name";
      lastname.style.marginBottom = "5px";
      $(lastname)
        .addClass("form-control")
        .focus(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
        });

      $(this.subdiv).append($(lastname));

      var height = document.createElement("input");
      height.type = "number";
      height.step = "0.1";
      height.min = "4.0";
      height.max = "7.0";
      height.placeholder = "Height";
      height.style.marginBottom = "5px";
      $(height)
        .addClass("form-control")
        .focus(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
        });

      $(this.subdiv).append($(height));

      var dob = document.createElement("input");
      dob.type = "date";
      dob.placeholder = "Date of birth";
      dob.value = "Date Of Birth";
      dob.style.marginBottom = "5px";
      $(dob)
        .addClass("form-control")
        .focus(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
        });

      $(this.subdiv).append($(dob));

      var gendermalediv = document.createElement("div");
      $(gendermalediv).addClass("form-check form-check-inline");
      gendermalediv.style.marginBottom = "5px";

      var maleinput = document.createElement("input");
      maleinput.type = "radio";
      maleinput.name = "gender" + id;
      maleinput.id = "maleradio" + id;
      maleinput.value = "male";
      maleinput.checked = true;
      $(maleinput)
        .addClass("form-check-input")
        .change(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
        });

      $(gendermalediv).append($(maleinput));

      var malelabel = document.createElement("label");
      $(malelabel).prop("for", "maleradio" + id);
      $(malelabel).addClass("form-check-label");
      malelabel.style.color = "white";

      malelabel.innerText = "Male";
      $(gendermalediv).append($(malelabel));

      $(this.subdiv).append($(gendermalediv));

      var genderfemalediv = document.createElement("div");
      $(genderfemalediv).addClass("form-check form-check-inline");

      var femaleinput = document.createElement("input");
      femaleinput.type = "radio";
      femaleinput.name = "gender" + id;
      femaleinput.id = "femaleradio" + id;
      femaleinput.value = "female";
      $(femaleinput)
        .addClass("form-check-input")
        .change(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
        });

      $(genderfemalediv).append($(femaleinput));

      var femalelabel = document.createElement("label");
      $(femalelabel).prop("for", "femaleradio" + id);
      $(femalelabel).addClass("form-check-label");
      femalelabel.style.color = "white";

      femalelabel.innerText = "Female";
      $(genderfemalediv).append($(femalelabel));

      $(this.subdiv).append($(genderfemalediv));

      var createButton = document.createElement("button");
      createButton.type = "button";
      $(createButton).addClass("btn btn-primary btn-block");
      createButton.innerText = "Create";
      $(createButton).click(() => {
        playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
        var newgen = 0;
        if (document.getElementById("femaleradio" + id).checked) {
          newgen = 1;
        }

        createProfile(
          firstname.value,
          lastname.value,
          dob.value,
          height.value,
          newgen
        );
      });

      $(this.subdiv).append($(createButton));
    } else {
      var statstable = document.createElement("table");
      $(statstable).addClass("table");

      var tbody = document.createElement("tbody");

      var facttr = document.createElement("tr");

      var factionlabel = document.createElement("td");
      factionlabel.style.color = "white";
      factionlabel.innerText = "Faction";
      $(facttr).append($(factionlabel));

      var factionvalue = document.createElement("td");
      factionvalue.style.color = "white";
      factionvalue.style.textAlign = "right";
      factionvalue.innerText =
        profile.CurrentFaction.FactionName.charAt(0).toUpperCase() +
        profile.CurrentFaction.FactionName.slice(1);
      $(facttr).append($(factionvalue));
      $(tbody).append($(facttr));

      var wallettr = document.createElement("tr");

      var walletlabel = document.createElement("td");
      walletlabel.style.color = "white";
      walletlabel.innerText = "Wallet";
      $(wallettr).append($(walletlabel));

      var walletvalue = document.createElement("td");
      walletvalue.style.color = "white";
      walletvalue.style.textAlign = "right";
      walletvalue.innerText = "$" + profile.Bal_Wallet;
      $(wallettr).append($(walletvalue));
      $(tbody).append($(wallettr));

      var banktr = document.createElement("tr");
      banktr.style.borderBottom = "1px solid white";

      var banklabel = document.createElement("td");
      banklabel.style.color = "white";
      banklabel.innerText = "Bank";
      $(banktr).append($(banklabel));

      var bankvalue = document.createElement("td");
      bankvalue.style.color = "white";
      bankvalue.style.textAlign = "right";
      bankvalue.innerText = "$" + profile.Bal_Bank;
      $(banktr).append($(bankvalue));
      $(tbody).append($(banktr));

      var inventtr = document.createElement("tr");
      inventtr.style.borderBottom = "1px solid white";

      var inventlabel = document.createElement("td");
      inventlabel.style.color = "white";
      inventlabel.innerText = "Inventory";
      $(inventtr).append($(inventlabel));

      var inventvalue = document.createElement("td");
      inventvalue.style.color = "white";
      inventvalue.style.textAlign = "right";
      inventvalue.innerText = this.getUsedSlots() + " / 25";
      $(inventtr).append($(inventvalue));
      $(tbody).append($(inventtr));

      $(statstable).append($(tbody));

      $(this.subdiv).append($(statstable));

      var healthlabel = document.createElement("div");
      healthlabel.style.width = "100%";
      healthlabel.style.textAlign = "center";
      healthlabel.style.color = "white";
      healthlabel.innerText = "Health: 25%";
      $(this.subdiv).append($(healthlabel));

      var healthdiv = document.createElement("div");
      $(healthdiv).addClass("progress");
      healthdiv.style.height = "2px";
      healthdiv.style.marginBottom = "3px";

      var healthprogress = document.createElement("div");
      $(healthprogress)
        .prop("role", "progressbar")
        .prop("aria-valuenow", "25")
        .prop("aria-valuemin", "0")
        .prop("aria-valuemax", "100")
        .addClass("progress-bar bg-danger");
      healthprogress.style.width = "25%";

      $(healthdiv).append($(healthprogress));

      $(this.subdiv).append($(healthdiv));

      var armourlabel = document.createElement("div");
      armourlabel.style.width = "100%";
      armourlabel.style.textAlign = "center";
      armourlabel.style.color = "white";
      armourlabel.innerText = "Armour: 25%";
      $(this.subdiv).append($(armourlabel));

      var armourdiv = document.createElement("div");
      $(armourdiv).addClass("progress");
      armourdiv.style.height = "2px";
      armourdiv.style.marginBottom = "3px";

      var armourprogress = document.createElement("div");
      $(armourprogress)
        .prop("role", "progressbar")
        .prop("aria-valuenow", "25")
        .prop("aria-valuemin", "0")
        .prop("aria-valuemax", "100")
        .addClass("progress-bar bg-primary");
      armourprogress.style.width = "25%";

      $(armourdiv).append($(armourprogress));

      $(this.subdiv).append($(armourdiv));

      var weightlabel = document.createElement("div");
      weightlabel.style.width = "100%";
      weightlabel.style.textAlign = "center";
      weightlabel.style.color = "white";
      weightlabel.innerText = "Weight: 25%";
      $(this.subdiv).append($(weightlabel));

      var weightdiv = document.createElement("div");
      $(weightdiv).addClass("progress");
      weightdiv.style.height = "2px";
      weightdiv.style.marginBottom = "15px";

      var weightprogress = document.createElement("div");
      $(weightprogress)
        .prop("role", "progressbar")
        .prop("aria-valuenow", "25")
        .prop("aria-valuemin", "0")
        .prop("aria-valuemax", "100")
        .addClass("progress-bar bg-warning");
      weightprogress.style.width = "25%";

      $(weightdiv).append($(weightprogress));

      $(this.subdiv).append($(weightdiv));

      var thirstlabel = document.createElement("div");
      thirstlabel.style.width = "100%";
      thirstlabel.style.textAlign = "center";
      thirstlabel.style.color = "white";
      thirstlabel.innerText = "Thirst: 25%";
      $(this.subdiv).append($(thirstlabel));

      var thirstdiv = document.createElement("div");
      $(thirstdiv).addClass("progress");
      thirstdiv.style.height = "2px";
      thirstdiv.style.marginBottom = "3px";

      var thirstprogress = document.createElement("div");
      $(thirstprogress)
        .prop("role", "progressbar")
        .prop("aria-valuenow", "25")
        .prop("aria-valuemin", "0")
        .prop("aria-valuemax", "100")
        .addClass("progress-bar bg-info");
      thirstprogress.style.width = "25%";

      $(thirstdiv).append($(thirstprogress));

      $(this.subdiv).append($(thirstdiv));

      var hungerlabel = document.createElement("div");
      hungerlabel.style.width = "100%";
      hungerlabel.style.textAlign = "center";
      hungerlabel.style.color = "white";
      hungerlabel.innerText = "Hunger: 25%";
      $(this.subdiv).append($(hungerlabel));

      var hungerdiv = document.createElement("div");
      $(hungerdiv).addClass("progress");
      hungerdiv.style.height = "2px";
      hungerdiv.style.marginBottom = "15px";

      var hungerprogress = document.createElement("div");
      $(hungerprogress)
        .prop("role", "progressbar")
        .prop("aria-valuenow", "25")
        .prop("aria-valuemin", "0")
        .prop("aria-valuemax", "100")
        .addClass("progress-bar")
        .css("background-color", "#e59400");
      hungerprogress.style.width = "25%";

      $(hungerdiv).append($(hungerprogress));

      $(this.subdiv).append($(hungerdiv));

      var selectButton = document.createElement("button");
      selectButton.type = "button";
      selectButton.style.marginBottom = "10px";
      $(selectButton).addClass("btn btn-success btn-block");
      selectButton.innerText = "Select";
      $(selectButton).click(() => {
        playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
        selectProfile(id);
      });
      $(this.subdiv).append($(selectButton));

      var deleteButton = document.createElement("button");
      deleteButton.type = "button";
      $(deleteButton)
        .addClass("btn btn-danger btn-block")
        .click(() => {
          playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
          Swal.fire({
            type: "warning",
            title: "Are you sure?",
            text: "This cannot be undone!",
            showCancelButton: true,
            confirmButtonText: "Delete",
            confirmButtonColor: "red"
          }).then(result => {
            if (result.value) {
              deleteProfile(id);
            }
          });
        });
      deleteButton.innerText = "Delete";

      $(this.subdiv).append($(deleteButton));
    }

    $(".mainbody").append($(this.subdiv));
  };

  this.toggleSub = function() {
    playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
    this.busy = true;
    if (!this.subdiv) {
      this.createSub();
    }
    if (this.selected) {
      this.subdiv.style.visibility = "visible";
      /*while (this.subdiv.clientHeight < 100) {
        this.subdiv.style.height = this.subdiv.clientHeight + 1;
      }*/

      var curi = 0;
      var loopfunc = () => {
        profile ? (curi += 2.5) : (curi += 1.5);
        this.subdiv.style.height = curi + "px";
        if (curi < (profile ? 485 : 258)) {
          setTimeout(loopfunc, 1);
        } else {
          this.busy = false;
        }
      };
      loopfunc();
    } else {
      /*while (this.subdiv.clientHeight > 0) {
        this.subdiv.style.height = this.subdiv.clientHeight - 1;
      }*/
      var curi = profile ? 485 : 258;
      var loopfunc = () => {
        profile ? (curi -= 2.5) : (curi -= 1.5);
        this.subdiv.style.height = curi + "px";
        if (curi > 0) {
          setTimeout(loopfunc, 1);
        } else {
          this.subdiv.style.visibility = "hidden";
          this.busy = false;
        }
      };
      loopfunc();
    }
  };

  this.maindiv = document.createElement("div");
  this.maindiv.style.backgroundColor = "rgba(52,58,64, 1)";
  this.maindiv.style.border = "1px solid #7c6a04";
  this.maindiv.style.borderRadius = "5px";
  var width = window.innerWidth / 4;
  this.maindiv.style.width = width - 5 + "px";
  this.maindiv.style.height = "75px";
  this.maindiv.style.position = "absolute";
  this.maindiv.style.top = 90 + "px";
  var rawleft = (window.innerWidth / 4) * id;
  this.maindiv.style.left = rawleft + width / 2 + "px";

  this.namespan = document.createElement("div");
  this.namespan.style.margin = "0px";
  this.namespan.style.position = "absolute";
  this.namespan.style.top = "50%";
  this.namespan.style.left = "50%";
  this.namespan.style.transform = "translateX(-50%) translateY(-50%)";
  if (!profile) {
    this.namespan.style.color = "grey";
    this.namespan.innerText = "Empty";
  } else {
    this.namespan.style.color = "grey";
    console.log(JSON.stringify(profile));
    this.namespan.innerText = profile.FirstName + " " + profile.LastName;
  }

  $(this.maindiv).append($(this.namespan));

  $(this.maindiv).hover(
    () => {
      if (this.selected) {
        return;
      }
      $(this.maindiv).css("border", "1px solid #f9db2f");
      $(this.namespan).css("color", "white");
    },
    () => {
      if (this.selected) {
        return;
      }
      $(this.maindiv).css("border", "1px solid #7c6a04");
      $(this.namespan).css("color", "grey");
    }
  );

  $(this.maindiv).click(() => {
    if (this.onSelected) {
      this.onSelected();
    }
  });

  $(".mainbody").append($(this.maindiv));
}


function FrontEnd()
{

  this.prof1 = new profileBox(0, backend.getProfiles()[0]);
  this.prof2 = new profileBox(1, backend.getProfiles()[1]);
  this.prof3 = new profileBox(2, backend.getProfiles()[2]);

  this.prof1.onSelected = () => {
    if (this.prof1.busy || this.prof2.busy || this.prof3.busy) return;

    this.prof1.selected = !this.prof1.selected;

    if (this.prof1.selected) {
      if (this.prof2.selected) {
        this.prof2.onSelected();
      }

      if (this.prof3.selected) {
        this.prof3.onSelected();
      }
      $(this.prof1.maindiv).css("border", "1px solid #f9db2f");
      $(this.prof1.namespan).css("color", "white");
    } else {
      $(this.prof1.maindiv).css("border", "1px solid #7c6a04");
      $(this.prof1.namespan).css("color", "grey");
    }

    this.prof1.toggleSub();
  };

  this.prof2.onSelected = () => {
    if (this.prof1.busy || this.prof2.busy || this.prof3.busy) return;

    this.prof2.selected = !this.prof2.selected;

    if (this.prof2.selected) {
      if (this.prof1.selected) {
        this.prof1.onSelected();
      }

      if (this.prof3.selected) {
        this.prof3.onSelected();
      }
      $(this.prof2.maindiv).css("border", "1px solid #f9db2f");
      $(this.prof2.namespan).css("color", "white");
    } else {
      $(this.prof2.maindiv).css("border", "1px solid #7c6a04");
      $(this.prof2.namespan).css("color", "grey");
    }

    this.prof2.toggleSub();
  };

  this.prof3.onSelected = () => {
    if (this.prof1.busy || this.prof2.busy || this.prof3.busy) return;
    this.prof3.selected = !this.prof3.selected;

    if (this.prof3.selected) {
      if (this.prof1.selected) {
        this.prof1.onSelected();
      }

      if (this.prof2.selected) {
        this.prof2.onSelected();
      }
      $(this.prof3.maindiv).css("border", "1px solid #f9db2f");
      $(this.prof3.namespan).css("color", "white");
    } else {
      $(this.prof3.maindiv).css("border", "1px solid #7c6a04");
      $(this.prof3.namespan).css("color", "grey");
    }

    this.prof3.toggleSub();
  };

  this.onClosed = function() {
    
  }
}


var frontend = new FrontEnd();
backend.setCurrentBase(frontend);
/*



inventory.addItem(
  new Item(
    this,
    1,
    "Sardines",
    "Better than nothing",
    { 0: 10, 1: 5 },
    100,
    5,
    2,
    "https://gamepedia.cursecdn.com/dayz_gamepedia/thumb/e/ea/Item_sardines.png/120px-Item_sardines.png"
  ),
  1
);

inventory.addItem(
  new Item(
    this,
    1,
    "Bandage",
    "Got a booboo? :(",
    { 2: 25 },
    109,
    10,
    5,
    "https://gamepedia.cursecdn.com/dayz_gamepedia/5/52/Item_Bandage.png?version=83f590d8c2f511272fd715e2834336a3"
  ),
  2
);

inventory.addItem(
  new Item(
    this,
    0,
    "Stone",
    "Useful material for building",
    null,
    1500,
    -1,
    3,
    "https://gamepedia.cursecdn.com/dayz_gamepedia/thumb/d/d4/Stone.png/80px-Stone.png?version=4fee68b069d4f5238356e08ae4bad8c5"
  ),
  3
);


$(function() {
  $('[data-toggle="tooltip"]').tooltip();
});

$(document).contextmenu(function(event) {
  event.preventDefault();
  if (context) {
    context.delete();
    context = null;
  }
  x = event.pageX;
  y = event.pageY;
  var hs = inventory.getSlotAt(event.pageX, event.pageY);
  if (hs) {
    if (hs.item) {
      console.log("here");
      if (tooltip) {
        tooltip.delete();
        tooltip = null;
      }

      context = new ContextMenu(this, hs.item, event.pageX, event.pageY);
    }
  }
});

$(document).click(function() {
  if (context) {
    context.delete();
    context = null;
  }
});

$(document).mousemove(function(event) {
  x = event.pageX;
  y = event.pageY;
  var dragging = inventory.getDraggingItem();
  if (dragging) {
    dragging.moveTo(x, y);
    if (tooltip) {
      tooltip.delete();
      tooltip = null;
    }
  } else {
    var hs = inventory.getSlotAt(x, y);
    if (hs) {
      if (hs.item) {
        if (context) {
          return;
        }
        if (!tooltip) {
          tooltip = new Tooltip(this, hs.item, x + 15, y);
        } else {
          tooltip.setItem(hs.item);
          tooltip.moveTo(x + 15, y);
        }

        /*if (!tooltip) {
          tooltip = document.createElement("div");
          tooltip.style.padding = "5px";
          tooltip.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
          tooltip.style.border = "1px solid black";
          tooltip.style.position = "absolute";
          tooltip.style.color = "white";
          tooltip.style.zIndex = 999;
          $(".mainbody").append($(tooltip));
        }else{
          $(tooltip).html("");
        }
        tooltip.style.display = "initial";
        tooltip.style.left = x + 15 + "px";
        tooltip.style.top = y + "px";

        var namediv = document.createElement("div");


        namediv.innerText = hs.item.name;
      }
    } else {
      if (tooltip) {
        tooltip.delete();
        tooltip = null;
      }
    }
  }
});*/
