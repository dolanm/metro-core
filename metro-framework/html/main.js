﻿class Backend {
  constructor() {
    this.data = {
      listeners: {},
      currentPage: "",
      currentBase: null,
      currentInventory: [],
      profiles: [],
      selectedProfile: -1,
      playerList: []
    };
  }

  getCurrentInventory() {
    return this.data.currentInventory;
  }

  setCurrentInventory(invent) {
    this.data.currentInventory = invent;
  }

  getCurrentBase() {
    return this.data.currentBase;
  }

  setCurrentBase(base) {
    this.data.currentBase = base;
  }

  getCurrentPage() {
    return this.data.currentPage;
  }

  addEventListener(name, func) {
    this.data.listeners[name] = func;
  }

  removeEventListener(name) {
    this.data.listeners[name] = null;
  }

  handleEvent(name, data) {
    var func = this.data.listeners[name];
    if (func) {
      func(data);
    }
  }

  setProfiles(profiles) {
    this.data.profiles = profiles;
  }

  getProfiles() {
    return this.data.profiles;
  }

  getSelectedProfile() {
    return this.data.profiles[this.data.selectedProfile];
  }

  openUI(name, ed) {
    return new Promise(resolve => {
      if (this.data.currentBase && this.data.currentBase.onClosed) {
        this.data.currentBase.onClosed();
      }
      this.data.currentPage = name;
      $("body").load("interfaces/" + name + "/index.html", () => {
        if (ed) {
          //test
        }
        return resolve();
      });
      document.body.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
    });
  }

  closeUI(force = false) {
    return new Promise(resolve => {
      if (this.data.currentPage === "profileSelection" && !force) {
        return;
      } else if (this.data.currentPage === "profileCreation") {
        $.post("http://metro-core/metro:nui:openprofiles", JSON.stringify({}));
        return;
      }

      this.openUI("ingame");
      $.post("http://metro-core/metro:nui:unfocus", JSON.stringify({}));
      return resolve();
    });
  }
}

var backend = new Backend();

backend.addEventListener("playerListUpdate", data => {
  backend.data.playerList = data.values.playerList;
  if (backend.getCurrentBase() && backend.getCurrentBase().onPlayerlistUpdate) {
    backend.getCurrentBase().onPlayerlistUpdate();
  }
});

backend.addEventListener("profilecreated", data => {
  backend.openUI("profileSelection", data).then(() => {
    Swal.fire({
      type: "success",
      title: "Profile Created"
    });
  });
});

backend.addEventListener("profiledeleted", data => {
  backend.openUI("profileSelection", data).then(() => {
    Swal.fire({
      type: "success",
      title: "Profile Deleted"
    });
  });
});

backend.addEventListener("inventoryupdate", data => {
  backend.setCurrentInventory(data.values.inventory);
  if (backend.getCurrentPage() === "inventory") {
    backend.openUI("inventory");
  }
});

backend.addEventListener("profiles", data => {
  backend.setProfiles(data.values.profiles);
});

backend.addEventListener("closeui", data => {
  backend.closeUI(true);
});

backend.addEventListener("openui", data => {
  backend.openUI(data.values.page, data);
});

backend.addEventListener("alert", data => {
  showAlert(data.values.type, data.values.title, data.values.text);
});

window.addEventListener("message", function(event) {
  var data = event.data;

  backend.handleEvent(data.type, data);
});

$(document).keyup(function(e) {
  if (e.key === "Escape") {
    backend.closeUI();
  }
});

function showAlert(type, title, text) {
  if (text) {
    Swal.fire({
      type: type,
      title: title,
      text: text
    });
  } else {
    Swal.fire({
      type: type,
      title: title
    });
  }
}

function setCursor(id) {
  $.post(
    "http://metro-core/metro:nui:setcursor",
    JSON.stringify({
      id
    })
  );
}

function playSound(set, sound) {
  $.post(
    "http://metro-core/metro:nui:playsound",
    JSON.stringify({
      sound,
      set
    })
  );
}

function createProfile(first, last, dob, height, gender) {
  $.post(
    "http://metro-core/metro:nui:createprofile",
    JSON.stringify({
      first,
      last,
      dob,
      height,
      gender
    })
  );
}

function deleteProfile(id) {
  $.post(
    "http://metro-core/metro:nui:deleteprofile",
    JSON.stringify({
      id
    })
  );
}

function selectProfile(id) {
  $.post(
    "http://metro-core/metro:nui:selectprofile",
    JSON.stringify({
      id
    })
  );
}

function previewModel(id, gender) {
  $.post(
    "http://metro-core/metro:nui:previewmodel",
    JSON.stringify({
      id,
      gender
    })
  );
}

function onInventorySwap(from, to) {
  $.post(
    "http://metro-core/metro:nui:inventory:swap",
    JSON.stringify({
      from,
      to
    })
  );
}

function onInventoryInteract(item) {
  $.post(
    "http://metro-core/metro:nui:inventory:interact",
    JSON.stringify({
      item
    })
  );
}

function handleMenuPlayersClick() {
  backend.openUI("players");
}

function handleMenuInventoryClick() {
  backend.openUI("inventory");
}

function handleMenuCraftingClick() {
  backend.openUI("crafting");
}

function handleMenuStatsClick() {
  showAlert("info", "Coming soon!");
}

function handleMenuContactsClick() {
  showAlert("info", "Coming soon!");
}
