﻿using metro_framework.bases;
using metro_framework.bases.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.utils
{
    public class ItemUtils
    {
        public static List<Item> items { get; set; }

        public static List<Recipe> recipes { get; set; }

        public static Item getItem(int id)
        {
            foreach(var item in items)
            {
                if(item.id == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static List<Item> getConsumables()
        {
            List<Item> consumables = new List<Item>();
            foreach(var item in items)
            {
                if(item.isConsumable())
                {
                    consumables.Add(item);
                }
            }
            return consumables;
        }

        public static List<Item> getMisc()
        {
            List<Item> miscs = new List<Item>();
            foreach (var item in items)
            {
                if (item.isMisc())
                {
                    miscs.Add(item);
                }
            }
            return miscs;
        }

        public static List<Item> getWeapons()
        {
            List<Item> weapons = new List<Item>();
            foreach (var item in items)
            {
                if (item.isWeapon())
                {
                    weapons.Add(item);
                }
            }
            return weapons;
        }
    }
}
