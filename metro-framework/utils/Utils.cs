﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.bases.users;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.utils
{
    public class Utils
    {
        public static List<Profile> profiles { get; set; }
        public static int selected_profile { get; set; }

        private static TimerBarPool TimerPool = new TimerBarPool();
        private static BarTimerBar TimerBar;
        public static bool IsWaitingForTimer;

        public static string LastCompletedTimer = "";
        private static float TimerModifier = 0.005f;

        public static Profile getSelectedProfile()
        {
            return profiles[selected_profile];
        }

        public static Guid SessionToken { get; set; }


        public static Dictionary<string, int> FactionClasses = new Dictionary<string, int>();

        public static String[] ModelsMale { get; set; }
        public static String[] ModelsFemale { get; set; }
        public static void DrawHelpText(string msg)
        {
            Function.Call(Hash.BEGIN_TEXT_COMMAND_DISPLAY_HELP, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_SCALEFORM, msg);
            Function.Call(Hash.END_TEXT_COMMAND_DISPLAY_HELP, 0, 0, 1, -1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="bg">140=Black | 6=Red | 184=Green</param>
        public static void DrawTextNotification(string text, int bg)
        {
            Function.Call(Hash._SET_NOTIFICATION_TEXT_ENTRY, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_STRING, text);
            Function.Call(Hash._SET_NOTIFICATION_BACKGROUND_COLOR, bg);
            Function.Call(Hash._DRAW_NOTIFICATION, false, false);
        }


        public static void DrawTimerBar(string text, int r, int g, int b, float modifier = 0.005f)
        {
            if (TimerBar != null)
            {
                TimerPool.Remove((TimerBarBase)TimerBar);
                TimerBar = (BarTimerBar)null;
                TimerBar = new BarTimerBar(text);
                TimerBar.Percentage = 0.0f;
                TimerBar.BackgroundColor = Color.FromArgb((int)byte.MaxValue, 0, 0, 0);
                TimerBar.ForegroundColor = Color.FromArgb((int)byte.MaxValue, r, g, b);
                TimerPool.Add((TimerBarBase)TimerBar);
            }
            else
            {
                TimerBar = new BarTimerBar(text);
                TimerBar.Percentage = 0.0f;
                TimerBar.BackgroundColor = Color.FromArgb((int)byte.MaxValue, 0, 0, 0);
                TimerBar.ForegroundColor = Color.FromArgb((int)byte.MaxValue, r, g, b);
                TimerPool.Add((TimerBarBase)TimerBar);
            }
            IsWaitingForTimer = true;
        }


        public static Boolean isValidDOB(string dob)
        {
            String[] parts = dob.Split('-');
            if(parts.Length != 3)
            {
                return false;
            }


            var temp = -1;
            if(!int.TryParse(parts[0], out temp) || parts[0].Length != 4)
            {
                return false;
            }

            if (!int.TryParse(parts[1], out temp) || parts[1].Length != 2)
            {
                return false;
            }

            if (!int.TryParse(parts[2], out temp) || parts[2].Length != 2)
            {
                return false;
            }

            return true;
        }

        public static async void GetHeadshot()
        {
            int hs = API.RegisterPedheadshot(Game.PlayerPed.Handle);

            while(!API.IsPedheadshotReady(hs))
            {
                await BaseScript.Delay(0);
            }

            String txt = API.GetPedheadshotTxdString(hs);

            

            Debug.WriteLine("headshot: " + txt);

        }

        public static async void SetPlayerModel(string model)
        {
            int m = API.GetHashKey(model);
            if(m != -1 && Game.PlayerPed.Model.Hash != m)
            {
                API.RequestModel((uint)m);
                while(!API.HasModelLoaded((uint)m))
                {
                    await BaseScript.Delay(0);
                }

                Model mo = new Model(m);
                await Game.Player.ChangeModel(mo);
                mo.MarkAsNoLongerNeeded();
                API.SetPedDefaultComponentVariation(API.GetPlayerPed(-1));
            }
        }


        public static void OnTick()
        {
            if(TimerBar != null)
            {
                if((double) TimerBar.Percentage >= 1.0)
                {
                    LastCompletedTimer = TimerBar.Label.ToString();
                    TimerPool.Remove((TimerBarBase)TimerBar);
                    TimerBar = (BarTimerBar)null;
                    IsWaitingForTimer = false;
                }
                else
                {
                    TimerBar.Percentage = TimerBar.Percentage + TimerModifier;
                }
            }
            TimerPool.Draw();
        }

    }
}
