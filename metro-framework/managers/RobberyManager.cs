﻿using CitizenFX.Core;
using metro_framework.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class RobberyManager
    {
        public bool IsRobbing { get; set; }
        private Ped TargetPed { get; set; }

        public void RobPed(Ped p)
        {
            if(this.IsRobbing)
            {
                Utils.DrawHelpText("You are currently robbing someone");
            }
            else
            {
                if(p != null && p.Exists())
                {
                    p.IsPositionFrozen = true;
                    this.IsRobbing = true;
                    this.TargetPed = p;
                }
            }
        }

        public void TickRobberies()
        {
            if(this.IsRobbing)
            {
                this.TargetPed.Task.PlayAnimation("mp_am_hold_up", "guard_handsup_loop", 1.0f, -1, AnimationFlags.Loop);
                if (!Utils.IsWaitingForTimer)
                {
                    if(Utils.LastCompletedTimer == "Robbing")
                    {
                        this.IsRobbing = false;
                        this.TargetPed.Task.ClearAll();
                        this.TargetPed.Task.FleeFrom(Game.PlayerPed);
                        this.TargetPed.IsPositionFrozen = false;
                    }
                }
            }
        }
    }
}
