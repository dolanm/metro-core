﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.bases;
using metro_framework.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class MapManager
    {
        private List<Blip> AllBlips = new List<Blip>();
        private List<Marker> AllMarkers = new List<Marker>();

        public MapManager()
        {

        }

        private void AddGasStations()
        {

        }

        public void Tick()
        {
            if(Game.PlayerPed.IsInVehicle() == false)
            {
                Ped[] AllPeds = World.GetAllPeds();
                if(AllPeds.Length > 0)
                {
                    foreach(Ped p in AllPeds)
                    {
                        if(p != Game.PlayerPed && p.IsDead == false)
                        {
                            if (p.Position.DistanceToSquared(Game.PlayerPed.Position) <= 2.0f)
                            {
                                Utils.DrawHelpText("Press ~INPUT_PICKUP~ to rob");
                                if (Game.IsControlJustPressed(1, Control.Pickup))
                                {
                                    Utils.DrawTextNotification("Robbery triggered", 140);
                                    Utils.DrawTimerBar("Robbing", 255, 0, 0);
                                    Client.INSTANCE.RobberyManager.RobPed(p);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
