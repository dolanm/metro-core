﻿using metro_framework.bases.factions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class FactionManager
    {
        public List<Faction> RegisteredFactions { get; set; }
        public FactionManager()
        {
            this.RegisteredFactions = new List<Faction>();
        }

        public Faction GetFactionByName(string name)
        {
            foreach (Faction f in this.RegisteredFactions)
            {
                if (f.FactionName == name)
                {
                    return f;
                }
            }
            return null;
        }

        public Faction GetFactionByID(int id)
        {
            foreach (Faction f in this.RegisteredFactions)
            {
                if (f.FactionID == id)
                {
                    return f;
                }
            }
            return null;
        }

        public void RegisterFaction(Faction f)
        {
            if (f != null)
            {
                this.RegisteredFactions.Add(f);
            }
            else
            {
                CitizenFX.Core.Debug.WriteLine("Attempted to register null faction...");
            }
        }
    }
}
