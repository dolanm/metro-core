﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class RadioManager
    {
        public double CurrentFreq { get; set; } = 000.0;

        public void SetFrequency(string FreqString)
        {
            double freq = Convert.ToDouble(FreqString);

            if(freq >= 000.0 && freq <= 999.9)
            {
                //Do shit
                this.CurrentFreq = freq;
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Set radio frequency to: " + freq.ToString("0.0") } });
            }
            else
            {
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[METRO]", "Radio frequency out of range(000.0 - 999.9)." } });
            }
        }

        public void SendMessage(string msg)
        {
            BaseScript.TriggerServerEvent("metro:SV_E:Radio:Send", this.CurrentFreq, msg);
        }

        public void RecieveMessage(double freq, string msg)
        {
            if (freq == this.CurrentFreq)
            {
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^2[RADIO]", msg } });
            }
        }
    }
}
