﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class VendorManager
    {
        public List<NPCVendor> Vendors = new List<NPCVendor>();
        public List<Blip> AddedBlips = new List<Blip>();

        public VendorManager()
        {

        }

        public void AddVendors()
        {
            this.Vendors.Add(new NPCVendor(1, "Contraband Vendor", new Vector3(900.24f, -1071.98f, 32.97f), 1.77f, 4.0f, "s_m_y_dealer_01", false, BlipSprite.Drugs));
        }

        public void CreateNPCS()
        {
            if(this.Vendors.Count > 0)
            {
                foreach(NPCVendor v in this.Vendors)
                {
                    Hash pedHash = Function.Call<Hash>(Hash.GET_HASH_KEY, v.VendorModel);
                    Function.Call(Hash.CREATE_PED, 0, pedHash, v.VendorLocation.X, v.VendorLocation.Y, v.VendorLocation.Z, v.VendorHeading, false, true);
                    CitizenFX.Core.Debug.WriteLine("Hash: " + pedHash.ToString());
                    /*v.Vendor = new Ped(API.CreatePed(0, (uint)pedHash, v.VendorLocation.X, v.VendorLocation.Y, v.VendorLocation.Z, v.VendorHeading, false, true));
                    v.Vendor.IsInvincible = true;
                    v.Vendor.CanBeTargetted = false;
                    v.Vendor.IsPositionFrozen = true;*/
                    //Function.Call(Hash.CREATE_PED, 0, pedHash, v.VendorLocation.X, v.VendorLocation.Y, v.VendorLocation.Z, v.VendorHeading, false, true);
                }
            }
        }

        public void CreateBlips()
        {
            if(this.Vendors.Count > 0)
            {
                foreach(NPCVendor v in this.Vendors)
                {
                    if(v.IsHidden == false)
                    {
                        Blip npc = new Blip(API.AddBlipForCoord(v.VendorLocation.X, v.VendorLocation.Y, v.VendorLocation.Z));
                        npc.Sprite = v.VendorIcon;
                        npc.Name = v.VendorTitle;
                        npc.Color = BlipColor.White;
                        npc.IsShortRange = true;
                        this.AddedBlips.Add(npc);
                    }
                }
            }
        }

        public void Tick()
        {
            /*if(this.Vendors.Count > 0)
            {
                foreach(NPCVendor v in this.Vendors)
                {
                    v.Vendor.Task.ClearAll();
                }
            }*/
        }
    }
}
