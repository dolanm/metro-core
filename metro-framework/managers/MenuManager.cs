﻿using metro_framework.jobs.ticked.police;
using metro_framework.menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class MenuManager
    {
        public ProfileMenu ProfileMenu { get; set; }
        public LockerMenu LockerMenu { get; set; }
        public GarageMenu GarageMenu { get; set; }

        public DevKit DevKit { get; set; }

        public MenuManager() { }

        public ProfileMenu GetProfileMenu()
        {
            CitizenFX.Core.Debug.WriteLine("CALLED GETPROFILEMENU");
            this.ProfileMenu = new ProfileMenu();
            return this.ProfileMenu;
        }

        public LockerMenu GetLockerMenu()
        {
            this.LockerMenu = new LockerMenu();
            return this.LockerMenu;
        }

        public GarageMenu GetPoliceGarageMenu()
        {
            this.GarageMenu = new GarageMenu();
            return this.GarageMenu;
        }

        public DevKit GetDevKit()
        {
            CitizenFX.Core.Debug.WriteLine("CALLED GETDEVKIT");
            this.DevKit = new DevKit();
            return this.DevKit;
        }


        public void Tick()
        {
            if(this.ProfileMenu != null)
            {
                this.ProfileMenu.Tick();
            }

            if (this.DevKit != null)
            {
                this.DevKit.Tick();
            }

            if(this.LockerMenu != null)
            {
                this.LockerMenu.Tick();
            }

            if(this.GarageMenu != null)
            {
                this.GarageMenu.Tick();
            }
        }
    }
}
