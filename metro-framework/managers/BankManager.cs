﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.utils;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.managers
{
    public class BankManager
    {
        public bool IsRobbing;
        public Vector3 RobbingLocation;

        public Vector3[] BankVaults =
        {
            new Vector3(146.77f, -1044.79f, 29.38f),
            new Vector3(-103.6623f, 6477.792f, 31.62672f),
            new Vector3(1176.496f, 2711.58f, 38.09778f),
            new Vector3(-2957.652f, 481.3654f, 15.70647f),
            new Vector3(-353.9925f, -54.05035f, 49.04631f),
            new Vector3(311.1357f, -283.2345f, 54.17453f)
        };

        public List<Blip> AddedBlips = new List<Blip>();

        int Countdown = 2;
        int SecondsCounted = 0;
        int LastUpdateTick;

        public void AddBankBlips()
        {
            foreach(Vector3 bank in this.BankVaults)
            {
                Blip bank_blip = new Blip(API.AddBlipForCoord(bank.X, bank.Y, bank.Z));
                bank_blip.Sprite = BlipSprite.DollarSign;
                bank_blip.Name = "Bank";
                bank_blip.Color = BlipColor.Green;
                bank_blip.IsShortRange = true;
                this.AddedBlips.Add(bank_blip);
            }
        }

        public void StartRobbery(Vector3 position)
        {
            this.IsRobbing = true;
            this.RobbingLocation = position;
            this.LastUpdateTick = Environment.TickCount;
        }

        public void CleanupRobbery()
        {
            this.IsRobbing = false;
            this.RobbingLocation = Vector3.Zero;
        }

        public void Tick()
        {
            foreach(Vector3 bank in this.BankVaults)
            {
                World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(bank.X, bank.Y, bank.Z - 0.9f), Vector3.Zero, Vector3.Zero, new Vector3(1.0f, 1.0f, 1.0f), UnknownColors.Red, false, false, false);
                if (Game.PlayerPed.Position.DistanceToSquared(bank) <= 1.0f)
                {
                    if(this.IsRobbing)
                    {
                        Utils.DrawHelpText("Press ~INPUT_PICKUP~ to cancel robbery");
                        if (Game.IsControlJustPressed(1, Control.Pickup))
                        {
                            BaseScript.TriggerServerEvent("metro:SV_E:Robbery:Cancel", Game.Player.ServerId, Game.PlayerPed.Position);
                        }
                    }
                    else
                    {
                        Utils.DrawHelpText("Press ~INPUT_PICKUP~ to start robbery");
                        if (Game.IsControlJustPressed(1, Control.Pickup))
                        {
                            BaseScript.TriggerServerEvent("metro:SV_E:Robbery:StartBank", Game.Player.ServerId, Game.PlayerPed.Position);
                        }
                    }
                }
            }

            if (this.IsRobbing && this.RobbingLocation != Vector3.Zero)
            {
                if(Game.PlayerPed.Position.DistanceToSquared(this.RobbingLocation) > 40.0f)
                {
                    Utils.DrawHelpText("You have 10 seconds to return before the robbery is cancelled");

                    if(this.Countdown <= this.SecondsCounted)
                    {
                        this.IsRobbing = false;
                        BaseScript.TriggerServerEvent("metro:SV_E:Robbery:Cancel", Game.Player.ServerId, this.RobbingLocation);
                    }
                    else if(Environment.TickCount - this.LastUpdateTick > 10000)
                    {
                        this.SecondsCounted = this.SecondsCounted + 1;
                        this.LastUpdateTick = Environment.TickCount;

                    }
                }
            }
        }
    }
}
