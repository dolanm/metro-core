﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.utils;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.jobs.ticked.police
{
    public class GarageMenu : UIMenu
    {
        public MenuPool MENUPOOL;

        public UIMenu LiveryMenu;
        public UIMenu ExtraMenu;

        private Vehicle CurrentGhostCar;

        private List<dynamic> ValidVehicles = new List<dynamic> { "POLICE", "POLICE3", "POLICE2", "POLICEB", "16FPIS"};
        private Dictionary<string, uint> VehicleHashes = new Dictionary<string, uint>()
        {
            {
                "POLICE", (uint)API.GetHashKey("POLICE")
            },
            {
                "POLICE3", (uint)API.GetHashKey("POLICE3")
            },
            {
                "POLICE2", (uint)API.GetHashKey("POLICE2")
            },
            {
                "POLICEB", (uint)API.GetHashKey("POLICEB")
            },
            {
                "16FPIS", (uint)API.GetHashKey("16FPIS")
            }
        };
        /*private Dictionary<string, uint> VehicleHashes = new Dictionary<string, uint>()
        {
            ["POLICE"] = (uint)API.GetHashKey("POLICE"),
            ["POLICE3"] = (uint)API.GetHashKey("POLICE3"),
            ["POLICE2"] = (uint)API.GetHashKey("POLICE2"),
            ["POLICEB"] = (uint)API.GetHashKey("POLICEB"),
            ["16FPIS"] = (uint)API.GetHashKey("16FPIS")
        };*/

        public void SetupCategories()
        {
            this.LiveryMenu = this.MENUPOOL.AddSubMenu(this, "Livery Menu");
            this.ExtraMenu = this.MENUPOOL.AddSubMenu(this, "Extra Menu");

            this.SetupAllOptions();
        }

        public void DeleteAllItems()
        {
            this.LiveryMenu.MenuItems.Clear();
            this.ExtraMenu.MenuItems.Clear();

            this.SetupAllOptions();

            this.MENUPOOL.RefreshIndex();
            this.RefreshIndex();
        }


        public void SetupAllOptions()
        {
            UIMenuListItem VehicleList = new UIMenuListItem("Vehicle", this.ValidVehicles, 0);
            this.AddItem(VehicleList);
            this.OnItemSelect += (async (src, item, index) =>
            {
                if(item == VehicleList)
                {
                    if (this.CurrentGhostCar != null)
                    {
                        this.CurrentGhostCar.Delete();
                        this.CurrentGhostCar = null;
                    }

                    if (this.VehicleHashes.ContainsKey(this.ValidVehicles[index]))
                    {
                        //this.CurrentGhostCar = new Vehicle(API.CreateVehicle(VehicleHashes(this.ValidVehicles[index]), Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z, Game.PlayerPed.Heading, true, false));
                        this.CurrentGhostCar = new Vehicle(API.CreateVehicle(this.VehicleHashes[this.ValidVehicles[index]], Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z, Game.PlayerPed.Heading, true, false));
                        if (this.CurrentGhostCar.Exists())
                        {
                            API.TaskWarpPedIntoVehicle(Game.PlayerPed.Handle, this.CurrentGhostCar.Handle, -1);
                        }
                    }
                }
            });
            /*this.OnListChange += (async (src, item, index) =>
            {
                if(item == VehicleList)
                {
                    if (this.CurrentGhostCar != null)
                    {
                        this.CurrentGhostCar.Delete();
                        this.CurrentGhostCar = null;
                    }
                    //Utils.DrawTextNotification("Text: " + this.ValidVehicles[index], 140);
                    if(this.VehicleHashes.ContainsKey(this.ValidVehicles[index]))
                    {
                        //this.CurrentGhostCar = new Vehicle(API.CreateVehicle(VehicleHashes(this.ValidVehicles[index]), Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z, Game.PlayerPed.Heading, true, false));
                        this.CurrentGhostCar = new Vehicle(API.CreateVehicle(this.VehicleHashes[this.ValidVehicles[index]], Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z, Game.PlayerPed.Heading, true, false));
                        if(this.CurrentGhostCar.Exists())
                        {
                            API.TaskWarpPedIntoVehicle(Game.PlayerPed.Handle, this.CurrentGhostCar.Handle, -1);
                        }
                    }
                }
            });*/

        }


        public GarageMenu() : base("Police Garage", "")
        {
            this.MENUPOOL = new MenuPool();

            this.SetupCategories();
            this.MENUPOOL.Add(this);
        }

        public void Tick()
        {
            this.MENUPOOL.ProcessMenus();
        }
    }
}
