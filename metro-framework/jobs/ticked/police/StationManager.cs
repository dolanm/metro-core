﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using metro_framework.utils;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.jobs.ticked.police
{
    public class StationManager
    {
        #region Station Vectors
        public Vector3[] StationLocations =
        {
            new Vector3(456.607f, -991.1454f, 30.68961f),
            new Vector3(379.4556f, -1615.109f, 29.29193f),
            new Vector3(830.2437f, -1287.259f, 28.21111f),
            new Vector3(-1069.939f, -855.9251f, 4.867281f),
            new Vector3(626.7874f, 25.19444f, 87.85793f),
            new Vector3(1849.392f, 2585.931f, 45.67201f),
            new Vector3(1856.176f, 3687.389f, 34.26708f),
            new Vector3(-447.6707f, 6016.231f, 31.7164f)
        };
        #endregion

        #region Locker Vectors
        public Vector3[] LockerLocations =
        {
            new Vector3(451.83f, -992.83f, 30.69f),
            new Vector3(831.83f, -1294.502f, 28.21111f),
            new Vector3(370.08f, -1608.10f, 29.29f),
            new Vector3(-1078.71f, -856.49f, 5.04f),
            new Vector3(620.34f, 17.77f, 87.89f),
            new Vector3(1846.27f, 2585.84f, 45.67f),
            new Vector3(1848.85f, 3689.70f, 34.27f),
            new Vector3(-448.03f, 6008.44f, 31.72f)
        };
        #endregion

        #region Garage Vectors
        public Vector3[] GarageLocations =
        {
            new Vector3(860.2898f, -1350.289f, 26.06626f),
            new Vector3(452.932f, -1017.658f, 28.46727f),
            new Vector3(1870.29f, 3693.466f, 33.60218f)
        };
        #endregion

        public List<Blip> AddedBlips = new List<Blip>();

        public void AddStationBlips()
        {
            foreach(Vector3 station in this.StationLocations)
            {
                Blip station_blip = new Blip(API.AddBlipForCoord(station.X, station.Y, station.Z));
                station_blip.Sprite = BlipSprite.PoliceStation;
                station_blip.Name = "Police Station";
                station_blip.Color = BlipColor.Blue;
                station_blip.IsShortRange = true;
                this.AddedBlips.Add(station_blip);
            }
        }

        public void Tick()
        {
            foreach (Vector3 station in this.StationLocations)
            {
                World.DrawMarker(MarkerType.HorizontalSplitArrowCircle, new Vector3(station.X, station.Y, station.Z - 0.9f), Vector3.Zero, Vector3.Zero, new Vector3(2.0f, 2.0f, 2.0f), UnknownColors.LightBlue, false, false, true);
                if (Game.PlayerPed.Position.DistanceToSquared(station) <= 1.0f)
                {
                    Utils.DrawHelpText("Press ~INPUT_PICKUP~ to toggle duty status");
                    if(Game.IsControlJustPressed(1, Control.Pickup))
                    {
                        if(PoliceMain.IsPlayerOnDuty)
                        {
                            Utils.DrawTextNotification("You are no longer on duty", 6);
                            PoliceMain.IsPlayerOnDuty = false;
                        }
                        else
                        {
                            switch(Utils.getSelectedProfile().FactionRank)
                            {
                                case 0:
                                    Utils.SetPlayerModel("s_m_y_cop_01");
                                    break;
                                case 1:
                                    Utils.SetPlayerModel("s_m_y_sheriff_02");
                                    break;
                                case 2:
                                    Utils.SetPlayerModel("s_m_y_hwaycop_02");
                                    break;
                            }
                            Utils.DrawTextNotification("You are now on duty", 184);
                            PoliceMain.IsPlayerOnDuty = true;
                        }
                    }
                }
            }

            if(PoliceMain.IsPlayerOnDuty)
            {
                foreach(Vector3 locker in this.LockerLocations)
                {
                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(locker.X, locker.Y, locker.Z - 0.9f), Vector3.Zero, Vector3.Zero, new Vector3(1.0f, 1.0f, 1.0f), UnknownColors.Yellow, false, false, false);
                    if (Game.PlayerPed.Position.DistanceToSquared(locker) <= 1.0f)
                    {
                        Utils.DrawHelpText("Press ~INPUT_PICKUP~ to open the locker");
                        if (Game.IsControlJustPressed(1, Control.Pickup))
                        {
                            Client.INSTANCE.MenuManager.GetLockerMenu().DeleteAllItems();
                            Client.INSTANCE.MenuManager.GetLockerMenu().Visible = true;
                        }
                    }
                }

                foreach(Vector3 garage in this.GarageLocations)
                {
                    World.DrawMarker(MarkerType.HorizontalCircleSkinny, new Vector3(garage.X, garage.Y, garage.Z - 0.9f), Vector3.Zero, Vector3.Zero, new Vector3(2.0f, 2.0f, 2.0f), UnknownColors.LightBlue, false, false, true);
                    if (Game.PlayerPed.Position.DistanceToSquared(garage) <= 1.0f)
                    {
                        if (Game.PlayerPed.IsInPoliceVehicle)
                        {
                            Utils.DrawHelpText("Press ~INPUT_PICKUP~ to store your vehicle");
                            if (Game.IsControlJustPressed(1, Control.Pickup))
                            {
                                if (Game.PlayerPed.IsSittingInVehicle())
                                {
                                    Vehicle cv = Game.PlayerPed.LastVehicle;
                                    if (API.GetPedInVehicleSeat(cv.Handle, -1) == Game.PlayerPed.Handle)
                                    {
                                        API.SetEntityAsMissionEntity(cv.Handle, true, true);
                                        int cvh = cv.Handle;
                                        API.DeleteVehicle(ref cvh);

                                        if (cv.Exists())
                                        {
                                            Utils.DrawTextNotification("Unable to store vehicle", 6);
                                        }
                                        else
                                        {
                                            Utils.DrawTextNotification("Vehicle has been stored", 184);
                                        }
                                    }
                                    else
                                    {
                                        Utils.DrawTextNotification("You must be in the driver's seat", 6);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Utils.DrawHelpText("Press ~INPUT_PICKUP~ to take a vehicle");
                            if (Game.IsControlJustPressed(1, Control.Pickup))
                            {
                                Client.INSTANCE.MenuManager.GetPoliceGarageMenu().DeleteAllItems();
                                Client.INSTANCE.MenuManager.GetPoliceGarageMenu().Visible = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
