﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.jobs.ticked.police
{
    public class LockerMenu : UIMenu
    {
        public MenuPool MENUPOOL;

        public UIMenu HeadOptions;
        public UIMenu TopOptions;
        public UIMenu LegOptions;
        public UIMenu HolsterOptions;

        public void SetupCategories()
        {
            this.HeadOptions = this.MENUPOOL.AddSubMenu(this, "Head");
            this.TopOptions = this.MENUPOOL.AddSubMenu(this, "Top");
            this.LegOptions = this.MENUPOOL.AddSubMenu(this, "Legs");
            this.HolsterOptions = this.MENUPOOL.AddSubMenu(this, "Holster");

            this.SetupAllOptions();
        }

        public void DeleteAllItems()
        {
            this.HeadOptions.MenuItems.Clear();
            this.TopOptions.MenuItems.Clear();
            this.LegOptions.MenuItems.Clear();
            this.HolsterOptions.MenuItems.Clear();

            this.SetupAllOptions();


            this.MENUPOOL.RefreshIndex();
            this.RefreshIndex();
        }

        public void SetupAllOptions()
        {
            #region HEAD
            int max_drawables_head = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 0);
            int max_textures_head = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 0, 1);
            int current_texture_head = 0;

            for(int i = 0; i < max_drawables_head; i++)
            {
                UIMenuItem headItem = new UIMenuItem(i.ToString());
                this.HeadOptions.AddItem(headItem);
            }

            this.HeadOptions.OnIndexChange += (async (src, index) =>
            {
                Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 0, index, 0, 0);
            });

            this.HeadOptions.OnItemSelect += (async (src, item, index) =>
            {
                if(current_texture_head < max_textures_head - 1)
                {
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 0, index, current_texture_head, 0);
                    current_texture_head += 1;
                }else if(current_texture_head == max_textures_head - 1)
                {
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 0, index, current_texture_head, 0);
                    current_texture_head = 0;
                }
            });
            #endregion

            #region TOP
            int max_drawables_top = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 3);
            int max_textures_top = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 3, 0);
            int current_textures_top = 0;

            for(int i = 0; i < max_drawables_top; i++)
            {
                UIMenuItem topItem = new UIMenuItem(i.ToString());
                this.TopOptions.AddItem(topItem);
            }

            this.TopOptions.OnIndexChange += (async (src, index) =>
            {
                Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, index, 0, 0);
            });

            this.TopOptions.OnItemSelect += (async (src, item, index) =>
            {
                if(current_textures_top < max_textures_top - 1)
                {
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, index, current_textures_top, 0);
                    current_textures_top += 1;
                }else if(current_textures_top == max_textures_top - 1)
                {
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, index, current_textures_top, 0);
                    current_textures_top = 0;
                }
            });
            #endregion

            #region LEG
            int max_drawables_leg = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 4);
            for(int i = 0; i < max_drawables_leg; i++)
            {
                UIMenuItem legItem = new UIMenuItem(i.ToString());
                this.LegOptions.AddItem(legItem);
            }

            this.LegOptions.OnIndexChange += (async (src, index) =>
            {
                Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 4, index, 0, 0);
            });
            #endregion

            #region HOLSTER
            int max_drawables_holster = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 9);
            for(int i = 0; i < max_drawables_holster; i++)
            {
                UIMenuItem holsterItem = new UIMenuItem(i.ToString());
                this.HolsterOptions.AddItem(holsterItem);
            }

            this.HolsterOptions.OnIndexChange += (async (src, index) =>
            {
                Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 9, index, 0, 0);
            });
            #endregion
        }

        public LockerMenu() : base("Locker Room", "")
        {
            this.MENUPOOL = new MenuPool();
            this.SetupCategories();

            this.MENUPOOL.Add(this);
        }

        public void Tick()
        {
            this.MENUPOOL.ProcessMenus();
        }
    }
}
