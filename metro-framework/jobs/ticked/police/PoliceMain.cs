﻿using CitizenFX.Core;
using metro_framework.bases.users;
using metro_framework.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metro_framework.jobs.ticked.police
{
    public class PoliceMain
    {
        public bool HasInitiated { get; set; }

        public StationManager StationManager { get; set; }

        public static bool IsPlayerOnDuty { get; set; }

        public PoliceMain()
        {
            this.StationManager = new StationManager();
        }

        public void Init()
        {
            Profile p = Utils.getSelectedProfile();
            if(p != null)
            {
                if(p.CurrentFaction.FactionName == "police")
                {
                    this.StationManager.AddStationBlips();
                }
            }

            this.HasInitiated = true;
        }
    }
}
