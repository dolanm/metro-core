resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937';

ui_page "html/index.html"

server_scripts {
	'metro-server.net.dll',
}

client_scripts {
	'System.Runtime.dll',
	'Newtonsoft.Json.dll',
	'nativeui.net.dll',
	'metro-client.net.dll',
	'discord.lua',
}


files {
	"html/index.html",
	"html/autoload.js",
	"html/main.js",
	"html/interfaces/ingame/index.html",
	"html/interfaces/ingame/js/main.js",
	"html/interfaces/inventory/index.html",
	"html/interfaces/inventory/js/main.js",
	"html/interfaces/crafting/index.html",
	"html/interfaces/crafting/js/main.js",
	"html/interfaces/profileSelection/index.html",
	"html/interfaces/profileSelection/js/main.js",
	"html/interfaces/profileCreation/index.html",
	"html/interfaces/profileCreation/js/main.js",
	"html/interfaces/players/index.html",
    "html/interfaces/players/js/main.js",
}